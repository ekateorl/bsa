import { withStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";

const MyRating = withStyles({
  iconFilled: {
    color: "#ffb29d",
  },
  iconHover: {
    color: "#ff9579",
  },
})(Rating);

export default MyRating;
