import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

const StyledToggleButton = withStyles({
  root: {
    "&$selected": {
      borderColor: "transparent",
      color: "white",
      background: "linear-gradient(45deg, #4db15b 0%, #93c354 100%)",

      "&$disabled": {
        color: "rgba(0,0,0,0.3)",
        background: "linear-gradient(45deg, #addeb4 0%, #ccea97 100%)",
      },
    },
  },
  selected: {},
  disabled: {},
})((props) => <ToggleButton color="default" {...props} />);

const useStyles = makeStyles({
  icon: {
    marginRight: "10px",
    width: "20px",
    height: "20px",
  },
  toggleButton: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

function MyToggleButton({ selected, children, ...props }) {
  const classes = useStyles();

  return (
    <StyledToggleButton variant="contained" selected={selected} {...props}>
      <div className={classes.toggleButton}>
        {selected ? (
          <CheckCircleIcon className={classes.icon} />
        ) : (
          <RadioButtonUncheckedIcon className={classes.icon} />
        )}
        {children}
      </div>
    </StyledToggleButton>
  );
}
export default MyToggleButton;
