import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { fileExists } from "../api/methods";

const useStyles = makeStyles({
  maskedIcon: {
    display: "block",
    textIndent: "-9999px",
    width: "100px",
    height: "100px",
    background: "#745e8a",
    WebkitMaskSize: "contain",
    WebkitMaskPosition: "center",
    WebkitMaskRepeat: "no-repeat",
    WebkitMaskBoxImage: (props) => `url(${props.url})`,
    //minHeight: "120px",
  },
});

const MyIcon = ({ url, id, ...other }) => {
  const [img, setImg] = useState(url);
  const classes = useStyles({ url: img });
  useEffect(
    () =>
      fileExists(
        url,
        () => setImg(url),
        () => setImg("/svg/rose.svg")
      ),
    [url]
  );
  return <div className={classes.maskedIcon} id={id} {...other}></div>;
};

export default MyIcon;
