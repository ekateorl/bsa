import React from "react";
import { withStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";

import { Gradients } from "../common/constants";

const gradient = "linear-gradient(-135deg, #ffb29d 0%, #dba6e8 100%)";

const MyTimeButton = withStyles({
  root: {
    color: "black",
    textTransform: "none",
    border: "1px solid transparent",
    background: `linear-gradient(#fff,#fff) padding-box, 
            ${gradient} border-box`,
    minWidth: "fit-content",
    "&$selected": {
      color: "white",
      background: gradient,
    },
  },
  selected: {},
})((props) => <ToggleButton color="default" {...props} />);

export default MyTimeButton;
