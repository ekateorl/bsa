import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";

import { Gradients } from "../common/constants";

const useStyles = makeStyles({
  card: {
    textTransform: "none",
    border: "1px solid transparent",
    background: (props) => `linear-gradient(#fff,#fff) padding-box, 
          ${props.gradient} border-box`,
    minWidth: "fit-content",
  },
});

function MyCard({ gradient = Gradients.orange, ...other }) {
  const classes = useStyles({ gradient });
  return <Card className={classes.card} {...other} variant="outlined" />;
}
export default MyCard;
