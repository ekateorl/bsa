import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import { Gradients } from "../common/constants";

const useStyles = makeStyles({
  card: {
    textTransform: "none",
    border: "1px solid transparent",
    background: (props) => `linear-gradient(#fff,#fff) padding-box, 
          ${props.gradient} border-box`,
    minWidth: "fit-content",
  },
});

function MyButton({ gradient = Gradients.orange, ...other }) {
  const classes = useStyles({ gradient });
  return <Button className={classes.card} {...other} variant="outlined" />;
}
export default MyButton;
