import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  title: {
    color: "#745e8a",
    fontWeight: "bold",
    marginBottom: theme.spacing(3),
  },
}));

function MyTitle(props) {
  const classes = useStyles();
  return <Typography className={classes.title} variant="h4" {...props} />;
}
export default MyTitle;
