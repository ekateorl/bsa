import React from "react";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

function MultipleTypeSelect({
  types,
  label = "",
  value = [],
  controlProps = {},
  ...props
}) {
  console.log(value);
  const selected = types.filter((i) => value.indexOf(i.id) !== -1);
  return (
    <FormControl variant="outlined" fullWidth {...controlProps}>
      <InputLabel htmlFor="outlined-age-native-simple">{label}</InputLabel>
      <Select
        multiple
        label={label}
        value={value}
        renderValue={() => (
          <div style={{ overflowWrap: "mormal", whiteSpace: "normal" }}>
            {selected.map((i) => i.name).join(", ")}
          </div>
        )}
        {...props}
      >
        {types.map((type, index) => (
          <MenuItem key={"type" + index} value={type.id}>
            {type.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default MultipleTypeSelect;
