import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";

import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";

import MyTitle from "../../UI/MyTitle";
import MyIcon from "../../UI/MyIcon";
import { Specialist } from "../../api/methods";
import ReviewList from "./ReviewList";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    width: "100%",
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
    width: "100%",
    height: "100px",
  },
  progress: {
    color: "#ffb29d",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
  },
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
        {children}
      </MyTitle>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

function ReviewModal({
  open,
  onClose = () => {},
  specialistId = -1,
  specialist = {},
}) {
  const classes = useStyles();

  const [reviews, setReviews] = useState([]);
  const [loading, setLoading] = useState(true);

  const getReviews = (id) => {
    if (id !== -1) {
      setLoading(true);
      Specialist.getReviews(id, ({ data }) => {
        setReviews(data);
        setLoading(false);
      });
    } else {
      setLoading(true);
    }
  };

  useEffect(() => getReviews(specialistId), [specialistId]);

  const handleClose = () => {
    onClose();
  };

  const dialogContent = () => {
    if (loading && open)
      return (
        <div className={classes.root}>
          <div className={classes.wrapper}>
            {loading && (
              <CircularProgress size={50} className={classes.progress} />
            )}
          </div>
        </div>
      );
    if (reviews && reviews.length) return <ReviewList reviews={reviews} />;
    return (
      <div
        style={{
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <MyIcon
          url="/svg/empty.svg"
          style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
        />

        <Typography color="textSecondary" style={{ marginTop: "10px" }}>
          У этого специалиста ещё нет отзывов
        </Typography>
      </div>
    );
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
        Отзывы о специалисте
      </DialogTitle>

      <DialogContent dividers>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            marginBottom: "15px",
          }}
        >
          <Avatar
            src={specialist.image}
            style={{ width: "100px", height: "100px", marginBottom: "10px" }}
          />
          <Typography variant="h5">
            {specialist.surname} {specialist.name} {specialist.fatherName}
          </Typography>
        </div>
        {dialogContent()}
      </DialogContent>
    </Dialog>
  );
}

export default ReviewModal;
