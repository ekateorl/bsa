import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import MyRating from "../../UI/MyRating";

function ReviewList({ reviews = [] }) {
  reviews = reviews.sort((a, b) => {
    if (b.reviewTime > a.reviewTime) return 1;
    return -1;
  });
  return (
    <List>
      {reviews.map(({ review, clientName, reviewTime, rating }, index) => (
        <>
          <ListItem
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              alignItems: "flex-start",
            }}
          >
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "space-between",
                alignItems: "baseline",
              }}
            >
              <Typography variant="h6">{clientName}</Typography>

              <Typography color="textSecondary">
                {moment(reviewTime).format("DD MMMM YYYY HH:mm")}
              </Typography>
            </div>
            <MyRating
              value={rating}
              style={{
                marginBottom: "10px",
              }}
            />
            <Typography>{review}</Typography>
          </ListItem>
          {index < reviews.length - 1 && <Divider component="li" />}
        </>
      ))}
    </List>
  );
}

export default ReviewList;
