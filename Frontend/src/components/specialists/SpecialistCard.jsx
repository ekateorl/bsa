import React from "react";
import moment from "moment";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";

import IconButton from "@material-ui/core/IconButton";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import { Avatar, Typography, Link } from "@material-ui/core";
import {
  formatMonths,
  isAdmin,
  ratingCountToString,
} from "../../common/methods";
import ModalButton from "../modals/ModalButton";
import ConfirmModalButton from "../modals/ConfirmModalButton";
import MyCard from "../../UI/MyCard";
import MyRating from "../../UI/MyRating";

import { Gradients } from "../../common/constants";
import AddEditSpecialistForm from "../modals/specialists/AddEditSpecialistForm";

function SpecialistCard({
  hasEvenIndex = false,
  specialist = {},
  onEdit = () => {},
  onDelete = () => {},
  onReviewOpen = () => {},
}) {
  const gradient = hasEvenIndex ? Gradients.blue : Gradients.orange;

  return (
    <MyCard
      style={{ height: "100%", display: "flex", flexDirection: "column" }}
      gradient={gradient}
    >
      <CardContent
        style={{ flexGrow: 1, display: "flex", flexDirection: "column" }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: "100%",
            marginBottom: "10px",
            flexDirection: "column",
          }}
        >
          <Avatar
            src={specialist.image}
            style={{ width: "100px", height: "100px", marginBottom: "10px" }}
          />

          <MyRating
            name="rating"
            value={specialist.rating}
            readOnly
            precision={0.01}
          />
          <Typography color="textSecondary" align="center">
            {specialist.rating}/5
          </Typography>
          <Typography color="textSecondary" align="center">
            {specialist.ratingCount > 0 ? (
              <Link
                href="#"
                onClick={() => onReviewOpen(specialist.id)}
                color="textSecondary"
              >
                {ratingCountToString(specialist.ratingCount)}
              </Link>
            ) : (
              ratingCountToString(specialist.ratingCount)
            )}
          </Typography>
        </div>

        <Typography
          variant="h6"
          align="center"
          style={{
            lineHeight: "1.5",
            marginBottom: "10px",
            flexGrow: 1,
          }}
        >
          {specialist.surname} {specialist.name} {specialist.fatherName}
        </Typography>
        {/* <Typography align="center">Парикмахер</Typography>*/}
        <Typography color="textSecondary" align="center">
          Стаж: {formatMonths(specialist.qualifyingPeriod)}
        </Typography>
        {isAdmin() && (
          <Typography align="center">{specialist.phone}</Typography>
        )}
      </CardContent>
      {isAdmin() && (
        <CardActions
          style={{ justifyContent: "flex-end", marginRight: "10px" }}
        >
          <ModalButton
            title="Редактирование специалиста"
            onSubmit={(...props) => onEdit(specialist.id, ...props)}
            form={(props) => (
              <AddEditSpecialistForm
                id="editSpecialistForm"
                initialValues={{
                  ...specialist,
                  image: specialist.image,
                  startDate: moment(specialist.startDate),
                }}
                {...props}
              />
            )}
            formId="editSpecialistForm"
          >
            <IconButton edge="end" color="primary">
              <EditOutlinedIcon />
            </IconButton>
          </ModalButton>
          <ConfirmModalButton
            title="Удалить специалиста?"
            text="Данный специалист больше не будет доступен для записи и редактирования"
            okText="Удалить"
            onOk={(...props) => onDelete(specialist.id, ...props)}
          >
            <IconButton edge="end" color="primary">
              <DeleteOutlineOutlinedIcon />
            </IconButton>
          </ConfirmModalButton>
        </CardActions>
      )}
    </MyCard>
  );
}

export default SpecialistCard;
