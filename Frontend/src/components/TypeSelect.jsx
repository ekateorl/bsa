import React from "react";

import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

function TypeSelect({ types, ...props }) {
  return (
    <TextField select {...props}>
      {types.map((type, index) => (
        <MenuItem key={"type" + index} value={type.id}>
          {type.name}
        </MenuItem>
      ))}
    </TextField>
  );
}

export default TypeSelect;
