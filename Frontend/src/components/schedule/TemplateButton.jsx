import React, { useState, useEffect } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import ModalButton from "../modals/ModalButton";
import SelectTemplateForm from "../modals/schedule/SelectTemplateForm";

import { Template } from "../../api/methods";
import AddEditTemplateForm from "../modals/schedule/AddEditTemplateForm";

function TemplateButton({
  onTemplateSelect = () => {},
  onSaveAsTemplate = () => {},
  ...props
}) {
  const [anchorEl, setAnchorEl] = useState(null);

  const [templates, setTemplates] = useState([]);
  const [loading, setLoading] = useState(true);

  const getTemplates = () => {
    setLoading(true);
    Template.getAll(({ data }) => {
      setLoading(false);
      setTemplates(data);
    });
  };

  useEffect(getTemplates, []);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSubmitSelected = ({ selected }, success) => {
    onTemplateSelect(templates[selected].id);
    success();
  };
  const handleSubmitSave = (values, success) => {
    onSaveAsTemplate(values, () => {
      getTemplates();
      success();
    });
  };

  return (
    <>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClick}
        endIcon={<ExpandMoreIcon />}
        {...props}
      >
        Шаблоны
      </Button>
      <Menu
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <ModalButton
          title="Выбор шаблона"
          form={({ ...props }) => (
            <SelectTemplateForm
              list={templates}
              initialValues={{ selected: 0 }}
              id="select-template-form"
              {...props}
            />
          )}
          formId="select-template-form"
          onSubmit={handleSubmitSelected}
        >
          <MenuItem onClick={handleClose}>Применить шаблон</MenuItem>
        </ModalButton>
        <ModalButton
          title="Создание шаблона"
          form={({ ...props }) => (
            <AddEditTemplateForm id="save-as-template-form" {...props} />
          )}
          formId="save-as-template-form"
          onSubmit={handleSubmitSave}
        >
          <MenuItem onClick={handleClose}>Сохранить как шаблон</MenuItem>
        </ModalButton>
      </Menu>
    </>
  );
}

export default TemplateButton;
