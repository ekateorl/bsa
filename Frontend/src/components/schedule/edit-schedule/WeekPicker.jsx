import React from "react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

import { formatDatePeriod, getWeek } from "../../../common/methods";

function WeekPicker({ week = [], onChange = () => {}, disabled }) {
  const onNextWeek = () => {
    const newWeek = getWeek(week[0].clone().add(1, "week"));
    onChange(newWeek);
  };

  const onPreviousWeek = () => {
    onChange(getWeek(week[0].clone().add(-1, "week")));
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "baseline",
        justifyContent: "space-between",
        width: "100%",
        marginBottom: "15px",
      }}
    >
      <Button
        onClick={onPreviousWeek}
        color="primary"
        startIcon={<ArrowBackIcon />}
        disabled={disabled}
      >
        Предыдущая
      </Button>
      <Typography variant="h5">{formatDatePeriod(week[0], week[6])}</Typography>
      <Button
        onClick={onNextWeek}
        color="primary"
        endIcon={<ArrowForwardIcon />}
        disabled={disabled}
      >
        Следующая
      </Button>
    </div>
  );
}

export default WeekPicker;
