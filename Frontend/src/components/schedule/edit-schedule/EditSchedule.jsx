import React, { useState, useEffect } from "react";
import {
  getWeek,
  minutesToString,
  stringToMinutes,
} from "../../../common/methods";

import ScheduleTable from "../ScheduleTable";
import WeekPicker from "./WeekPicker";
import ActionsArea from "./ActionsArea";

import { Template, TimeSlot } from "../../../api/methods";

const generateTimeSlots = (start, end, duration) => {
  const startTime = stringToMinutes(start);
  const endTime = stringToMinutes(end);
  const durationTime = stringToMinutes(duration);

  const result = [];
  for (let i = startTime; i + durationTime <= endTime; i += durationTime)
    result.push({
      beginning: minutesToString(i),
      duration: minutesToString(durationTime),
      checked: false,
    });
  return result;
};

const generateWeek = (slots = []) => {
  const result = [];
  for (let i = 0; i < 7; i++) {
    result.push(
      slots.map((i) => {
        return { ...i };
      })
    );
  }
  return result;
};

function EditSchedule() {
  const slots = generateTimeSlots("8:00", "18:00", "00:30");

  const [timeSlots, setTimeSlots] = useState(generateWeek(slots));

  const [initialValues, setInitialValues] = useState(generateWeek(slots));
  const [specialistId, setSpecialistId] = useState(-1);

  const [loadingTimeSlots, setLoadingTimeSlots] = useState(true);
  const [saving, setSaving] = useState(false);
  const [templateSaving, setTemplateSaving] = useState(false);

  const [week, setWeek] = useState(getWeek());

  const addExtistingSlots = (existingSlots = []) => {
    const newTimeSlots = generateWeek(slots);
    existingSlots.forEach((slots, i) => {
      if (!slots) return;
      slots.forEach((timeSlot) => {
        const j = newTimeSlots[i].findIndex(
          (item) =>
            item.beginning === timeSlot.beginning &&
            item.duration === timeSlot.duration
        );
        if (j !== -1) newTimeSlots[i][j] = { ...timeSlot, checked: true };
      });
    });
    setInitialValues(
      newTimeSlots.map((i) =>
        i.map((j) => {
          return { ...j };
        })
      )
    );
    setTimeSlots([...newTimeSlots]);
  };

  const getTimeSlots = (specialistId, week) => {
    setLoadingTimeSlots(true);
    if (specialistId !== -1)
      TimeSlot.getForSpecialist(
        {
          dates: week.map((day) => day.format("YYYY-MM-DD")),
          specialistId,
        },
        ({ data }) => {
          addExtistingSlots(
            data.map((i) => {
              return i.timeSlots ? i.timeSlots : [];
            })
          );
          setLoadingTimeSlots(false);
        }
      );
  };

  useEffect(() => {
    getTimeSlots(specialistId, week);
  }, [specialistId, week]);

  const onSelect = (i, j) => {
    timeSlots[j][i].checked = !timeSlots[j][i].checked;
    setTimeSlots([...timeSlots]);
  };

  const updateTimeSlots = (data) => {
    setSaving(true);
    TimeSlot.update(
      data,
      () => {
        getTimeSlots(specialistId, week);
        setSaving(false);
      },
      () => setSaving(false)
    );
  };

  const onSave = () => {
    const timeToCreate = [];
    const timeToDelete = [];
    timeSlots.forEach((list, i) => {
      const newTimeSlots = [];
      list.forEach((timeSlot) => {
        if ((timeSlot.id || timeSlot.id === 0) && !timeSlot.checked)
          timeToDelete.push(timeSlot.id);
        else if (!timeSlot.id && timeSlot.id !== 0 && timeSlot.checked)
          newTimeSlots.push({
            beginning: timeSlot.beginning,
            duration: timeSlot.duration,
          });
      });
      if (newTimeSlots.length > 0)
        timeToCreate.push({
          date: week[i].format("YYYY-MM-DD"),
          timeSlots: newTimeSlots,
        });
    });
    updateTimeSlots({
      specialistId,
      timeToCreate,
      timeToDelete,
    });
  };

  const onSaveAsTemplate = (template, success) => {
    template.timeSlots = timeSlots.map((list) =>
      list.filter((i) => i.checked).sort((i) => i.beginning)
    );
    setTemplateSaving(true);
    Template.create(template, () => {
      setTemplateSaving(false);
      success();
    });
  };

  const applyTemplate = (template) => {
    timeSlots.forEach((list) => {
      list.forEach((timeSlot) => {
        if (!timeSlot.appointmentFk) timeSlot.checked = false;
      });
    });

    template.forEach((slots, i) => {
      if (!slots) return;
      slots.forEach((timeSlot) => {
        const j = timeSlots[i].findIndex(
          (item) =>
            item.beginning === timeSlot.beginning &&
            item.duration === timeSlot.duration
        );
        if (j !== -1) timeSlots[i][j].checked = true;
      });
    });

    setTimeSlots([...timeSlots]);
  };

  const onTemplateApply = (id) => {
    setLoadingTimeSlots(true);
    Template.get(id, ({ data }) => {
      applyTemplate(data.timeSlots);
      setLoadingTimeSlots(false);
    });
  };

  const onReset = () => {
    setTimeSlots(
      initialValues.map((i) =>
        i.map((j) => {
          return { ...j };
        })
      )
    );
  };

  const loading =
    specialistId === -1 || loadingTimeSlots || saving || templateSaving;

  return (
    <>
      <ActionsArea
        onSave={onSave}
        onReset={onReset}
        onChange={setSpecialistId}
        onSaveAsTemplate={onSaveAsTemplate}
        onTemplateSelect={onTemplateApply}
        specialistId={specialistId}
        loading={loading}
        saving={saving}
      />
      <WeekPicker
        week={week}
        onChange={(newWeek) => setWeek(Array.from(newWeek))}
        disabled={loading}
      />

      <ScheduleTable
        timeSlots={timeSlots}
        head={week.map((day) => day.format("dd, D"))}
        onSelect={onSelect}
        loading={loading}
      />
    </>
  );
}

export default EditSchedule;
