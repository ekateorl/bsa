import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Typography } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";

import TemplateButton from "../TemplateButton";
import ModalButton from "../../modals/ModalButton";
import SelectSpecialistsForm from "../../modals/schedule/SelectSpecialistForm";
import { Specialist } from "../../../api/methods";
import { Images } from "../../../api/constants";
import LoadingButton from "../../../UI/LoadingButton";

const useStyles = makeStyles({
  root: {
    marginBottom: "20px",
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    width: "100px",
    height: "100px",
  },
  spaceBetweetWrapper: {
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "space-between",
    width: "100%",
  },
  columnWrapper: {
    display: "flex",
    flexDirection: "column",
    marginLeft: "20px",
  },
  buttonWrapper1: {
    marginTop: "10px",
    width: "fit-content",
  },
  button: { width: "fit-content", marginRight: "10px" },
});

function ActionsArea({
  specialistId = -1,
  onChange = () => {},
  onSave = () => {},
  onReset = () => {},
  onSaveAsTemplate = () => {},
  onTemplateSelect = () => {},
  loading = false,
  saving = false,
}) {
  const classes = useStyles();

  const [specialists, setSpecialists] = useState([]);
  const [loadingSpecialists, setLoadingSpecialists] = useState(true);

  const selectedIndex = specialists.findIndex((i) => i.id === specialistId);

  const selectedSpecialist =
    specialists && specialists[selectedIndex] ? specialists[selectedIndex] : {};

  const getSpecialists = () => {
    setLoadingSpecialists(true);
    Specialist.getAll(({ data }) => {
      setLoadingSpecialists(false);
      data.forEach((i) => (i.image = `${Images.specialist(i.image)}`));
      setSpecialists(data);
      if (specialistId === -1 && data && data[0]) onChange(data[0].id);
    });
  };

  useEffect(() => {
    getSpecialists();
  }, []);

  const onSubmit = ({ selected }, success) => {
    onChange(specialists[selected].id);
    success();
  };

  return (
    <div className={classes.root}>
      <Avatar src={selectedSpecialist.image} className={classes.avatar} />
      <div className={classes.spaceBetweetWrapper}>
        <div className={classes.columnWrapper}>
          <Typography variant="h5">
            {selectedSpecialist.surname} {selectedSpecialist.name}{" "}
            {selectedSpecialist.fatherName}
          </Typography>
          <div className={classes.buttonWrapper1}>
            <ModalButton
              title="Выбор специалиста"
              formId="specialist-radio-form"
              onSubmit={onSubmit}
              form={({ ...props }) => (
                <SelectSpecialistsForm
                  id="specialist-radio-form"
                  initialValues={{ selected: selectedIndex }}
                  list={specialists}
                  {...props}
                />
              )}
            >
              <Button
                variant="outlined"
                color="primary"
                className={classes.button}
                disabled={loading}
              >
                Выбрать специалиста
              </Button>
            </ModalButton>
            <TemplateButton
              disabled={loading}
              onSaveAsTemplate={onSaveAsTemplate}
              onTemplateSelect={onTemplateSelect}
            />
          </div>
        </div>
        <div style={{ display: "flex" }}>
          <LoadingButton
            onClick={onSave}
            variant="contained"
            color="primary"
            className={classes.button}
            disabled={loading}
            pending={saving}
          >
            Сохранить
          </LoadingButton>
          <Button
            onClick={onReset}
            variant="outlined"
            color="primary"
            disabled={loading}
            style={{ display: "inline" }}
          >
            Отменить изменения
          </Button>
        </div>
      </div>
    </div>
  );
}

export default ActionsArea;
