import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import MyToggleButton from "../../UI/MyToggleButton";

function ScheduleTable({
  timeSlots = [],
  head = [],
  onSelect = () => {},
  loading = false,
}) {
  return (
    <Table stickyHeader size="small">
      <TableHead>
        <TableRow>
          {head.map((item) => (
            <TableCell align="center">
              <Typography>{item}</Typography>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {timeSlots[0].map((_, i) => (
          <TableRow key={`row-${i}`}>
            {head.map((_, j) => (
              <TableCell align="center">
                <MyToggleButton
                  variant="contained"
                  style={{ width: "100%", height: "100%" }}
                  selected={timeSlots[j][i].checked}
                  onChange={() => onSelect(i, j)}
                  disabled={
                    (timeSlots[j][i].appointmentFk !== null &&
                      timeSlots[j][i].appointmentFk !== undefined) ||
                    loading
                  }
                >
                  {timeSlots[j][i].beginning}
                </MyToggleButton>
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default ScheduleTable;
