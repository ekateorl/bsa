import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import { Button } from "@material-ui/core";

import ModalButton from "../../modals/ModalButton";
import { Template } from "../../../api/methods";
import LoadingButton from "../../../UI/LoadingButton";
import SelectTemplateForm from "../../modals/schedule/SelectTemplateForm";
import AddEditTemplateForm from "../../modals/schedule/AddEditTemplateForm";
import ConfirmModalButton from "../../modals/ConfirmModalButton";

const useStyles = makeStyles({
  root: {
    marginBottom: "20px",
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    width: "100px",
    height: "100px",
  },
  spaceBetweetWrapper: {
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "space-between",
    width: "100%",
  },
  columnWrapper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  buttonWrapper1: {
    marginTop: "10px",
    width: "fit-content",
  },
  button: { width: "fit-content", marginRight: "10px" },
  bottomButton: { width: "max-content", marginTop: "10px" },
  flexToRight: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
  },
  flexToLeft: {
    display: "flex",
    flexDirection: "column",
  },
});

function TemplatesActionsArea({
  templateId = -1,
  onChange = () => {},
  onSave = () => {},
  onReset = () => {},
  onSaveAsTemplate = () => {},
  onAddTemplate = () => {},
  onDeleteTemplate = () => {},
  loading = false,
  saving = false,
}) {
  const classes = useStyles();

  const [templates, setTemplates] = useState([]);
  const [forReset, setForReset] = useState(Math.random());

  const [loadingTemplates, setLoadingTemplates] = useState(true);

  const selectedIndex = templates.findIndex((i) => i.id === templateId);

  const selectedTemplate =
    templates && templates[selectedIndex] ? templates[selectedIndex] : {};

  const getTemplates = () => {
    setLoadingTemplates(true);
    Template.getAll(({ data }) => {
      setLoadingTemplates(false);
      setTemplates(data);
      if (templateId === -1 && data && data[0]) onChange(data[0].id);
    });
  };

  useEffect(() => {
    getTemplates();
  }, [templateId]);

  const onSubmit = ({ selected }, success) => {
    onChange(templates[selected].id);
    success();
  };

  const handleSave = (values, setSubmitting) => {
    setSubmitting(true);
    onSave(values, () => {
      setSubmitting(false);
      getTemplates();
    });
  };

  const handleSaveAsTemplate = (values, success) => {
    onSaveAsTemplate(values, () => {
      getTemplates();
      success();
    });
  };

  const handleAdd = (values, success) => {
    onAddTemplate(values, () => {
      getTemplates();
      success();
    });
  };

  const handleDelete = (values, success) => {
    onDeleteTemplate(values, () => {
      getTemplates();
      success();
    });
  };
  const handleReset = () => {
    setForReset(Math.random);
    // formRef.current.reset();
    onReset();
  };

  return (
    <div className={classes.root}>
      <div className={classes.columnWrapper}>
        <AddEditTemplateForm
          initialValues={{ name: selectedTemplate.name, forReset }}
          id="edit-template-form"
          handleSubmit={handleSave}
        />
        <div className={classes.spaceBetweetWrapper}>
          <div className={classes.flexToLeft}>
            <div className={classes.buttonWrapper1}>
              <ModalButton
                title="Выбор шаблона"
                formId="specialist-radio-form"
                onSubmit={onSubmit}
                disabled={loading}
                form={({ ...props }) => (
                  <SelectTemplateForm
                    id="specialist-radio-form"
                    initialValues={{ selected: selectedIndex }}
                    list={templates}
                    {...props}
                  />
                )}
              >
                <Button
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  disabled={loading}
                >
                  Выбрать шаблон
                </Button>
              </ModalButton>
              <ModalButton
                title="Добавление шаблона"
                form={({ ...props }) => (
                  <AddEditTemplateForm id="add-template-form" {...props} />
                )}
                formId="add-template-form"
                onSubmit={handleAdd}
              >
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  disabled={loading}
                >
                  Добавить
                </Button>
              </ModalButton>
            </div>
            <ConfirmModalButton
              title="Удалить шаблон?"
              okText="Удалить"
              onOk={handleDelete}
            >
              <Button
                variant="outlined"
                color="primary"
                className={classes.bottomButton}
                disabled={loading}
              >
                Удалить выбранный
              </Button>
            </ConfirmModalButton>
          </div>
          <div className={classes.flexToRight}>
            <div style={{ display: "flex" }}>
              <LoadingButton
                form="edit-template-form"
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
                disabled={loading}
                pending={saving}
              >
                Сохранить
              </LoadingButton>
              <Button
                onClick={handleReset}
                variant="outlined"
                color="primary"
                disabled={loading}
                style={{ display: "inline" }}
              >
                Отменить изменения
              </Button>
            </div>
            <ModalButton
              title="Добавление шаблона"
              form={({ ...props }) => (
                <AddEditTemplateForm id="save-as-template-form" {...props} />
              )}
              formId="save-as-template-form"
              onSubmit={handleSaveAsTemplate}
            >
              <Button
                variant="outlined"
                color="primary"
                className={classes.bottomButton}
                disabled={loading}
              >
                Сохранить как новый шаблон
              </Button>
            </ModalButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TemplatesActionsArea;
