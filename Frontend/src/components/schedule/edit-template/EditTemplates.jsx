import React, { useState, useEffect } from "react";
import { minutesToString, stringToMinutes } from "../../../common/methods";

import ScheduleTable from "../ScheduleTable";
import TemplatesActionsArea from "./TemplatesActionsArea";

import { Template } from "../../../api/methods";
import moment from "moment";

const generateTimeSlots = (start, end, duration) => {
  const startTime = stringToMinutes(start);
  const endTime = stringToMinutes(end);
  const durationTime = stringToMinutes(duration);

  const result = [];
  for (let i = startTime; i + durationTime <= endTime; i += durationTime)
    result.push({
      beginning: minutesToString(i),
      duration: minutesToString(durationTime),
      checked: false,
    });
  return result;
};

const generateWeek = (slots = []) => {
  const result = [];
  for (let i = 0; i < 7; i++) {
    result.push(
      slots.map((i) => {
        return { ...i };
      })
    );
  }
  return result;
};

function EditTemplates() {
  const slots = generateTimeSlots("8:00", "18:00", "00:30");

  const [timeSlots, setTimeSlots] = useState(generateWeek(slots));

  const [initialValues, setInitialValues] = useState(generateWeek(slots));
  const [templateId, setTemplatetId] = useState(-1);

  const [loadingTimeSlots, setLoadingTimeSlots] = useState(true);
  const [saving, setSaving] = useState(false);
  const [templateSaving, setTemplateSaving] = useState(false);

  const addExtistingSlots = (existingSlots = []) => {
    const newTimeSlots = generateWeek(slots);
    existingSlots.forEach((slots, i) => {
      if (!slots) return;
      slots.forEach((timeSlot) => {
        const j = newTimeSlots[i].findIndex(
          (item) =>
            item.beginning === timeSlot.beginning &&
            item.duration === timeSlot.duration
        );
        if (j !== -1) newTimeSlots[i][j] = { ...timeSlot, checked: true };
      });
    });
    setInitialValues(
      newTimeSlots.map((i) =>
        i.map((j) => {
          return { ...j };
        })
      )
    );
    setTimeSlots([...newTimeSlots]);
  };

  const getTimeSlots = (templateId) => {
    setLoadingTimeSlots(true);
    if (templateId !== -1)
      Template.get(templateId, ({ data }) => {
        addExtistingSlots(data.timeSlots);
        setLoadingTimeSlots(false);
      });
  };

  useEffect(() => {
    getTimeSlots(templateId);
  }, [templateId]);

  const onSelect = (i, j) => {
    timeSlots[j][i].checked = !timeSlots[j][i].checked;
    setTimeSlots([...timeSlots]);
  };

  const updateTimeSlots = (data, success) => {
    setSaving(true);

    Template.update(
      templateId,
      data,
      () => {
        getTimeSlots(templateId);
        setSaving(false);
        success();
      },
      () => setSaving(false)
    );
  };

  const onSave = (template, success) => {
    const timeToCreate = [];
    const timeToDelete = [];
    timeSlots.forEach((list, i) => {
      const newTimeSlots = [];
      list.forEach((timeSlot) => {
        if ((timeSlot.id || timeSlot.id === 0) && !timeSlot.checked)
          timeToDelete.push(timeSlot.id);
        else if (!timeSlot.id && timeSlot.id !== 0 && timeSlot.checked)
          newTimeSlots.push({
            beginning: timeSlot.beginning,
            duration: timeSlot.duration,
          });
      });
      timeToCreate.push(newTimeSlots);
    });
    updateTimeSlots(
      {
        ...template,
        timeToCreate,
        timeToDelete,
      },
      success
    );
  };

  const onSaveAsTemplate = (template, success) => {
    template.timeSlots = timeSlots.map((list) =>
      list.filter((i) => i.checked).sort((i) => i.beginning)
    );
    setLoadingTimeSlots(true);
    Template.create(template, ({ data }) => {
      success();
      setTemplatetId(data.id);
    });
  };

  const onAddTemplate = (template, success) => {
    setLoadingTimeSlots(true);
    Template.create(template, ({ data }) => {
      success();
      setTemplatetId(data.id);
    });
  };

  const onDeleteTemplate = (success) => {
    setLoadingTimeSlots(true);
    Template.delete(templateId, () => {
      success();
      setTemplatetId(-1);
    });
  };

  const onReset = () => {
    setTimeSlots(
      initialValues.map((i) =>
        i.map((j) => {
          return { ...j };
        })
      )
    );
  };

  const loading =
    templateId === -1 || loadingTimeSlots || saving || templateSaving;

  return (
    <>
      <TemplatesActionsArea
        onSave={onSave}
        onReset={onReset}
        onChange={setTemplatetId}
        onSaveAsTemplate={onSaveAsTemplate}
        onAddTemplate={onAddTemplate}
        onDeleteTemplate={onDeleteTemplate}
        templateId={templateId}
        loading={loading}
        saving={saving}
      />
      <ScheduleTable
        timeSlots={timeSlots}
        head={moment.weekdaysShort(true)}
        onSelect={onSelect}
        loading={loading}
      />
    </>
  );
}

export default EditTemplates;
