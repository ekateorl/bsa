import React, { useState } from "react";
import {
  ViewState,
  GroupingState,
  IntegratedGrouping,
} from "@devexpress/dx-react-scheduler";
import {
  Scheduler,
  Resources,
  Appointments,
  AppointmentTooltip,
  GroupingPanel,
  DayView,
  AppointmentForm,
} from "@devexpress/dx-react-scheduler-material-ui";
import moment from "moment";
import { stringToMinutes } from "../../../common/methods";
import AppointmentInfoDialog from "./AppointmentInfoDialog";
import { Appointment as AppointmentAPI } from "../../../api/methods";
import { Images } from "../../../api/constants";

export default function AppointmentSchedule({
  specialists = [],
  appointments = [],
  date = "2017-05-28",
  refresh = () => {},
}) {
  let appointmentList = [];

  appointments.forEach((i) => {
    const startDate = moment(`${i.date}T${i.beginning}:00`);
    appointmentList.push({
      id: i.id,
      title: i.clientName,
      specialists: [i.specialistFk],
      startDate: startDate.toDate(),
      endDate: startDate.add(stringToMinutes(i.duration), "minutes").toDate(),
    });
  });

  let specialistList = specialists.map((i) => {
    return {
      text: i.surname + " " + i.name,
      id: i.id,
      color: "#745e8a",
    };
  });

  if (specialistList.length === 0) specialistList = [{}];

  const resources = [
    {
      fieldName: "specialists",
      title: "Специалисты",
      instances: specialistList,
      allowMultiple: true,
    },
  ];
  const grouping = [
    {
      resourceName: "specialists",
    },
  ];
  const groupByDate = () => true;

  const [open, setOpen] = useState(false);
  const [appInfo, setAppInfo] = useState({});

  const onClose = () => setOpen(false);

  const onAppointmentCancel = (id = -1, values = {}, success = () => {}) => {
    AppointmentAPI.cancel(id, values, () => {
      refresh();
      success();
      setOpen(false);
    });
  };

  const onAppointmentClick = ({ data }) => {
    const { id } = data;
    AppointmentAPI.get(id, ({ data }) => {
      const specialist = { ...data.specialist };
      specialist.image = Images.specialist(specialist.image);
      console.log(data);
      setAppInfo({ ...data, specialist });
    });
    setOpen(true);
  };

  const Appointment = ({ children, style, ...restProps }) => (
    <Appointments.Appointment
      {...restProps}
      style={{
        ...style,
        fontSize: "10pt",
        // background: "linear-gradient(-135deg, #ffb29d 0%, #dba6e8 100%)",
        borderRadius: "8px",
      }}
      onClick={onAppointmentClick}
    >
      {children}
    </Appointments.Appointment>
  );

  return (
    <div style={{ overflowX: "auto" }}>
      <div style={{ width: 50 + specialistList.length * 200 + "px" }}>
        <Scheduler data={appointmentList} locale={"ru-RU"}>
          <ViewState currentDate={date} />
          <GroupingState grouping={grouping} groupByDate={groupByDate} />

          <DayView startDayHour={8} endDayHour={19} cellDuration={30} />
          <Appointments appointmentComponent={Appointment} />
          <Resources data={resources} mainResourceName="specialists" />

          <IntegratedGrouping />

          <AppointmentForm />
          <GroupingPanel />
        </Scheduler>
        <AppointmentInfoDialog
          open={open}
          appInfo={appInfo}
          onClose={onClose}
          onAppointmentCancel={onAppointmentCancel}
        />
      </div>
    </div>
  );
}
