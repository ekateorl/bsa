import moment from "moment";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import { Appointment, Specialist } from "../../../api/methods";
import AppointmentSchedule from "./AppointmentSchedule";
import { KeyboardDatePicker } from "@material-ui/pickers";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  tabs: {
    marginBottom: theme.spacing(3),
  },
  root: {
    display: "flex",
    alignItems: "center",
    width: "100%",
  },
  progress: {
    color: "#ffb29d",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
  },
}));

function AppointmentsForDay() {
  const classes = useStyles();

  const [specialists, setSpecialists] = useState([{}]);
  const [specialistsLoading, setSpecialistsLoading] = useState(true);

  const [appointments, setAppointments] = useState([]);
  const [appointmentsLoading, setAppointmentsLoading] = useState([]);

  const [date, setDate] = useState(moment().format("YYYY-MM-DD"));

  const getSpecialists = () => {
    setSpecialistsLoading(true);
    Specialist.getAll(({ data }) => {
      setSpecialists(data);
      setSpecialistsLoading(false);
    });
  };

  useEffect(getSpecialists, []);

  const getAppointments = () => {
    setAppointmentsLoading(true);
    Appointment.getForDate({ date }, ({ data }) => {
      setAppointments(data);
      setAppointmentsLoading(false);
    });
  };
  useEffect(getAppointments, [date]);

  return (
    <>
      {!specialistsLoading && (
        <>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <KeyboardDatePicker
              label="Дата"
              variant="inline"
              inputVariant="outlined"
              autoOk
              openTo="date"
              format="DD.MM.yyyy"
              disableToolbar
              value={date}
              InputLabelProps={{ shrink: true }}
              onChange={(newDate) => {
                setDate(moment(newDate).format("YYYY-MM-DD"));
              }}
              disabled={appointmentsLoading}
            />
          </div>
          <AppointmentSchedule
            specialists={specialists}
            date={date}
            appointments={appointments}
            refresh={getAppointments}
          />
        </>
      )}
      {specialistsLoading && (
        <div className={classes.root}>
          <CircularProgress size={50} className={classes.progress} />
        </div>
      )}
    </>
  );
}

export default AppointmentsForDay;
