import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";

import DialogContent from "@material-ui/core/DialogContent";
import MuiDialogTitle from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import MyTitle from "../../../UI/MyTitle";
import { Typography } from "@material-ui/core";
import ModalButton from "../../modals/ModalButton";
import CancelAppointmentForm from "../../modals/schedule/CancelAppointmentForm";
import GeneralAppointmentInfo from "../../appointments/GeneralAppointmentInfo";
import { formatDuration } from "../../../common/methods";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
        {children}
      </MyTitle>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

function AppointmentInfoDialog({
  open,
  loading,
  onAppointmentCancel = () => {},
  onClose = () => {},
  appInfo = {},
}) {
  const onSubmit = (values, success) =>
    onAppointmentCancel(appInfo.id, values, success);
  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="sm"
      fullWidth
      onClose={onClose}
    >
      <DialogTitle onClose={onClose}>
        <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
          Информация о записи
        </MyTitle>
      </DialogTitle>
      <DialogContent dividers>
        <Card variant="outlined" style={{ marginBottom: "15px" }}>
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Клиент
            </Typography>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                <Typography variant="h6">{appInfo.client?.name}</Typography>
              </div>
              <div>
                <Typography align="right">
                  {appInfo.client?.phoneNumber}
                </Typography>
                <Typography align="right">{appInfo.client?.email}</Typography>
              </div>
            </div>
          </CardContent>
        </Card>

        <GeneralAppointmentInfo
          date={appInfo.date}
          beginning={appInfo.beginning}
          specialist={appInfo.specialist ? appInfo.specialist : {}}
        />

        <Typography align="center">{appInfo.services?.join(", ")}</Typography>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "baseline",
            marginTop: "15px",
          }}
        >
          <Typography color="textSecondary">
            {formatDuration(appInfo.duration)}
          </Typography>
          <Typography
            variant="h5"
            style={{ fontWeight: "bold", color: "#ffb29d", marginLeft: "20px" }}
          >
            {appInfo.price} ₽
          </Typography>
        </div>
      </DialogContent>
      <DialogActions>
        <div style={{ width: "100%" }}>
          <ModalButton
            title="Отменить запись?"
            form={(props) => (
              <CancelAppointmentForm id="cancel-app-form" {...props} />
            )}
            formId="cancel-app-form"
            onSubmit={onSubmit}
          >
            <Button
              color="primary"
              variant="contained"
              style={{ width: "100%" }}
              disabled={appInfo.completed}
            >
              Отменить запись
            </Button>
          </ModalButton>
        </div>
      </DialogActions>
    </Dialog>
  );
}

export default AppointmentInfoDialog;
