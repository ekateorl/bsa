import React from "react";
import { withStyles } from "@material-ui/core/styles";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

const StyledRadio = withStyles({
  root: {
    "&$checked": {
      color: "#ffb29d",
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

export default function DataTypeRadio({ value, onChange = () => {} }) {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Тип отображаемых данных</FormLabel>
      <RadioGroup value={value} onChange={onChange} row>
        <FormControlLabel
          value="count"
          control={<StyledRadio />}
          label="Количество услуг"
        />
        <FormControlLabel
          value="sum"
          control={<StyledRadio />}
          label="Приход"
        />
      </RadioGroup>
    </FormControl>
  );
}
