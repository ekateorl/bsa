import React, { useEffect, useState } from "react";

import moment from "moment";

import { Bar } from "react-chartjs-2";
import PeriodPicker from "./PeriodPicker";

import { getChartGradients, interpolateColors } from "../../common/methods";
import { Specialist } from "../../api/methods";
import DataTypeRadio from "./DataTypeRadio";

const options = (title = "", labelEnd = "") => {
  return {
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: title,
      },
    },
    scales: {
      y: {
        ticks: {
          beginAtZero: true,
          callback: (value) => value.toLocaleString() + labelEnd,
        },
      },
    },
  };
};

function SpecialistsPerfomance() {
  const [period, setPeriod] = useState({
    start: moment().startOf("month"),
    end: moment(),
  });

  const [data, setData] = useState([]);
  useEffect(() => {
    Specialist.getPerfomance(period, ({ data }) => setData(data));
  }, [period]);

  const [dataType, setDataType] = useState("count");
  const onDataTypeChange = ({ target }) => setDataType(target.value);

  const getDatasets = data.specialists
    ? data.specialists.map((i) => i[dataType]).sort((i, j) => j - i)
    : [];

  const getLabels = data.specialists
    ? data.specialists.map((i) => i.surname + " " + i.name)
    : [];

  const chartData = (canvas) => {
    const ctx = canvas.getContext("2d");
    return {
      labels: getLabels,
      datasets: [
        {
          data: getDatasets,
          backgroundColor: getChartGradients(ctx),
          borderWidth: 0,
        },
      ],
    };
  };

  const totalType = dataType === "count" ? "totalCount" : "totalSum";
  let total = data[totalType] ? data[totalType].toLocaleString() : 0;
  if (dataType === "sum") total += " ₽";
  let title =
    dataType === "count" ? "Количество оказанных услуг" : "Приход в кассу";
  title += ` (Всего ${total})`;

  const labelEnd = dataType === "sum" ? " ₽" : "";

  return (
    <>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div>
          <PeriodPicker value={period} onChange={setPeriod} />
        </div>

        <DataTypeRadio value={dataType} onChange={onDataTypeChange} />
      </div>
      <div style={{ margin: "15px 0", height: "fit-content" }}>
        <Bar data={chartData} options={options(title, labelEnd)} />
      </div>
    </>
  );
}

export default SpecialistsPerfomance;
