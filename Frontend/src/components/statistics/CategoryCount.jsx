import moment from "moment";
import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { DataGrid } from "@material-ui/data-grid";

import { Pie } from "react-chartjs-2";
import PeriodPicker from "./PeriodPicker";

import { getChartGradients } from "../../common/methods";
import { Category } from "../../api/methods";

const columns = [
  {
    field: "name",
    headerName: "НАЗВАНИЕ УСЛУГИ",
    flex: 1,
  },
  {
    field: "category",
    headerName: "КАТЕГОРИЯ",
    flex: 1,
  },
  { field: "count", headerName: "КОЛИЧЕСТВО", width: 200, type: "number" },
];

const options = {
  legend: {
    position: "left",
    labels: {
      boxWidth: 10,
    },
  },
};

function CategoryCount() {
  const [period, setPeriod] = useState({
    start: moment().subtract(1, "month"),
    end: moment(),
  });
  const [data, setData] = useState([]);

  useEffect(() => {
    Category.getCount(period, ({ data }) => setData(data));
  }, [period]);

  const rows = () => {
    const result = [];
    data.forEach((i) => {
      const services = i.services.map((j) => {
        return {
          ...j,
          category: i.name,
        };
      });
      result.push(...services);
    });
    return result;
  };

  const chartData = (canvas) => {
    const ctx = canvas.getContext("2d");

    return {
      labels: data.map((i) => i.name),
      datasets: [
        {
          data: data.map((i) => i.count),
          backgroundColor: getChartGradients(ctx, 450),
          borderWidth: 2,
        },
      ],
    };
  };

  return (
    <Grid container spacing={7}>
      <Grid item xs>
        <PeriodPicker value={period} onChange={setPeriod} />
        <div style={{ marginTop: "15px", height: "100%" }}>
          <DataGrid
            rows={rows()}
            columns={columns}
            disableColumnMenu
            disableSelectionOnClick
            hideFooter
            density="compact"
            sortModel={[
              {
                field: "count",
                sort: "desc",
              },
            ]}
          />
        </div>
      </Grid>
      <Grid item xs={5}>
        <div style={{ width: "400px", height: "400px", float: "right" }}>
          <Pie data={chartData} options={options} />
        </div>
      </Grid>
    </Grid>
  );
}

export default CategoryCount;
