import React, { useEffect, useState } from "react";
import moment from "moment";
import * as d3 from "d3-scale-chromatic";
import { Button } from "@material-ui/core";

import { Line } from "react-chartjs-2";
import PeriodPicker from "./PeriodPicker";
import ModalButton from "../modals/ModalButton";

import { interpolateColors } from "../../common/methods";
import { Category, Specialist } from "../../api/methods";
import SelectSpecialistsForm from "../modals/categories/SelectSpecialistsForm";

const options = {
  plugins: {
    legend: {
      position: "top",
      labels: {
        boxWidth: 10,
      },
    },
  },
};

const colorRangeInfo = {
  colorStart: 0.6,
  colorEnd: 0.9,
  useEndAsStart: false,
};

const colorScale = d3.interpolateCubehelixDefault;

function RatingDynamics() {
  const [period, setPeriod] = useState({
    start: moment().startOf("month"),
    end: moment(),
  });

  const [data, setData] = useState([]);

  const [specialists, setSpecialists] = useState([]);
  useEffect(() => {
    Specialist.getAll(({ data }) => setSpecialists(data));
  }, []);

  const [selected, setSelected] = useState([]);
  useEffect(() => {
    const success = ({ data }) => {
      data.forEach((i) => (i.specialist = selected.find((j) => j.id === i.id)));
      setData(data);
    };
    if (selected && selected.length > 0)
      Specialist.getRatingDynamics(
        {
          ...period,
          specialists: selected.map((i) => i.id),
        },
        success
      );
  }, [selected, period]);

  const onSelected = (values, success) => {
    setSelected(values.selected);
    success();
  };

  const days = () => {
    const { start, end } = period;
    const result = [];
    for (
      let date = start.clone();
      date.isSameOrBefore(end, "day");
      date.add(1, "day")
    )
      result.push(date.clone());
    return result;
  };

  const chartData = () => {
    const colors = interpolateColors(data.length, colorScale, colorRangeInfo);
    const dataX = days();
    const datasets = data.map((item, i) => {
      const { name, surname } = item.specialist;
      const dataY = [];
      let index = 0;
      item.ratingDynamics.forEach((j) => {
        while (!moment(j.date).isSameOrBefore(dataX[index], "day")) {
          dataY.push(null);
          index++;
        }
        dataY.push(j.rating);
        index++;
      });
      return {
        label: surname + " " + name,
        data: dataY,
        backgroundColor: colors[i],
        borderColor: colors[i],
        spanGaps: true,
      };
    });
    return {
      labels: dataX.map((i) => i.format("D.MM")),
      datasets: datasets,
    };
  };

  return (
    <>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div>
          <PeriodPicker value={period} onChange={setPeriod} />
        </div>
        <ModalButton
          title="Выберите специалистов"
          formId="select-specialists-form"
          onSubmit={onSelected}
          form={(props) => (
            <SelectSpecialistsForm
              id="select-specialists-form"
              list={specialists}
              initialValues={{ selected: selected }}
              {...props}
            />
          )}
        >
          <Button
            variant="contained"
            color="primary"
            style={{ height: "fit-content", width: "max-content" }}
          >
            Выбрать специалистов
          </Button>
        </ModalButton>
      </div>
      <div style={{ margin: "15px 0" }}>
        <Line data={chartData} options={options} />
      </div>
    </>
  );
}

export default RatingDynamics;
