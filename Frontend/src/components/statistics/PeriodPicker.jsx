import React from "react";
import moment from "moment";
import { KeyboardDatePicker } from "@material-ui/pickers";

function PeriodPicker({ value = {}, onChange = () => {} }) {
  const curValue = value ? value : {};
  const { start, end } = curValue;

  const pickerProps = {
    variant: "inline",
    inputVariant: "outlined",
    autoOk: true,
    openTo: "date",
    format: "DD.MM.yyyy",
    disableToolbar: true,
    InputLabelProps: { shrink: true },
    disableFuture: true,
  };

  return (
    <>
      <KeyboardDatePicker
        {...pickerProps}
        label="Начало"
        value={start ? start : moment()}
        onChange={(newStart) => {
          onChange({ start: newStart, end });
        }}
        style={{ marginRight: "15px" }}
      />
      <KeyboardDatePicker
        {...pickerProps}
        label="Конец"
        value={end ? end : moment()}
        onChange={(newEnd) => {
          onChange({ start, end: newEnd });
        }}
      />
    </>
  );
}

export default PeriodPicker;
