import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";

import LoadingButton from "../../UI/LoadingButton";
import GeneralAppointmentInfo from "./GeneralAppointmentInfo";
import MyTitle from "../../UI/MyTitle";
import ServicesTable from "../categories/ServicesTable";
import { Divider } from "@material-ui/core";

function ConfirmAppointmentDialog({
  open,
  loading,
  onOk = () => {},
  onCancel = () => {},
  appInfoList = [],
}) {
  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle id="simple-dialog-title" style={{ height: "60px" }}>
        <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
          Подтверждение записи
        </MyTitle>
      </DialogTitle>
      <DialogContent dividers>
        {appInfoList.map((appInfo, index) => {
          const isLast = index === appInfoList.length - 1;
          return (
            <>
              <GeneralAppointmentInfo {...appInfo} />
              <ServicesTable {...appInfo.cart} />
              {!isLast && <Divider style={{ margin: "20px -15px" }} />}
            </>
          );
        })}
      </DialogContent>
      <DialogActions>
        <LoadingButton
          onClick={onOk}
          color="primary"
          variant="contained"
          disabled={loading}
          autoFocus
          pending={loading}
        >
          Записаться на сеанс
        </LoadingButton>
        <Button onClick={onCancel} disabled={loading} color="primary">
          Отмена
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ConfirmAppointmentDialog;
