import React from "react";
import moment from "moment";

import { Avatar, Typography } from "@material-ui/core";

function GeneralAppointmentInfo({ specialist, date, beginning }) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        marginBottom: "15px",
      }}
    >
      <Avatar
        src={specialist.image}
        style={{ width: "100px", height: "100px", marginBottom: "10px" }}
      />
      <Typography variant="h6">
        {specialist.surname} {specialist.name} {specialist.fatherName}
      </Typography>
      <Typography variant="h6" style={{ color: "#745e8a", fontWeight: "bold" }}>
        {moment(date).format("DD MMMM YYYY")} в {beginning}
      </Typography>
    </div>
  );
}

export default GeneralAppointmentInfo;
