import React from "react";
import moment from "moment";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";

import { Typography } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";

import { formatDuration } from "../../common/methods";
import ModalButton from "../modals/ModalButton";
import RateAppointmentForm from "../modals/appointments/RateAppointmentForm";
import MyRating from "../../UI/MyRating";
import GeneralAppointmentInfo from "./GeneralAppointmentInfo";

function AppointmentsTable({
  appointments = [],
  onCancel = () => {},
  onRate = () => {},
}) {
  const renderRaiting = (appointment) => {
    const { canRate, rating, review, id } = appointment;
    if (rating !== null && rating !== undefined)
      return (
        <>
          <MyRating name="rating" value={rating} readOnly precision={0.01} />
          {canRate && (
            <ModalButton
              title="Оцените сеанс"
              form={(props) => (
                <>
                  <GeneralAppointmentInfo {...appointment} />{" "}
                  <RateAppointmentForm
                    id="edit-rate-app-form"
                    initialValues={{ rating, review }}
                    {...props}
                  />
                </>
              )}
              formId="edit-rate-app-form"
              onSubmit={(...props) => onRate(id, ...props)}
            >
              <Button
                edge="end"
                aria-haspopup="true"
                color="primary"
                variant="outlined"
                style={{ marginTop: "10px" }}
              >
                Изменить
              </Button>
            </ModalButton>
          )}
        </>
      );
    if (canRate)
      return (
        <ModalButton
          title="Оцените сеанс"
          form={(props) => (
            <>
              <GeneralAppointmentInfo {...appointment} />{" "}
              <RateAppointmentForm id="rate-app-form" {...props} />
            </>
          )}
          formId="rate-app-form"
          onSubmit={(...props) => onRate(id, ...props)}
        >
          <Button
            edge="end"
            aria-haspopup="true"
            color="primary"
            variant="outlined"
          >
            Оценить
          </Button>
        </ModalButton>
      );
    return (
      <Typography color="textSecondary">Оценка не была поставлена</Typography>
    );
  };
  const table = (
    <Table size="small" aria-label="a dense table">
      <TableHead>
        <TableRow>
          <TableCell>Дата и время</TableCell>
          <TableCell>Специалист</TableCell>
          <TableCell>Услуги</TableCell>

          <TableCell>Стоимость</TableCell>
          <TableCell>
            {appointments[0] && appointments[0].completed && "Ваша оценка"}
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {appointments.map((appointment) => (
          <TableRow key={appointment.id}>
            <TableCell
              component="th"
              scope="row"
              style={{ verticalAlign: "top" }}
            >
              <div style={{ margin: "15px 0" }}>
                <Typography
                  variant="h6"
                  style={{ color: "#745e8a", fontWeight: "bold" }}
                >
                  {moment(appointment.date).format("DD MMMM YYYY")} в{" "}
                  {appointment.beginning}
                </Typography>
                <Typography color="textSecondary">
                  {formatDuration(appointment.duration)}
                </Typography>
              </div>
            </TableCell>
            <TableCell style={{ verticalAlign: "top" }}>
              <div
                style={{
                  display: "flex",
                  margin: "15px 0",
                }}
              >
                <Avatar
                  src={appointment.specialist.image}
                  style={{ width: "70px", height: "70px" }}
                />
                <div style={{ display: "inline", marginLeft: "10px" }}>
                  <Typography>
                    {appointment.specialist.surname}{" "}
                    {appointment.specialist.name}
                  </Typography>
                  <Typography>{appointment.specialist.fatherName}</Typography>
                </div>
              </div>
            </TableCell>
            <TableCell align="left" style={{ verticalAlign: "top" }}>
              <div style={{ margin: "15px 0" }}>
                {appointment.services.map((service, index) => (
                  <Typography key={`service-${index}`}>{service}</Typography>
                ))}
              </div>
            </TableCell>

            <TableCell style={{ verticalAlign: "top" }}>
              <Typography
                variant="h6"
                style={{
                  color: "#745e8a",
                  fontWeight: "bold",
                  margin: "15px 0",
                }}
              >
                {appointment.price} ₽
              </Typography>
            </TableCell>
            <TableCell style={{ verticalAlign: "top", width: "120px" }}>
              <div
                style={{
                  margin: "15px 0",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                {!appointment.completed && (
                  <ModalButton
                    title="Отменить запись?"
                    onOk={(...props) => onCancel(appointment.id, ...props)}
                    form={() => <GeneralAppointmentInfo {...appointment} />}
                  >
                    <Button
                      edge="end"
                      aria-haspopup="true"
                      color="primary"
                      variant="outlined"
                    >
                      Отменить
                    </Button>
                  </ModalButton>
                )}
                {appointment.completed && renderRaiting(appointment)}
              </div>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );

  return <>{table}</>;
}

export default AppointmentsTable;
