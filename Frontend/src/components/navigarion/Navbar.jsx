import React from "react";
import { useLocation, Link, useHistory } from "react-router-dom";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { AppBar, Tabs, Tab, Typography, Toolbar } from "@material-ui/core";

import CartButton from "./CartButton";
import AccountButton from "./AccountButton";
import { Gradients } from "../../common/constants";
import { Routes } from "../../common/config";
import { isAdmin } from "../../common/methods";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  cartButton: {
    marginRight: theme.spacing(1),
  },
  root: {
    background: Gradients.main,
  },
}));

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#fff",
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    color: "#fff",
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(18),
    marginRight: theme.spacing(1),
    "&:focus": {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

function Navbar({ config }) {
  const classes = useStyles();
  const location = useLocation().pathname;
  const isOnMakeAppointmentPage = location === Routes.makeAppointment;
  const history = useHistory();
  const redirectToMain = () => history.push(Routes.categories);
  return (
    <AppBar position="static" className={classes.root} elevation={0}>
      <Toolbar>
        <div
          style={{ height: "100%", cursor: "pointer" }}
          onClick={redirectToMain}
        >
          <Typography variant="h5">BeautySalon</Typography>
        </div>
        <div className={classes.grow} />
        {!isOnMakeAppointmentPage && (
          <>
            <StyledTabs value={location} aria-label="simple tabs example">
              {config.map((item) => (
                <StyledTab
                  label={item.text}
                  component={Link}
                  to={item.path}
                  value={item.path}
                  key={item.path}
                />
              ))}
            </StyledTabs>
            {!isAdmin() && <CartButton className={classes.cartButton} />}
            <AccountButton />
          </>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Navbar;
