import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import Navbar from "./Navbar";
import { getNavbarConfig } from "../../common/methods";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(2),
  },
}));

function LayoutForRoutes({ children }) {
  const classes = useStyles();
  return (
    <>
      <Navbar config={getNavbarConfig()} />
      <Container className={classes.container} fixed>
        {children}
      </Container>
    </>
  );
}

export default LayoutForRoutes;
