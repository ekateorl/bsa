import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";

import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";

import MyTitle from "../../UI/MyTitle";
import MyIcon from "../../UI/MyIcon";
import ServicesTable from "../categories/ServicesTable";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    width: "100%",
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
    width: "100%",
  },
  progress: {
    color: "#ffb29d",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
  },
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
        {children}
      </MyTitle>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

function CartModal({
  open,
  onClose = () => {},
  onSubmit = () => {},
  loading,
  cart = { items: [] },
  onRemoveClick = (id) => {},
}) {
  const classes = useStyles();

  const { items, total } = cart;
  const handleClose = () => {
    onClose();
  };

  const handleButtonClick = () => {
    onSubmit();
  };

  const dialogContent = () => {
    if (items && items.length)
      return (
        <div className={classes.root}>
          <div className={classes.wrapper}>
            <ServicesTable
              items={items}
              total={total}
              onRemoveClick={onRemoveClick}
              loading={loading}
              removable
            />
            {loading && (
              <CircularProgress size={50} className={classes.progress} />
            )}
          </div>
        </div>
      );
    return (
      <div
        style={{
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <MyIcon
          url="/svg/sad.svg"
          style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
        />

        <Typography color="textSecondary" style={{ marginTop: "10px" }}>
          Грустно-пусто
        </Typography>
      </div>
    );
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
        Корзина
      </DialogTitle>

      <DialogContent dividers>{dialogContent()}</DialogContent>

      <DialogActions>
        <Button
          onClick={handleButtonClick}
          color="primary"
          variant="contained"
          style={{ width: "100%" }}
          disabled={!items || !items.length || loading}
        >
          Записаться на сеанс
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default CartModal;
