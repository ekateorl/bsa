import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useBus, { dispatch } from "use-bus";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import CartModal from "./CartModal";
import { Cart } from "../../api/methods";
import { Events } from "../../common/constants";
import { Routes } from "../../common/config";
import {
  getUserInfo,
  isUser,
  getLocalCart,
  setLocalCart,
  isAdmin,
} from "../../common/methods";

function CartButton({ ...props }) {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [cart, setCart] = useState({ items: [] });
  const [loading, setLoading] = useState(false);

  const getCart = () => {
    const success = ({ data }) => {
      setCart(data);
      setLoading(false);
    };
    const fail = () => setLoading(false);
    if (isUser()) Cart.get(success, fail);
    else {
      const services = getLocalCart();
      Cart.getLocal({ services }, success);
    }
  };

  useEffect(getCart, []);

  useBus(Events.addToCart, getCart, []);

  const removeLocalItem = (id) => {
    let cart = getLocalCart();
    cart = cart.filter((item) => item !== id);
    setLocalCart(cart);
    getCart();
    dispatch(Events.removeFromCart);
  };
  const removeItem = (id) => {
    if (!isUser()) removeLocalItem(id);
    else {
      setLoading(true);

      Cart.delete(id, () => {
        getCart();
        dispatch(Events.removeFromCart);
      });
    }
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const redirectToMakeAppointmentPage = () => {
    history.push(Routes.makeAppointment);
  };

  return (
    <>
      <IconButton
        edge="end"
        aria-label="account of current user"
        aria-haspopup="true"
        color="inherit"
        {...props}
        onClick={handleOpen}
        disabled={isAdmin()}
      >
        <Badge badgeContent={cart ? cart.items.length : 0} color="secondary">
          <ShoppingCartIcon />
        </Badge>
      </IconButton>

      <CartModal
        open={open}
        onSubmit={redirectToMakeAppointmentPage}
        onClose={() => setOpen(false)}
        loading={loading}
        cart={cart}
        onRemoveClick={removeItem}
      />
    </>
  );
}

export default CartButton;
