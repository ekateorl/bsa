import React, { useState } from "react";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { getUserInfo, removeUserInfo, isUser } from "../../common/methods";
import { useHistory } from "react-router-dom";
import { Routes } from "../../common/config";

function AccountButton({ ...props }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const Logout = () => {
    removeUserInfo();
    window.location.reload();
  };

  const redirectToLoginPage = () => {
    history.push(Routes.login);
  };

  const redirectToRegisterPage = () => {
    history.push(Routes.register);
  };

  const redirectToAppointmentsPage = () => {
    history.push(Routes.appointments);
  };

  const menuItems = () => {
    const user = getUserInfo();
    if (user)
      return (
        <>
          <MenuItem>Здравствуйте, {user.name}</MenuItem>
          {isUser() && (
            <MenuItem onClick={redirectToAppointmentsPage}>Мои записи</MenuItem>
          )}
          <MenuItem onClick={Logout}>Выйти</MenuItem>
        </>
      );
    return (
      <>
        <MenuItem onClick={redirectToLoginPage}>Войти</MenuItem>
        <MenuItem onClick={redirectToRegisterPage}>Зарегистрироваться</MenuItem>
      </>
    );
  };
  return (
    <>
      <IconButton
        edge="end"
        aria-controls="simple-menu"
        aria-label="account of current user"
        aria-haspopup="true"
        color="inherit"
        onClick={handleClick}
        {...props}
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        elevation={2}
      >
        {menuItems()}
      </Menu>
    </>
  );
}

export default AccountButton;
