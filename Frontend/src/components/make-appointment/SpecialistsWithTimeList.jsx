import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import SpecialistWithTime from "./SpecialistWithTime";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "fit-content",
    minHeight: "100px",
    position: "relative",
  },
  wrapper: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
  progress: {
    color: "#ffb29d",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
  },
}));

function SpecialistsWithTimeList({
  specialists = [],
  onSelectedChange = () => {},
  loading = false,
  selectedTimeSlot = -1,
}) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List>
        {specialists
          .sort((a, b) => b.specialist.rating - a.specialist.rating)
          .map((item, index) => (
            <>
              <SpecialistWithTime
                {...item}
                onSelectedChange={onSelectedChange}
                selectedTimeSlot={selectedTimeSlot}
              />
              {index < specialists.length - 1 && <Divider variant="middle" />}
            </>
          ))}
      </List>
      {loading && (
        <div className={classes.wrapper}>
          <CircularProgress size={50} className={classes.progress} />
        </div>
      )}
    </div>
  );
}

export default SpecialistsWithTimeList;
