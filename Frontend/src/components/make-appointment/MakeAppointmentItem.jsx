import React, { useState, useEffect } from "react";
import moment from "moment";

import { DatePicker } from "@material-ui/pickers";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";

import SpecialistsWithTimeList from "../../components/make-appointment/SpecialistsWithTimeList";
import { Specialist } from "../../api/methods";
import { Images } from "../../api/constants";
import ServicesTable from "../categories/ServicesTable";

function MakeAppointmentItem({
  group = {},
  selectedTimeSlot = -1,
  onSelectedChange = () => {},
}) {
  const { items = [], total = {} } = group;
  const [selectedDate, setSelectedDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [specialists, setSpecialists] = useState();
  const [loadingSpecialists, setLoadingSpecialists] = useState(false);

  const handleDateChange = (date) => {
    const newDate = date.format("YYYY-MM-DD");
    setSelectedDate(newDate);
  };

  const getSpecialists = (date = "") => {
    setLoadingSpecialists(true);
    Specialist.getSpecialistsWIthTime(
      { date, services: items.map((i) => i.id) },
      ({ data = [] }) => {
        data.forEach(
          (i) => (i.specialist.image = Images.specialist(i.specialist.image))
        );
        setSpecialists(data);
        setLoadingSpecialists(false);
      }
    );
  };

  useEffect(() => getSpecialists(selectedDate), [selectedDate]);

  return (
    <Grid container spacing={10}>
      <Grid item xs style={{ maxWidth: "500px" }}>
        <Card variant="outlined">
          <ServicesTable {...group} />
        </Card>
      </Grid>
      <Grid item xs>
        <DatePicker
          autoOk
          variant="inline"
          openTo="date"
          value={selectedDate}
          onChange={handleDateChange}
          disabled={loadingSpecialists}
          disableToolbar
          style={{ marginBottom: "5px" }}
          inputVariant="outlined"
          format="LL, dd"
          disablePast
        />
        <SpecialistsWithTimeList
          specialists={specialists}
          onSelectedChange={(data) =>
            onSelectedChange({ ...data, date: selectedDate })
          }
          loading={loadingSpecialists}
          selectedTimeSlot={selectedTimeSlot}
        />
      </Grid>
    </Grid>
  );
}

export default MakeAppointmentItem;
