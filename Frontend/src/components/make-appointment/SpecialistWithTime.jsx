import React from "react";
import moment from "moment";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { Avatar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MyTimeButton from "../../UI/MyTimeButton";
import MyRating from "../../UI/MyRating";

const useStyles = makeStyles((theme) => ({
  verticalDiv: {
    display: "flex",
    flexDirection: "column",
  },
  wrapPanel: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
  },
}));

function SpecialistWithTime({
  specialist,
  timeSlots,
  selectedTimeSlot = -1,
  status,
  closestDate,
  onSelectedChange = () => {},
}) {
  const classes = useStyles();
  const renderTime = () => {
    if (status === 0)
      return (
        <div className={classes.wrapPanel}>
          {timeSlots.map((timeSlot) => {
            const selected = timeSlot.id === selectedTimeSlot;
            const onChange = () => {
              if (!selected) onSelectedChange({ specialist, timeSlot });
              else onSelectedChange(undefined);
            };
            return (
              <MyTimeButton
                onChange={onChange}
                style={{
                  marginRight: "10px",
                  marginTop: "5px",
                  fontSize: "12pt",
                  width: "100px",
                }}
                selected={selected}
              >
                {timeSlot.beginning}
              </MyTimeButton>
            );
          })}
        </div>
      );
    let message = "";
    message = "В этот день у специалиста нет свободного времени.";
    if (closestDate)
      message +=
        " Ближайша свободная дата: " +
        moment(closestDate).format("D MMMM YYYY");
    else message = message + " Свободных дат нет";
    return <Typography color="textSecondary">{message}</Typography>;
  };

  return (
    <ListItem style={{ marginBottom: "10px", alignItems: "end" }}>
      <ListItemIcon style={{ marginTop: "10px" }}>
        <Avatar
          src={specialist.image}
          style={{ width: "70px", height: "70px", marginRight: "25px" }}
        />
      </ListItemIcon>
      <ListItemText
        primary={
          <>
            <Typography variant="h5">
              {specialist.surname} {specialist.name} {specialist.fatherName}
            </Typography>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginBottom: "10px",
              }}
            >
              <MyRating value={specialist.rating} readOnly precision={0.01} />
              <Typography
                color="textSecondary"
                style={{ display: "inline", marginLeft: "10px" }}
              >
                {specialist.rating} / 5
              </Typography>
            </div>
          </>
        }
        secondary={renderTime()}
      ></ListItemText>
      <ListItemText>{}</ListItemText>
    </ListItem>
  );
}

export default SpecialistWithTime;
