import React from "react";
import { Formik, Field } from "formik";
import TextField from "@material-ui/core/TextField";
import MyRating from "../../../UI/MyRating";

const RateAppointmentForm = ({ handleSubmit, id, initialValues = {} }) => {
  return (
    <Formik
      initialValues={{
        rating: 0,
        review: "",
        ...initialValues,
      }}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmit(values, setSubmitting);
      }}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        const itemProps = (name = "") => {
          return {
            error: errors[name] && touched[name],
            helperText: errors[name] && touched[name] && errors[name],
            name: name,
            required: true,
            value: values[name],
            onChange: handleChange,
            onBlur: handleBlur,
            margin: "normal",
            fullWidth: true,
            variant: "outlined",
          };
        };
        return (
          <form onSubmit={handleSubmit} id={id} noValidate>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <Field name="rating" id="rating">
                {({ field: { value }, form: { setFieldValue } }) => (
                  <MyRating
                    value={value}
                    onChange={(_, newRating) => {
                      if (!newRating) newRating = 0;
                      setFieldValue("rating", newRating);
                    }}
                    size="large"
                  />
                )}
              </Field>
            </div>
            <TextField
              label="Отзыв"
              multiline
              {...itemProps("review")}
              required={false}
            />
          </form>
        );
      }}
    </Formik>
  );
};
export default RateAppointmentForm;
