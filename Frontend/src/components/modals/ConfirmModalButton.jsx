import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";

import DialogTitle from "@material-ui/core/DialogTitle";

import Button from "@material-ui/core/Button";

import MyTitle from "../../UI/MyTitle";
import LoadingButton from "../../UI/LoadingButton";

function ConfirmModalButton({
  children,
  title = "",
  text = "",
  okText = "OK",
  cancelText = "Отмена",

  onOk = () => {},
}) {
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const handleOk = () => {
    setLoading(true);
    onOk(
      () => {
        setOpen(false);
        setLoading(false);
      },
      (errors) => {
        setLoading(false);
      }
    );
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <>
      <div
        onClick={handleOpen}
        style={{ width: "min-content", display: "inline" }}
      >
        {children}
      </div>
      <Dialog open={open} maxWidth="sm" fullWidth>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
            {title}
          </MyTitle>
        </DialogTitle>

        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {text}
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          <LoadingButton
            color="primary"
            variant="contained"
            disabled={loading}
            pending={loading}
            onClick={handleOk}
          >
            {okText}
          </LoadingButton>
          <Button onClick={handleClose} color="primary" disabled={loading}>
            {cancelText}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default ConfirmModalButton;
