import React from "react";
import { Formik } from "formik";
import TextField from "@material-ui/core/TextField";

const CancelAppointmentForm = ({ handleSubmit, id, initialValues = {} }) => {
  return (
    <Formik
      initialValues={{
        reason: "",
        ...initialValues,
      }}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmit(values, setSubmitting);
      }}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        const itemProps = (name = "") => {
          return {
            error: errors[name] && touched[name],
            helperText: errors[name] && touched[name] && errors[name],
            name: name,
            required: true,
            value: values[name],
            onChange: handleChange,
            onBlur: handleBlur,
            margin: "normal",
            fullWidth: true,
            variant: "outlined",
          };
        };
        return (
          <form onSubmit={handleSubmit} id={id} noValidate>
            <TextField
              label="Причина отмены"
              multiline
              {...itemProps("reason")}
            />
          </form>
        );
      }}
    </Formik>
  );
};
export default CancelAppointmentForm;
