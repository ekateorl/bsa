import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Typography from "@material-ui/core/Typography";

import MyIcon from "../../../UI/MyIcon";

const StyledRadio = withStyles({
  root: {
    "&$checked": {
      color: "#ffb29d",
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

function TemplateRadio({ list = [], value = 0, onChange = () => {} }) {
  const isEmpty = !list || !list.length;

  return (
    <>
      {isEmpty && (
        <div
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <MyIcon
            url="/svg/empty.svg"
            style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
          />

          <Typography color="textSecondary" style={{ marginTop: "10px" }}>
            Выбирать не из чего
          </Typography>
        </div>
      )}
      {!isEmpty && (
        <List>
          {list.map((item, i) => (
            <ListItem
              onClick={() => onChange(i)}
              button
              key={`specialist-list-item-${item.id}`}
            >
              <ListItemText primary={item.name} />
              <ListItemIcon>
                <StyledRadio
                  edge="start"
                  checked={i === value}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{
                    "aria-labelledby": `specialist-item-${item.id}`,
                  }}
                />
              </ListItemIcon>
            </ListItem>
          ))}
        </List>
      )}
    </>
  );
}

export default TemplateRadio;
