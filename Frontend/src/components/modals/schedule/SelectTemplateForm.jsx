import React from "react";
import { Formik, Field } from "formik";
import TemplateRadio from "./TemplateRadio";

const SelectTemplateForm = ({
  handleSubmit,
  id,
  initialValues = {
    selected: 0,
  },
  list,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleSubmit(values);
      }}
    >
      {({ handleSubmit }) => {
        return (
          <form onSubmit={handleSubmit} id={id}>
            <Field name="selected" id="selected">
              {({ field: { value }, form: { setFieldValue } }) => (
                <TemplateRadio
                  list={list}
                  value={value}
                  onChange={(selected) => setFieldValue("selected", selected)}
                />
              )}
            </Field>
          </form>
        );
      }}
    </Formik>
  );
};

export default SelectTemplateForm;
