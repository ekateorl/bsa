import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";

const AddEditTemplateForm = ({ handleSubmit, id, initialValues = {} }) => {
  return (
    <Formik
      enableReinitialize
      initialValues={{
        name: "",
        ...initialValues,
      }}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmit(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required("Пожалуйста, заполните поле"),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        return (
          <form onSubmit={handleSubmit} id={id} noValidate>
            <TextField
              error={errors.name && touched.name}
              helperText={errors.name && touched.name && errors.name}
              label="Название"
              name="name"
              required
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              margin="normal"
              fullWidth
              variant="outlined"
              InputLabelProps={{ shrink: true }}
            />
          </form>
        );
      }}
    </Formik>
  );
};
export default AddEditTemplateForm;
