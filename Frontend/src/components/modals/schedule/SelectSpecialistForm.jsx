import React from "react";
import { Formik, Field } from "formik";
import SpecialistRadio from "./SpecialistRadio";

const SelectSpecialistsForm = ({
  handleSubmit,
  id,
  initialValues = {
    selected: 0,
  },
  list,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleSubmit(values);
      }}
    >
      {({ handleSubmit }) => {
        return (
          <form onSubmit={handleSubmit} id={id}>
            <Field name="selected" id="selected">
              {({ field: { value }, form: { setFieldValue } }) => (
                <SpecialistRadio
                  list={list}
                  value={value}
                  onChange={(selected) => setFieldValue("selected", selected)}
                />
              )}
            </Field>
          </form>
        );
      }}
    </Formik>
  );
};

export default SelectSpecialistsForm;
