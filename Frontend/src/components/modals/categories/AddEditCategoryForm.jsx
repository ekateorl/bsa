import React from "react";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import ImageInput from "./ImageInput";

const AddEditCategoryForm = ({ handleSubmit, id, initialValues = {} }) => {
  return (
    <Formik
      initialValues={{
        name: "",
        image: undefined,
        file: undefined,
        ...initialValues,
      }}
      onSubmit={(values, { setSubmitting }) => {
        const data = new FormData();
        Object.keys(values).forEach((key) => {
          if (key === "file") {
            if (values.file) data.append("file", values.file, values.file.name);
            return;
          }
          data.append(key, values[key]);
        });

        handleSubmit(data, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required("Пожалуйста, заполните поле"),
        image: Yup.string(),
        file: Yup.mixed(),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        return (
          <form onSubmit={handleSubmit} id={id} noValidate>
            <Field name="file" id="file">
              {({ field: { value }, form: { setFieldValue } }) => (
                <ImageInput
                  value={values.image}
                  onDelete={() => {
                    setFieldValue("imageDelete", true);
                    setFieldValue("image", undefined);
                  }}
                  onChange={(img, file) => {
                    setFieldValue("imageDelete", false);
                    setFieldValue("image", img);
                    setFieldValue("file", file);
                  }}
                />
              )}
            </Field>
            <TextField
              error={errors.name && touched.name}
              helperText={errors.name && touched.name && errors.name}
              label="Название"
              name="name"
              required
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              margin="normal"
              fullWidth
              variant="outlined"
            />
          </form>
        );
      }}
    </Formik>
  );
};
export default AddEditCategoryForm;
