import React, { useState, useEffect } from "react";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputAdornment from "@material-ui/core/InputAdornment";

import TypeSelect from "../../TypeSelect";
import { Category } from "../../../api/methods";
import DurationInput from "./DurationInput";
import SpecialistsInput from "./SpecialistsInput";

function AddEditServiceForm({ handleSubmit, id, initialValues = {} }) {
  const [categories, setCategories] = useState([]);
  const [loadigCategories, setLoadigCategories] = useState(false);

  const getCategories = () => {
    setLoadigCategories(true);
    Category.getAll(({ data }) => {
      setLoadigCategories(false);
      setCategories(data);
    });
  };

  useEffect(getCategories, []);

  return (
    <Formik
      initialValues={{
        categoryFk: 0,
        name: "",
        price: 0,
        duration: "00:00",
        description: "",
        specialists: [],
        ...initialValues,
      }}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmit(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        categoryFk: Yup.string().required("Пожалуйста, заполните поле"),
        name: Yup.string().required("Пожалуйста, заполните поле"),
        price: Yup.string().required("Пожалуйста, заполните поле"),
        duration: Yup.string().required("Пожалуйста, заполните поле"),
        description: Yup.string().nullable(),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        const itemProps = (name = "") => {
          return {
            error: errors[name] && touched[name],
            helperText: errors[name] && touched[name] && errors[name],
            name: name,
            required: true,
            value: values[name],
            onChange: handleChange,
            onBlur: handleBlur,
            margin: "normal",
            fullWidth: true,
            variant: "outlined",
          };
        };
        return (
          <form onSubmit={handleSubmit} id={id} noValidate>
            <TypeSelect
              types={categories}
              {...itemProps("categoryFk")}
              label="Категория"
            />
            <TextField {...itemProps("name")} label="Название" />
            <TextField
              type="number"
              InputProps={{
                inputProps: { min: 0 },
                startAdornment: (
                  <InputAdornment position="start">₽</InputAdornment>
                ),
              }}
              {...itemProps("price")}
              value={values.price.toString()}
              label="Цена"
            />
            <FormControl
              component="fieldset"
              required
              fullWidth
              style={{ marginTop: "15px" }}
            >
              <FormLabel component="legend" style={{ marginBottom: "10px" }}>
                Длительность
              </FormLabel>
              <Field name="duration" id="duration" type="string">
                {({ field: { value }, form: { setFieldValue } }) => (
                  <DurationInput
                    value={value}
                    onChange={(duration) => setFieldValue("duration", duration)}
                  />
                )}
              </Field>
              <FormHelperText>
                {errors["duration"] &&
                  touched["duration"] &&
                  errors["duration"]}
              </FormHelperText>
            </FormControl>

            <TextField
              label="Описание"
              multiline
              {...itemProps("description")}
              required={false}
            />

            <FormControl
              component="fieldset"
              fullWidth
              style={{ marginTop: "15px" }}
            >
              <FormLabel component="legend" style={{ marginBottom: "10px" }}>
                Специалисты
              </FormLabel>
              <Field name="specialists" id="specialists">
                {({ field: { value }, form: { setFieldValue } }) => (
                  <SpecialistsInput
                    value={value}
                    onChange={(specialists) =>
                      setFieldValue("specialists", specialists)
                    }
                  />
                )}
              </Field>
              <FormHelperText>
                {errors["specialists"] &&
                  touched["specialists"] &&
                  errors["specialists"]}
              </FormHelperText>
            </FormControl>
          </form>
        );
      }}
    </Formik>
  );
}

export default AddEditServiceForm;
