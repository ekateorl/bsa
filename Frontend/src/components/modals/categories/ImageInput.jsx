import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import ImageIcon from "@material-ui/icons/Image";
import Alert from "@material-ui/lab/Alert";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(1),
  },
}));

const ImageInput = ({ value, onDelete = () => {}, onChange = () => {} }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const isSvg = file.type === "image/svg+xml";
      if (isSvg) {
        const img = URL.createObjectURL(file);
        onChange(img, file);
      } else setOpen(true);
    }
  };

  return (
    <FormControl component="fieldset" fullWidth style={{ marginTop: "15px" }}>
      <FormLabel component="legend" style={{ marginBottom: "10px" }}>
        Изображение
      </FormLabel>
      <Collapse in={open}>
        <Alert
          className={classes.root}
          severity="info"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          Вы можете загружать только .svg файлы
        </Alert>
      </Collapse>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <Avatar
          src={value}
          variant="rounded"
          style={{
            marginRight: "10px",
            width: "100px",
            height: "100px",
          }}
        >
          <ImageIcon
            style={{
              width: "50px",
              height: "50px",
            }}
          />
        </Avatar>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            margin: "0 0 0 auto",
          }}
        >
          <input
            style={{ display: "none" }}
            id="contained-button-file"
            type="file"
            accept=".svg"
            onChange={(event) => {
              handleChange(event);
            }}
            margin="normal"
          />
          <label htmlFor="contained-button-file">
            <Button
              variant="outlined"
              color="primary"
              component="span"
              style={{ marginBottom: "10px" }}
            >
              Загрузить новое изображение
            </Button>
          </label>

          <Button variant="outlined" color="primary" onClick={onDelete}>
            Удалить
          </Button>
        </div>
      </div>
    </FormControl>
  );
};
export default ImageInput;
