import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Typography from "@material-ui/core/Typography";

import Avatar from "@material-ui/core/Avatar";

import MyIcon from "../../../UI/MyIcon";
import { Images } from "../../../api/constants";

const StyledCheckbox = withStyles({
  root: {
    "&$checked": {
      color: "#ffb29d",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

function SpecialistsSelect({ list = [], value = [], onChange = () => {} }) {
  const isEmpty = !list || !list.length;
  const handleToggle = (item) => {
    const currentIndex = value.findIndex(
      (specialist) => specialist.id === item.id
    );
    const newChecked = [...value];
    if (currentIndex === -1) {
      newChecked.push(item);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    onChange(newChecked);
  };
  return (
    <>
      {isEmpty && (
        <div
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <MyIcon
            url="/svg/empty.svg"
            style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
          />

          <Typography color="textSecondary" style={{ marginTop: "10px" }}>
            Выбирать больше не из чего
          </Typography>
        </div>
      )}
      {!isEmpty && (
        <List>
          {list.map((specialist) => (
            <ListItem
              onClick={() => handleToggle(specialist)}
              button
              key={`specialist-list-item-${specialist.id}`}
            >
              <ListItemAvatar>
                <Avatar src={Images.specialist(specialist.image)} />
              </ListItemAvatar>
              <ListItemText
                primary={`${specialist.surname} ${specialist.name} ${specialist.fatherName}`}
              />
              <ListItemIcon>
                <StyledCheckbox
                  edge="start"
                  checked={
                    value.findIndex((item) => item.id === specialist.id) !== -1
                  }
                  tabIndex={-1}
                  disableRipple
                  inputProps={{
                    "aria-labelledby": `specialist-item-${specialist.id}`,
                  }}
                />
              </ListItemIcon>
            </ListItem>
          ))}
        </List>
      )}
    </>
  );
}

export default SpecialistsSelect;
