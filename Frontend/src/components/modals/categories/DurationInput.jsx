import React from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

function DurationInput({ value, onChange = () => {} }) {
  if (!value) value = "0:0";
  const time = value.split(":");
  let mins = Number(time[1]);
  if (mins > 59) mins = 59;
  const hours = Number(time[0]);
  const handleChange = (hours, mins) => {
    const hoursStr = `${hours}`.padStart(2, "0");
    const minsStr = `${mins}`.padStart(2, "0");
    onChange(`${hoursStr}:${minsStr}`);
  };

  const handleHoursChange = ({ target }) => {
    handleChange(target.value, mins);
  };

  const handleMinsChange = ({ target }) => {
    handleChange(hours, target.value);
  };
  return (
    <div style={{ display: "flex" }}>
      <TextField
        InputProps={{
          inputProps: { min: 0 },
          endAdornment: <InputAdornment position="end">ч</InputAdornment>,
        }}
        variant="outlined"
        type="number"
        value={hours.toString()}
        onChange={handleHoursChange}
        fullWidth
      />
      <TextField
        InputProps={{
          inputProps: { min: 0, max: 59 },
          endAdornment: <InputAdornment position="end">мин</InputAdornment>,
        }}
        variant="outlined"
        type="number"
        value={mins.toString()}
        onChange={handleMinsChange}
        style={{ display: "inline", marginLeft: "10px" }}
        fullWidth
      />
    </div>
  );
}

export default DurationInput;
