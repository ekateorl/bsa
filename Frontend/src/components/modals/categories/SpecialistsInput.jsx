import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";

import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import ModalButton from "../ModalButton";
import SelectSpecialistsForm from "./SelectSpecialistsForm";
import { exclude } from "../../../common/methods";
import { Specialist } from "../../../api/methods";
import { Images } from "../../../api/constants";

function SpecialistsInput({ value = [], onChange = () => {} }) {
  if (!value) value = [];
  const inputValue = value;
  const [specialists, setSpecialists] = useState([]);

  useEffect(() => Specialist.getAll(({ data }) => setSpecialists(data)), []);

  const onSelected = (values, success) => {
    onChange([...value, ...values.selected]);
    success();
  };

  const removeSpecialist = (id = -1) => {
    const newValue = value.filter((i) => i.id !== id);
    onChange(newValue);
  };

  return (
    <>
      <List>
        {value.map(({ id, name, surname, fatherName, image }) => (
          <ListItem>
            <ListItemAvatar>
              <Avatar src={Images.specialist(image)} />
            </ListItemAvatar>
            <ListItemText primary={`${surname} ${name} ${fatherName}`} />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => removeSpecialist(id)}
              >
                <RemoveCircleOutlineIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
      <div style={{ width: "100%" }}>
        <ModalButton
          title="Выберите специалистов"
          formId="select-specialists-form"
          onSubmit={onSelected}
          form={(props) => (
            <SelectSpecialistsForm
              id="select-specialists-form"
              list={exclude(specialists, inputValue)}
              {...props}
            />
          )}
        >
          <Button variant="outlined" style={{ width: "100%" }}>
            Добавить
          </Button>
        </ModalButton>
      </div>
    </>
  );
}

export default SpecialistsInput;
