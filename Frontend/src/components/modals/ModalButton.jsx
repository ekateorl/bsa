import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import Button from "@material-ui/core/Button";

import MyTitle from "../../UI/MyTitle";
import LoadingButton from "../../UI/LoadingButton";

function ModalButton({
  children,
  title = "",
  okText = "OK",
  cancelText = "Отмена",
  form = () => {},
  formId,
  onSubmit = () => {},
  onOk,
  disabled = false,
}) {
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const handleSubmit = (values, setSubmitting = () => {}) => {
    console.log(values);
    setSubmitting(true);
    setLoading(true);
    onSubmit(
      values,
      () => {
        setOpen(false);
        setLoading(false);
        setSubmitting(false);
      },
      (errors) => {
        setSubmitting(false);
        setLoading(false);
      }
    );
  };

  const handleOk = () => {
    if (onOk) {
      setLoading(true);
      onOk(
        () => {
          setOpen(false);
          setLoading(false);
        },
        (errors) => {
          setLoading(false);
        }
      );
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    if (!disabled) setOpen(true);
  };

  return (
    <>
      <div
        onClick={handleOpen}
        style={{ width: "min-content", display: "inline" }}
      >
        {children}
      </div>
      <Dialog open={open} maxWidth="sm" fullWidth>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <MyTitle variant="h5" style={{ marginBottom: "0px" }}>
            {title}
          </MyTitle>
        </DialogTitle>

        <DialogContent dividers>{form({ handleSubmit })}</DialogContent>

        <DialogActions>
          <div onClick={handleOk}>
            <LoadingButton
              color="primary"
              variant="contained"
              type="submit"
              form={formId}
              id={`submit-for-${formId}`}
              pending={loading}
            >
              {okText}
            </LoadingButton>
          </div>
          <Button onClick={handleClose} color="primary" disabled={loading}>
            {cancelText}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default ModalButton;
