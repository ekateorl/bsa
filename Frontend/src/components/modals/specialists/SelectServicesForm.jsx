import React, { useEffect, useState } from "react";
import { Formik, Field } from "formik";
import ServicesSelect from "./ServicesSelect";
import { Category, Service } from "../../../api/methods";
import { exclude } from "../../../common/methods";
import TypeSelect from "../../TypeSelect";

const SelectServicesForm = ({
  handleSubmit,
  id,
  initialValues = {
    selected: [],
  },
  listToExclude = [],
}) => {
  const [category, setCategory] = useState(-1);
  const [categories, setCategories] = useState([]);
  const [services, setServices] = useState([]);
  const [loading, setLoading] = useState(true);

  const getServices = (categoryId = -1) => {
    setLoading(true);
    Service.getByCategory(
      { categoryId },
      ({ data }) => {
        setServices(data);
        setLoading(false);
      },
      () => {
        setServices([]);
        setLoading(false);
      }
    );
  };

  const getCategories = () => {
    Category.getAll(({ data }) => {
      setCategories(data);
      if (data && data[0]) {
        getServices(data[0].id);
        setCategory(data[0].id);
      }
    });
  };

  const handleChange = ({ target }) => {
    getServices(target.value);
    setCategory(target.value);
  };

  useEffect(() => getCategories(), []);
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleSubmit(values);
      }}
    >
      {({ handleSubmit }) => {
        return (
          <form onSubmit={handleSubmit} id={id}>
            <TypeSelect
              types={categories}
              onChange={handleChange}
              value={category}
              fullWidth
              variant="outlined"
            />
            <Field name="selected" id="selected">
              {({ field: { value }, form: { setFieldValue } }) => (
                <ServicesSelect
                  list={exclude(services, listToExclude)}
                  value={value}
                  onChange={(selected) => setFieldValue("selected", selected)}
                  loading={loading}
                />
              )}
            </Field>
          </form>
        );
      }}
    </Formik>
  );
};

export default SelectServicesForm;
