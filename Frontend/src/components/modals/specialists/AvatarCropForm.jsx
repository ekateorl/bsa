import React from "react";
import { Formik, Field } from "formik";
import AvatarCrop from "./AvatarCrop";

const AvatarCropForm = ({ handleSubmit, id }) => {
  return (
    <Formik
      initialValues={{ file: null }}
      onSubmit={(values) => {
        handleSubmit(values);
      }}
    >
      {({ handleSubmit }) => {
        return (
          <form onSubmit={handleSubmit} id={id}>
            <Field name="selected" id="selected">
              {({ form: { setFieldValue } }) => (
                <AvatarCrop
                  onChange={(cropped) => setFieldValue("file", cropped)}
                />
              )}
            </Field>
          </form>
        );
      }}
    </Formik>
  );
};

export default AvatarCropForm;
