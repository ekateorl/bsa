import React, { useState } from "react";
import Avatar from "react-avatar-edit";

function AvatarCrop({ onChange = () => {} }) {
  const [file, setFile] = useState();

  const onCrop = (cropped) => {
    onChange(cropped);
  };

  const onClose = () => {
    onChange(null);
  };

  const onBeforeFileLoad = (elem) => {
    setFile(elem.target.files[0]);
  };

  return (
    <Avatar
      height={295}
      onCrop={onCrop}
      onClose={onClose}
      onBeforeFileLoad={onBeforeFileLoad}
      label="Выберите файл"
      labelStyle={{
        fontFamily: '"Didact Gothic"',
        fontSize: "16pt",
        color: "#bdbdbd",
      }}
      src={file}
      width={"100%"}
      exportAsSquare
    />
  );
}

export default AvatarCrop;
