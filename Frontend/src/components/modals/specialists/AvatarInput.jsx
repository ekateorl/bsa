import React from "react";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import ModalButton from "../ModalButton";
import AvatarCropForm from "./AvatarCropForm";

const AvatarInput = ({ value, onDelete = () => {}, onChange = () => {} }) => {
  const handleChange = (file) => {
    if (file) {
      onChange(file, file);
    }
  };

  return (
    <FormControl component="fieldset" fullWidth style={{ marginTop: "15px" }}>
      <FormLabel component="legend" style={{ marginBottom: "10px" }}>
        Изображение
      </FormLabel>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <Avatar
          src={value}
          style={{
            marginRight: "10px",
            width: "100px",
            height: "100px",
          }}
        />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            margin: "0 0 0 auto",
          }}
        >
          <ModalButton
            title="Загрузка нового изображения"
            form={(props) => (
              <AvatarCropForm id="avatar-crop-form" {...props} />
            )}
            formId="avatar-crop-form"
            onSubmit={(values, success) => {
              handleChange(values.file);
              success();
            }}
          >
            <Button
              variant="outlined"
              color="primary"
              style={{ marginBottom: "10px", width: "max-content" }}
            >
              Загрузить новое изображение
            </Button>
          </ModalButton>
          <Button variant="outlined" color="primary" onClick={onDelete}>
            Удалить
          </Button>
        </div>
      </div>
    </FormControl>
  );
};
export default AvatarInput;
