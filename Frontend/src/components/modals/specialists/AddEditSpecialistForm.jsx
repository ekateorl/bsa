import React from "react";
import moment from "moment";

import { Formik, Field } from "formik";
import * as Yup from "yup";

import TextField from "@material-ui/core/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import { KeyboardDatePicker } from "@material-ui/pickers";
import InputMask from "react-input-mask";
import ServicesInput from "./ServicesInput";
import AvatarInput from "./AvatarInput";

const positions = [
  { id: 0, name: "Парикмахер" },
  { id: 1, name: "Мастер маникюра" },
  { id: 2, name: "Массажист" },
  { id: 3, name: "Массажист" },
  { id: 4, name: "Массажист" },
  { id: 5, name: "Массажист" },
];

function DataURIToBlob(dataURI) {
  const splitDataURI = dataURI.split(",");
  const byteString =
    splitDataURI[0].indexOf("base64") >= 0
      ? atob(splitDataURI[1])
      : decodeURI(splitDataURI[1]);
  const mimeString = splitDataURI[0].split(":")[1].split(";")[0];

  const ia = new Uint8Array(byteString.length);
  for (let i = 0; i < byteString.length; i++) ia[i] = byteString.charCodeAt(i);

  return new Blob([ia], { type: mimeString });
}

const AddEditSpecialistForm = ({ handleSubmit, id, initialValues = {} }) => {
  const ref = React.createRef();

  return (
    <Formik
      initialValues={{
        surname: "",
        name: "",
        fatherName: "",
        image:
          "http://pm1.narvii.com/7155/42cda5fca6be5d2a749997f64ae756487b77fe03r1-1200-849v2_uhq.jpg",
        imageDelete: false,
        file: null,
        phone: "",
        startDate: moment().format("YYYY-MM-DD"),
        services: [],
        position: [],
        ...initialValues,
      }}
      onSubmit={(values = {}, { setSubmitting }) => {
        const data = new FormData();
        Object.keys(values).forEach((key) => {
          if (key === "file") {
            if (values.file)
              data.append("file", DataURIToBlob(values.file), "image.jpg");
            return;
          }
          if (key === "services") {
            values.services.forEach((i) => data.append("services[]", i.id));
            return;
          }
          if (key === "startDate") {
            data.append(
              "startDate",
              moment(values.startDate).format("YYYY-MM-DD")
            );
            return;
          }
          data.append(key, values[key]);
        });
        handleSubmit(data, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        surname: Yup.string().nullable().required("Пожалуйста, заполните поле"),
        name: Yup.string().required("Пожалуйста, заполните поле"),
        image: Yup.string(),
        phone: Yup.string()
          .required("Пожалуйста, заполните поле")
          .test(
            "test-mask",
            "Пожалуйста, заполните поле",
            function (value = "") {
              return !value.includes("_");
            }
          ),
        startDate: Yup.date()
          .nullable()
          .required("Пожалуйста, заполните поле")
          .typeError("Неверный формат даты"),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        } = props;
        const itemProps = (name = "") => {
          return {
            error: errors[name] && touched[name],
            helperText: errors[name] && touched[name] && errors[name],
            name: name,
            required: true,
            value: values[name],
            onChange: handleChange,
            onBlur: handleBlur,
            margin: "normal",
            fullWidth: true,
            variant: "outlined",
          };
        };
        return (
          <form onSubmit={handleSubmit} id={id} noValidate ref={ref}>
            <Field name="startDate" id="startDate">
              {({ field: { value }, form: { setFieldValue } }) => (
                <AvatarInput
                  value={values.image}
                  onDelete={() => {
                    setFieldValue("imageDelete", true);
                    setFieldValue("image", undefined);
                  }}
                  onChange={(img, file) => {
                    setFieldValue("imageDelete", false);
                    setFieldValue("image", img);
                    setFieldValue("file", file);
                  }}
                />
              )}
            </Field>
            <TextField label="Фамилия" {...itemProps("surname")} />
            <TextField label="Имя" {...itemProps("name")} />
            <TextField
              label="Отчество"
              {...itemProps("fatherName")}
              required={false}
            />

            <FormControl
              component="fieldset"
              fullWidth
              style={{ marginTop: "10px" }}
            >
              <InputMask
                mask="+7-999-999-99-99"
                {...itemProps("phone")}
                disabled={false}
                maskChar="_"
              >
                {(inputProps) => (
                  <TextField
                    label="Телефон"
                    variant="outlined"
                    {...inputProps}
                  />
                )}
              </InputMask>
            </FormControl>

            <Field name="startDate" id="startDate">
              {({ field: { value }, form: { setFieldValue } }) => (
                <KeyboardDatePicker
                  {...itemProps("startDate")}
                  label="Дата начала работы"
                  variant="inline"
                  inputVariant="outlined"
                  autoOk
                  openTo="date"
                  format="DD.MM.yyyy"
                  disableToolbar
                  value={value}
                  InputLabelProps={{ shrink: true }}
                  onChange={(startDate) => {
                    setFieldValue("startDate", startDate);
                  }}
                />
              )}
            </Field>

            <FormControl
              component="fieldset"
              fullWidth
              style={{ marginTop: "15px" }}
            >
              <FormLabel component="legend" style={{ marginBottom: "10px" }}>
                Услуги
              </FormLabel>
              <Field name="services" id="services">
                {({ field: { value }, form: { setFieldValue } }) => (
                  <ServicesInput
                    value={value}
                    onChange={(services) => setFieldValue("services", services)}
                  />
                )}
              </Field>
              <FormHelperText>
                {errors["services"] &&
                  touched["services"] &&
                  errors["services"]}
              </FormHelperText>
            </FormControl>
          </form>
        );
      }}
    </Formik>
  );
};
export default AddEditSpecialistForm;
