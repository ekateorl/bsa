import React from "react";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";

import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import ModalButton from "../ModalButton";
import SelectSpecialistsForm from "./SelectServicesForm";

function ServicesInput({ value = [], onChange = () => {} }) {
  if (!value) value = [];
  const inputValue = value;

  const onSelected = (values, success) => {
    onChange([...value, ...values.selected]);
    success();
  };

  const removeItem = (id = -1) => {
    const newValue = value.filter((i) => i.id !== id);
    onChange(newValue);
  };

  return (
    <>
      <List>
        {value.map(({ id, name }) => (
          <ListItem>
            <ListItemText primary={name} />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => removeItem(id)}
              >
                <RemoveCircleOutlineIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
      <div style={{ width: "100%" }}>
        <ModalButton
          title="Выберите услуги"
          formId="select-specialists-form"
          onSubmit={onSelected}
          form={(props) => (
            <SelectSpecialistsForm
              id="select-specialists-form"
              listToExclude={inputValue}
              {...props}
            />
          )}
        >
          <Button variant="outlined" style={{ width: "100%" }}>
            Добавить
          </Button>
        </ModalButton>
      </div>
    </>
  );
}

export default ServicesInput;
