import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import { serviceStatuses } from "../../common/constants";
import Tooltip from "@material-ui/core/Tooltip";
import { formatDuration, isAdmin } from "../../common/methods";
import ModalButton from "../modals/ModalButton";
import AddEditServiceForm from "../modals/categories/AddEditServiceForm";
import { Service } from "../../api/methods";
import ConfirmModalButton from "../modals/ConfirmModalButton";
import ServiceRow from "./ServiceRow";

function ServiceList({
  services = [],
  onAddToCartClick = (serviceId) => {},
  loading = false,
  reload = () => {},
}) {
  const renderToolTip = (component, status) => {
    let message = "";
    if (status === serviceStatuses.alreadyAdded)
      message = "Эта услуга уже есть в корзине";
    if (status === serviceStatuses.available || message === "")
      return component;
    return (
      <Tooltip
        title={
          <Typography variant="body2" style={{ textAlign: "center" }}>
            {message}
          </Typography>
        }
      >
        <span>{component}</span>
      </Tooltip>
    );
  };

  const updateService = (id, service, success, fail) => {
    const specialists = service.specialists.map((i) => i.id);
    Service.update(
      id,
      { ...service, specialists },
      () => {
        success();
        reload();
      },
      fail
    );
  };

  const deleteService = (id = -1) => {
    Service.delete(id, () => {
      reload();
    });
  };

  const renderButtons = (service) => {
    if (isAdmin())
      return (
        <>
          <ModalButton
            title="Изменение услуги"
            form={(props) => (
              <AddEditServiceForm
                id={`editServiceForm-${service.id}`}
                initialValues={service}
                {...props}
              />
            )}
            onSubmit={(...props) => updateService(service.id, ...props)}
            formId={`editServiceForm-${service.id}`}
          >
            <IconButton edge="end" color="primary">
              <EditOutlinedIcon />
            </IconButton>
          </ModalButton>
          <ConfirmModalButton
            title="Удалить услугу?"
            text="Даннай услуга больше не будет доступна для записи и редактирования"
            okText="Удалить"
            onOk={() => {
              deleteService(service.id);
            }}
          >
            <IconButton edge="end" color="primary">
              <DeleteOutlineOutlinedIcon />
            </IconButton>
          </ConfirmModalButton>
        </>
      );
    else
      return renderToolTip(
        <IconButton
          edge="end"
          aria-haspopup="true"
          color="primary"
          disabled={service.status !== serviceStatuses.available || loading}
          onClick={() => onAddToCartClick(service.id)}
        >
          <AddShoppingCartIcon />
        </IconButton>,
        service.status
      );
  };
  return (
    <Table>
      <TableBody>
        {services.map((service) => (
          <ServiceRow service={service} renderButtons={renderButtons} />
        ))}
      </TableBody>
    </Table>
  );
}

export default ServiceList;
