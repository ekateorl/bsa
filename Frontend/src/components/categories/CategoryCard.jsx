import React from "react";
import MyButton from "../../UI/MyButton";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Gradients } from "../../common/constants";
import MyIcon from "../../UI/MyIcon";
import { CardMedia } from "@material-ui/core";
import { Images } from "../../api/constants";

function CategoryCard({ hasEvenIndex = false, category, ...props }) {
  const gradient = hasEvenIndex ? Gradients.blue : Gradients.orange;
  return (
    <MyButton gradient={gradient} {...props}>
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <CardMedia style={{ margin: "10px" }}>
          <MyIcon
            url={Images.category(category.image)}
            style={{ width: "100px", height: "100px" }}
          />
        </CardMedia>

        <CardContent
          style={{
            width: "100%",
          }}
        >
          <Typography variant="h6">{category.name}</Typography>
        </CardContent>
      </div>
    </MyButton>
  );
}

export default CategoryCard;
