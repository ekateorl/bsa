import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Typography from "@material-ui/core/Typography";
import { formatDuration } from "../../common/methods";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
});

function ServiceRow({ service = {}, renderButtons = (service) => {} }) {
  const [open, setOpen] = useState(false);
  const classes = useRowStyles();
  return (
    <>
      <TableRow key={service.id} className={classes.root}>
        <TableCell style={{ width: "20px" }}>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          <Typography variant="h6">{service.name}</Typography>
        </TableCell>
        <TableCell align="right">
          <Typography variant="text" color="textSecondary" noWrap>
            {formatDuration(service.duration)}
          </Typography>
        </TableCell>
        <TableCell align="right">
          <Typography
            variant="h6"
            style={{ color: "#ffb29d", fontWeight: "bold" }}
            noWrap
          >
            {service.price} ₽
          </Typography>
        </TableCell>
        <TableCell align="right" style={{ minWidth: "80px" }}>
          {renderButtons(service)}
        </TableCell>
      </TableRow>

      <TableRow key={service.id}>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={12}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Typography
              gutterBottom
              component="div"
              style={{ marginLeft: "65px", marginBottom: "15px" }}
            >
              {service.description}
            </Typography>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
}

export default ServiceRow;
