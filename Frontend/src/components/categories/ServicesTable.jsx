import React from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import Typography from "@material-ui/core/Typography";
import { formatDuration } from "../../common/methods";

function ServicesTable({
  items = [],
  total = {},
  onRemoveClick = (id) => {},
  loading = false,
  removable = false,
}) {
  const handleRemoveClick = ({ id, itemId }) => {
    onRemoveClick(itemId ? itemId : id);
  };
  return (
    <Table size={removable ? "small" : "middle"} aria-label="a dense table">
      <TableBody>
        {items.map((service) => (
          <TableRow key={service.id}>
            <TableCell>
              <Typography>{service.name}</Typography>
            </TableCell>
            <TableCell align="right">
              <Typography color="textSecondary" noWrap>
                {formatDuration(service.duration)}
              </Typography>
            </TableCell>
            <TableCell align="right">
              <Typography noWrap>{service.price} ₽</Typography>
            </TableCell>
            {removable && (
              <TableCell align="right">
                <IconButton
                  edge="end"
                  aria-haspopup="true"
                  color="primary"
                  onClick={() => handleRemoveClick(service)}
                  disabled={loading}
                >
                  <DeleteOutlineOutlinedIcon />
                </IconButton>
              </TableCell>
            )}
          </TableRow>
        ))}
      </TableBody>

      <TableFooter>
        <TableRow key="total">
          <TableCell
            component="th"
            scope="row"
            style={{ borderBottom: "none" }}
          >
            <Typography
              variant="h6"
              color="textPrimary"
              style={{ fontWeight: "bold" }}
            >
              Итого
            </Typography>
          </TableCell>
          <TableCell align="right" style={{ borderBottom: "none" }}>
            <Typography variant="h6" noWrap>
              {formatDuration(total.duration)}
            </Typography>
          </TableCell>
          <TableCell align="right" style={{ borderBottom: "none" }}>
            <Typography
              variant="h6"
              noWrap
              style={{ color: "#ffb29d", fontWeight: "bold" }}
            >
              {total.price} ₽
            </Typography>
          </TableCell>
          {removable && <TableCell style={{ borderBottom: "none" }} />}
        </TableRow>
      </TableFooter>
    </Table>
  );
}

export default ServicesTable;
