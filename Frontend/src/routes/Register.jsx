import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import TextField from "@material-ui/core/TextField";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Alert from "@material-ui/lab/Alert";
import FormControl from "@material-ui/core/FormControl";

import { makeStyles } from "@material-ui/core/styles";

import { Formik } from "formik";
import * as Yup from "yup";

import InputMask from "react-input-mask";

import { Authorization, Appointment } from "../api/methods";
import { Routes } from "../common/config";
import { setUserInfo, setLocalCart } from "../common/methods";
import LoadingButton from "../UI/LoadingButton";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    background: "linear-gradient(180deg, #92a9e3 0%, #dba6e8 100%)",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    width: "100%",
    marginTop: theme.spacing(2),
  },
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(1),
    },
  },
}));

function Register() {
  const classes = useStyles();
  const history = useHistory();
  const [error, setError] = useState();
  const [allValid, setAllValid] = useState(false);
  const [passwordValidation, setPasswordValidation] = useState({
    passwordLength: false,
    passwordHasDigits: false,
    passwordHasUpperCase: false,
    passwordHasLowerCase: false,
    passwordHasSymbols: false,
  });

  const messages = {
    passwordLength: "Пароль должен содержать не менее 6 символов",
    passwordHasDigits: "Пароль должен содержать хотя бы одну цифру",
    passwordHasUpperCase:
      "Пароль должен содержать хотя бы одну прописную букву",
    passwordHasLowerCase: "Пароль должен содержать хотя бы одну строчную букву",
    passwordHasSymbols:
      "Пароль должен содержать хотя бы один специальный символ",
  };

  const passwordHasDigits = "^(?=.*[0-9]).+$";
  const passwordHasUpperCase = "^(?=.*[A-Z]).+$";
  const passwordHasLowerCase = "^(?=.*[a-z]).+$";
  const passwordHasSymbols = "^(?=.*[!@#$%^&*]).+$";

  function passwordRule(value) {
    if (!value) value = "";
    const newValidation = {
      passwordLength: value.length >= 6,
      passwordHasDigits: value.match(passwordHasDigits) !== null,
      passwordHasUpperCase: value.match(passwordHasUpperCase) !== null,
      passwordHasLowerCase: value.match(passwordHasLowerCase) !== null,
      passwordHasSymbols: value.match(passwordHasSymbols) !== null,
    };

    setPasswordValidation({
      ...newValidation,
    });
    let valid = true;
    for (var prop in newValidation) {
      valid &= newValidation[prop];
    }
    setAllValid(valid);
    if (valid) return true;
    return false;
  }

  const displayValidation = (validation, allValid) => {
    if (allValid)
      return (
        <Alert severity="success">
          Пароль удовлетворяет всем требованиям безопасности
        </Alert>
      );
    return (
      <div className={classes.root}>
        {Object.keys(validation).map((key) => {
          const type = validation[key] ? "success" : "warning";
          return <Alert severity={type}>{messages[key]}</Alert>;
        })}
      </div>
    );
  };

  const state = history.location.state;
  const appointments = state ? state.appointments : null;

  const makeAppointment = (appointments = []) => {
    Appointment.create({ appointments }, () => {
      history.push(Routes.appointments);
      setLocalCart([]);
    });
  };

  const handleSubmit = (values, setSubmitting) => {
    setSubmitting(true);
    setError("");
    Authorization.register(
      values,
      ({ data }) => {
        console.log(data);
        setSubmitting(false);
        setUserInfo(data);
        if (appointments) makeAppointment(appointments);
        else history.push(Routes.categories);
      },
      (errors, status) => {
        setSubmitting(false);
        console.log(errors);
        if (status === 401)
          if (errors[0].includes("Username") && errors[0].includes("taken")) {
            setError("Пользователь с таким e-mail уже существует");
            return;
          }
        if (errors[0]) setError(errors[0]);
      }
    );
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Регистрация
        </Typography>
        {error && (
          <div className={classes.alert}>
            <Alert severity="error">{error}</Alert>
          </div>
        )}
        <Formik
          initialValues={{
            email: "",
            password: "",
            repeatPassword: "",
            surname: "",
            name: "",
            fatherName: "",
            phoneNumber: "",
          }}
          onSubmit={(values, { setSubmitting }) => {
            handleSubmit(values, setSubmitting);
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Некорректный e-mail")
              .required("Пожалуйста, заполните поле"),

            password: Yup.string("")
              .required("Пожалуйста, заполните поле")
              .test("password", "", function (value) {
                return passwordRule(value);
              }),
            repeatPassword: Yup.string()
              .required("Пожалуйста, заполните поле")
              .oneOf([Yup.ref("password")], "Пароли не совпадают"),
            surname: Yup.string().required("Пожалуйста, заполните поле"),
            name: Yup.string().required("Пожалуйста, заполните поле"),
            phoneNumber: Yup.string()
              .required("Пожалуйста, заполните поле")
              .test(
                "test-mask",
                "Пожалуйста, заполните поле",
                function (value = "") {
                  return !value.includes("_");
                }
              ),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              // dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              // handleReset,
            } = props;
            const itemProps = (name = "") => {
              return {
                error: errors[name] && touched[name],
                helperText: errors[name] && touched[name] && errors[name],
                name: name,
                required: true,
                value: values[name],
                onChange: handleChange,
                onBlur: handleBlur,
                margin: "normal",
                fullWidth: true,
                variant: "outlined",
              };
            };
            return (
              <form className={classes.form} onSubmit={handleSubmit} noValidate>
                <TextField
                  error={errors.surname && touched.surname}
                  helperText={
                    errors.surname && touched.surname && errors.surname
                  }
                  label="Фамилия"
                  name="surname"
                  required
                  value={values.surname}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                <TextField
                  error={errors.name && touched.name}
                  helperText={errors.name && touched.name && errors.name}
                  label="Имя"
                  name="name"
                  required
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                <TextField
                  error={errors.fatherName && touched.fatherName}
                  helperText={
                    errors.fatherName && touched.fatherName && errors.fatherName
                  }
                  label="Отчество"
                  name="fatherName"
                  value={values.fatherName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                <TextField
                  error={errors.email && touched.email}
                  helperText={errors.email && touched.email && errors.email}
                  label="E-mail"
                  name="email"
                  required
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />

                <FormControl
                  component="fieldset"
                  fullWidth
                  style={{ marginTop: "10px" }}
                >
                  <InputMask
                    mask="+7-999-999-99-99"
                    {...itemProps("phoneNumber")}
                    disabled={false}
                    maskChar="_"
                  >
                    {(inputProps) => (
                      <TextField
                        label="Телефон"
                        variant="outlined"
                        {...inputProps}
                      />
                    )}
                  </InputMask>
                </FormControl>
                <TextField
                  error={errors.password && touched.password}
                  helperText={
                    errors.password && touched.password && errors.password
                  }
                  label="Пароль"
                  name="password"
                  type="password"
                  required
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                {displayValidation(passwordValidation, allValid)}
                <TextField
                  error={errors.repeatPassword && touched.repeatPassword}
                  helperText={
                    errors.repeatPassword &&
                    touched.repeatPassword &&
                    errors.repeatPassword
                  }
                  label="Повторите пароль"
                  name="repeatPassword"
                  type="password"
                  required
                  value={values.repeatPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                <LoadingButton
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  type="submit"
                  pending={isSubmitting}
                >
                  Зарегистрироваться
                </LoadingButton>
              </form>
            );
          }}
        </Formik>
      </div>
    </Container>
  );
}

export default Register;
