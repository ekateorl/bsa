import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CircularProgress from "@material-ui/core/CircularProgress";

import MyTitle from "../../UI/MyTitle";

import { Specialist } from "../../api/methods";
import { isAdmin } from "../../common/methods";
import ModalButton from "../../components/modals/ModalButton";
import SpecialistCard from "../../components/specialists/SpecialistCard";
import AddEditSpecialistForm from "../../components/modals/specialists/AddEditSpecialistForm";
import { Images } from "../../api/constants";
import ReviewModal from "../../components/specialists/ReviewModal";

function Specialists() {
  const [specialists, setSpecialists] = useState([]);
  const [loading, setLoading] = useState(false);
  const [specialistId, setSpecialistId] = useState(-1);
  const [open, setOpen] = useState(false);

  const getSpecialists = () => {
    setLoading(true);
    Specialist.getAll(({ data }) => {
      setLoading(false);
      data.forEach(
        (i) => (i.image = `${Images.specialist(i.image)}?time=${Date.now()}`)
      );
      setSpecialists(data);
    });
  };

  useEffect(getSpecialists, []);

  const addSpecialist = (formData, success, fail) => {
    Specialist.create(
      formData,
      () => {
        success();
        getSpecialists();
      },
      fail
    );
  };

  const updateSpecialist = (id, formData, success, fail) => {
    Specialist.update(
      id,
      formData,
      () => {
        success();
        getSpecialists();
      },
      fail
    );
  };

  const deleteSpecialist = (id, success, fail) => {
    Specialist.delete(
      id,
      () => {
        success();
        getSpecialists();
      },
      fail
    );
  };

  const openReviewModal = (specialistId) => {
    setSpecialistId(specialistId);
    setOpen(true);
  };

  const closeReviewModal = () => setOpen(false);

  return (
    <>
      <MyTitle>
        Специалисты{" "}
        {isAdmin() && (
          <ModalButton
            title="Добавление специалиста"
            form={(props) => <div></div>}
            formId="addSpecialistForm"
            onSubmit={addSpecialist}
            form={(props) => (
              <AddEditSpecialistForm id="addSpecialistForm" {...props} />
            )}
          >
            <IconButton color="primary" stile={{ display: "inline" }}>
              <AddCircleOutlineIcon fontSize="large" />
            </IconButton>
          </ModalButton>
        )}
      </MyTitle>

      {loading && (
        <div
          style={{
            width: "100%",
            justifyContent: "center",
            display: "flex",
          }}
        >
          <CircularProgress size={50} style={{ color: "#ffb29d" }} />
        </div>
      )}
      <Grid container spacing={3}>
        {specialists.map((specialist, index) => {
          const row = Math.floor(index / 4);
          const column = index % 4;
          const hasEvenIndex = row % 2 === column % 2;
          return (
            <Grid item xs={3}>
              <SpecialistCard
                specialist={specialist}
                hasEvenIndex={hasEvenIndex}
                onEdit={updateSpecialist}
                onDelete={deleteSpecialist}
                onReviewOpen={openReviewModal}
              />
            </Grid>
          );
        })}
      </Grid>
      <ReviewModal
        open={open}
        specialistId={specialistId}
        specialist={specialists.find((i) => i.id === specialistId)}
        onClose={closeReviewModal}
      />
    </>
  );
}

export default Specialists;
