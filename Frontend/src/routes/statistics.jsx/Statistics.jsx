import React, { useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import MyTitle from "../../UI/MyTitle";
import CategoryCount from "../../components/statistics/CategoryCount";
import RatingDynamics from "../../components/statistics/RatingDynamics";
import SpecialistsPerfomance from "../../components/statistics/SpecialistsPerfomance";

const useStyles = makeStyles((theme) => ({
  tabs: {
    marginBottom: theme.spacing(4),
  },
}));

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#745e8a",
    },
  },
})((props) => (
  <Tabs {...props} centered TabIndicatorProps={{ children: <span /> }} />
));

function Statistics() {
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  const page = () => {
    switch (value) {
      case 1:
        return <CategoryCount />;
      case 2:
        return <SpecialistsPerfomance />;
      case 3:
        return <RatingDynamics />;
      default:
        return <div />;
    }
  };

  return (
    <>
      <MyTitle>Статистика</MyTitle>
      <div className={classes.tabs}>
        <StyledTabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab disableRipple label="Сеансы за период" />
          <Tab disableRipple label="Популярность услуг" />
          <Tab disableRipple label="Работа специалистов" />
          <Tab disableRipple label="Динамика рейтинга" />
        </StyledTabs>
      </div>
      {page()}
    </>
  );
}

export default Statistics;
