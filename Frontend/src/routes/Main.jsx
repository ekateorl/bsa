import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Routes } from "../common/config";
import LayoutForRoutes from "../components/navigarion/LayoutForRoutes";
import Appointments from "./appointments/Appointments";
import Categories from "./categories/Categories";
import Category from "./categories/Category";
import Login from "./Login";
import MakeAppointment from "./make-appointment/MakeAppointment";
import Register from "./Register";
import Schedule from "./schedule/Schedule";
import Specialists from "./specialists/Specialists";
import Statistics from "./statistics.jsx/Statistics";

function Main() {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path={Routes.categories}
          component={() => (
            <LayoutForRoutes>
              <Categories />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.category({ id: ":id" })}
          component={(props) => (
            <LayoutForRoutes>
              <Category {...props} />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.specialists}
          component={() => (
            <LayoutForRoutes>
              <Specialists />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.schedule}
          component={() => (
            <LayoutForRoutes>
              <Schedule />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.statistics}
          component={() => (
            <LayoutForRoutes>
              <Statistics />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.appointments}
          component={() => (
            <LayoutForRoutes>
              <Appointments />
            </LayoutForRoutes>
          )}
        />
        <Route
          exact
          path={Routes.makeAppointment}
          component={() => (
            <LayoutForRoutes>
              <MakeAppointment />
            </LayoutForRoutes>
          )}
        />
        <Route exact path={Routes.login} component={Login} />
        <Route exact path={Routes.register} component={Register} />
        <Route render={() => <Redirect to={Routes.categories} />} />
      </Switch>
    </Router>
  );
}

export default Main;
