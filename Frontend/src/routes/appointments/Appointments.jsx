import React, { useState, useEffect } from "react";

import { withStyles, makeStyles } from "@material-ui/core/styles";

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";

import MyTitle from "../../UI/MyTitle";
import MyIcon from "../../UI/MyIcon";

import { Appointment } from "../../api/methods";
import AppointmentsTable from "../../components/appointments/AppointmentsTable";

import { Images } from "../../api/constants";

const useStyles = makeStyles((theme) => ({
  tabs: {
    marginBottom: theme.spacing(3),
  },
  root: {
    display: "flex",
    alignItems: "center",
    width: "100%",
  },
  progress: {
    color: "#ffb29d",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
  },
}));

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#745e8a",
    },
  },
})((props) => (
  <Tabs {...props} centered TabIndicatorProps={{ children: <span /> }} />
));

function Appointments() {
  const [value, setValue] = useState(0);
  const [loading, setLoading] = useState(false);
  const [appointments, setAppointments] = useState([]);

  const handleCancel = (id = 0, success, fail) => {
    Appointment.cancel(
      id,
      {},
      () => {
        getAppointments(value);
        success();
      },
      fail
    );
  };

  const handleRate = (id = 0, values, success) => {
    Appointment.rate(id, values, () => {
      success();
      getAppointments(value);
    });
  };

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  const getAppointments = (value) => {
    if (value === 1) getCompleteAppointments();
    else getIncompleteAppointments();
  };

  const setAppointmentsData = (data = []) => {
    data.forEach(
      (i) => (i.specialist.image = Images.specialist(i.specialist.image))
    );
    setAppointments(data);
  };

  const getIncompleteAppointments = () => {
    setLoading(true);
    Appointment.getIncomplete(({ data }) => {
      setAppointmentsData(data);
      setLoading(false);
    });
  };

  const getCompleteAppointments = () => {
    setLoading(true);
    Appointment.getComplete(({ data }) => {
      setAppointmentsData(data);
      setLoading(false);
    });
  };

  useEffect(() => getAppointments(value), [value]);
  const classes = useStyles();
  const isEmpty = !(appointments && appointments.length);
  return (
    <>
      <MyTitle>Мои записи</MyTitle>
      <div className={classes.tabs}>
        <StyledTabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab disableRipple label="Предстоящие" />
          <Tab disableRipple label="Завершённые" />
        </StyledTabs>
      </div>

      {isEmpty && !loading && (
        <div
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <MyIcon
            url="/svg/empty.svg"
            style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
          />

          <Typography color="textSecondary" style={{ marginTop: "10px" }}>
            Записи отсутсвуют
          </Typography>
        </div>
      )}

      {!isEmpty && !loading && (
        <AppointmentsTable
          appointments={appointments}
          onCancel={handleCancel}
          onRate={handleRate}
        />
      )}
      {loading && (
        <div className={classes.root}>
          <CircularProgress size={50} className={classes.progress} />
        </div>
      )}
    </>
  );
}

export default Appointments;
