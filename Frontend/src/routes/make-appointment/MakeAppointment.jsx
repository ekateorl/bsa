import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";

import MyTitle from "../../UI/MyTitle";

import { Appointment, Cart, Service } from "../../api/methods";
import { Routes } from "../../common/config";
import { Typography } from "@material-ui/core";
import ConfirmAppointmentDialog from "../../components/appointments/ConfirmAppointmentDialog";
import { isUser, getLocalCart } from "../../common/methods";
import MakeAppointmentItem from "../../components/make-appointment/MakeAppointmentItem";

function MakeAppointment() {
  const history = useHistory();

  const [open, setOpen] = useState(false);
  const [makingAppointment, setMakingAppointment] = useState(false);
  const [confirmInfo, setConfirmInfo] = useState();

  const [groups, setGroups] = useState([]);

  const getGroups = (serviceIds = []) => {
    const success = ({ data }) => {
      let curGroups = [];
      data.forEach((group, index) => {
        Cart.getLocal({ services: group }, ({ data }) => {
          curGroups[index] = data;
          setGroups([...curGroups]);
        });
      });
    };
    Service.getGroups({ services: serviceIds }, success);
  };

  const getCart = () => {
    const success = ({ data }) => {
      getGroups(data.items.map((i) => i.id));
    };
    if (isUser()) Cart.get(success);
    else {
      const services = getLocalCart();
      getGroups(services);
    }
  };

  useEffect(getCart, []);

  const [selectedTime, setSelectedTime] = useState([]);

  const onSelectedTimeChange = (time, index) => {
    selectedTime[index] = time;
    console.log(selectedTime);
    setSelectedTime([...selectedTime]);
  };

  const isAllTimeSelected = () => {
    let result = selectedTime.length === groups.length;
    selectedTime.forEach((i) => (result &= Boolean(i)));
    return result;
  };

  const handleClose = () => setOpen(false);

  const makeAppointment = (appointments = []) => {
    setMakingAppointment(true);

    Appointment.create({ appointments }, () =>
      history.push(Routes.appointments)
    );
  };

  const onConfirmAppointmentClick = () => {
    const appointments = groups.map((group, index) => {
      return {
        timeSlotId: selectedTime[index].timeSlot.id,
        services: group.items.map((i) => i.id),
      };
    });
    if (isUser()) makeAppointment(appointments);
    else {
      history.push({
        pathname: Routes.login,
        state: { appointments },
      });
    }
  };

  const renderConfirmDialog = (confirmInfo) => {
    return (
      <ConfirmAppointmentDialog
        open={open}
        appInfoList={confirmInfo}
        onOk={onConfirmAppointmentClick}
        onCancel={handleClose}
        loading={makingAppointment}
      />
    );
  };

  const onMakeAppClick = () => {
    const info = selectedTime.map(({ specialist, timeSlot, date }, index) => {
      return {
        specialist,
        beginning: timeSlot.beginning,
        date,
        cart: groups[index],
      };
    });
    setConfirmInfo(info);
    setOpen(true);
  };

  return (
    <>
      <MyTitle>Оформление записи</MyTitle>
      <Grid container spacing={10}>
        <Grid item xs style={{ maxWidth: "500px" }}>
          <Typography
            variant="h6"
            color="textSecondary"
            style={{ marginBottom: "15px", textAlign: "center" }}
          >
            Информация о сеансе
          </Typography>
        </Grid>
        <Grid item xs>
          <Typography
            variant="h6"
            color="textSecondary"
            style={{ marginBottom: "15px", textAlign: "center" }}
          >
            Выберите дату, специалиста и время
          </Typography>
        </Grid>
      </Grid>

      {groups.map((group, index) => {
        let selectedTimeSlot = -1;
        if (selectedTime[index])
          selectedTimeSlot = selectedTime[index].timeSlot.id;
        console.log(selectedTimeSlot);

        const handleChange = (time) => onSelectedTimeChange(time, index);
        return (
          <>
            <MakeAppointmentItem
              group={group}
              selectedTimeSlot={selectedTimeSlot}
              onSelectedChange={handleChange}
            />
            <Divider style={{ margin: "15px 0" }} />
          </>
        );
      })}
      <Button
        variant="contained"
        color="primary"
        style={{
          marginBottom: "15px",
          float: "right",
        }}
        disabled={!isAllTimeSelected()}
        onClick={onMakeAppClick}
      >
        Записаться
      </Button>

      {confirmInfo && renderConfirmDialog(confirmInfo)}
    </>
  );
}

export default MakeAppointment;
