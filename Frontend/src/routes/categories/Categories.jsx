import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CircularProgress from "@material-ui/core/CircularProgress";

import MyTitle from "../../UI/MyTitle";

import { Category } from "../../api/methods";
import { Routes } from "../../common/config";
import CategoryCard from "../../components/categories/CategoryCard";
import { isAdmin } from "../../common/methods";
import ModalButton from "../../components/modals/ModalButton";
import AddEditCategoryForm from "../../components/modals/categories/AddEditCategoryForm";

function Categories() {
  const history = useHistory();
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);

  const getCategories = () => {
    setLoading(true);
    Category.getAll(({ data }) => {
      setLoading(false);
      setCategories(data);
    });
  };

  useEffect(getCategories, []);

  const redirectToCategoryPage = (id) => {
    history.push(Routes.category({ id }));
  };

  const addCategory = (formData, success, fail) => {
    formData.append("image", "image");
    Category.add(
      formData,
      () => {
        success();
        getCategories();
      },
      fail
    );
  };

  return (
    <>
      <MyTitle>
        Категории услуг{" "}
        {isAdmin() && (
          <ModalButton
            title="Добавление категории"
            form={(props) => (
              <AddEditCategoryForm id="addEditCategoryForm" {...props} />
            )}
            formId="addEditCategoryForm"
            onSubmit={addCategory}
          >
            <IconButton
              color="primary"
              onClick={() => {}}
              stile={{ display: "inline" }}
            >
              <AddCircleOutlineIcon fontSize="large" />
            </IconButton>
          </ModalButton>
        )}
      </MyTitle>

      {loading && (
        <div
          style={{
            width: "100%",
            justifyContent: "center",
            display: "flex",
          }}
        >
          <CircularProgress size={50} style={{ color: "#ffb29d" }} />
        </div>
      )}
      <Grid container spacing={3}>
        {categories.map((category, index) => {
          const row = Math.floor(index / 3);
          const column = index % 3;
          const hasEvenIndex = row % 2 === column % 2;
          return (
            <Grid item xs={4}>
              <CategoryCard
                hasEvenIndex={hasEvenIndex}
                category={category}
                onClick={() => redirectToCategoryPage(category.id)}
                fullWidth
              />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}

export default Categories;
