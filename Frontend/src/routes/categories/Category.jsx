import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import useBus, { dispatch } from "use-bus";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import CircularProgress from "@material-ui/core/CircularProgress";
import ServiceList from "../../components/categories/ServiceList";

import { Cart, Category as CategoryRequests, Service } from "../../api/methods";
import { Events, Gradients, serviceStatuses } from "../../common/constants";
import MyTitle from "../../UI/MyTitle";
import MyIcon from "../../UI/MyIcon";
import { Images } from "../../api/constants";
import {
  getLocalCart,
  isAdmin,
  isUser,
  setLocalCart,
} from "../../common/methods";
import AddEditServiceForm from "../../components/modals/categories/AddEditServiceForm";
import ModalButton from "../../components/modals/ModalButton";
import AddEditCategoryForm from "../../components/modals/categories/AddEditCategoryForm";
import ConfirmModalButton from "../../components/modals/ConfirmModalButton";
import { useHistory } from "react-router";
import { Routes } from "../../common/config";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
    width: "100%",
  },
  serviceProgress: {
    color: "#ffb29d",
    position: "absolute",
    top: "0%",
    left: "50%",
    marginLeft: -25,
  },
}));

function Category({ match }) {
  const classes = useStyles();
  const history = useHistory();

  const [category, setCategory] = useState({
    name: "",
    id: 0,
    image: "",
  });
  const [services, _setServices] = useState([]);
  const setServices = (services = []) => {
    if (!isUser()) {
      const cart = getLocalCart();
      services.forEach((service) => {
        const isInCart = cart.findIndex((id) => id === service.id) !== -1;
        service.status = isInCart
          ? serviceStatuses.alreadyAdded
          : serviceStatuses.available;
      });
    }
    _setServices([...services]);
  };

  const [isFound, setIsFound] = useState(undefined);
  const [loadingCategory, setLoadingCategory] = useState(true);
  const [loadingServices, setLoadingServices] = useState(false);

  const getCategory = () => {
    setLoadingCategory(true);
    CategoryRequests.get(
      match.params.id,
      ({ data }) => {
        setLoadingCategory(false);
        data.image = `${Images.category(data.image)}?time=${Date.now()}`;
        console.log(data.image);
        setCategory(data);
        setIsFound(true);
      },
      () => {
        setLoadingCategory(false);
        setIsFound(false);
      }
    );
  };

  const getServices = () => {
    setLoadingServices(true);
    Service.getByCategory({ categoryId: match.params.id }, ({ data }) => {
      setLoadingServices(false);
      setServices(data);
    });
  };

  useEffect(() => {
    getCategory();
    getServices();
  }, []);

  useBus(Events.removeFromCart, getServices, []);

  const addToLocalCart = (serviceId = 0) => {
    const cart = getLocalCart();
    cart.push(serviceId);
    setLocalCart(cart);
    setServices(services);
    dispatch(Events.addToCart);
  };

  const addToCart = (serviceId = 0) => {
    if (!isUser()) addToLocalCart(serviceId);
    else
      Cart.add({ serviceId }, () => {
        getServices();
        dispatch(Events.addToCart);
      });
  };

  const updateCategory = (formData, success, fail) => {
    console.log(formData);
    formData.append("image", category.name);

    CategoryRequests.update(
      match.params.id,
      formData,
      () => {
        success();
        getCategory();
      },
      fail
    );
  };

  const deleteCategory = () => {
    CategoryRequests.delete(category.id, () => {
      history.push(Routes.categories);
    });
  };

  const addService = (service, success, fail) => {
    const specialists = service.specialists.map((i) => i.id);
    Service.create(
      { ...service, specialists },
      () => {
        success();
        getServices();
      },
      fail
    );
  };

  const renderComponent = (isFound = undefined) => {
    if (isFound === true)
      return (
        <>
          {loadingCategory && (
            <div
              style={{
                width: "100%",
                justifyContent: "center",
                display: "flex",
              }}
            >
              <CircularProgress size={50} style={{ color: "#ffb29d" }} />
            </div>
          )}
          <MyTitle>
            {category.name + " "}
            {isAdmin() && (
              <>
                <ModalButton
                  title="Добавление услуги"
                  onSubmit={addService}
                  form={(props) => (
                    <AddEditServiceForm
                      id="addServiceForm"
                      initialValues={{ categoryFk: category.id }}
                      {...props}
                    />
                  )}
                  formId="addServiceForm"
                >
                  <IconButton color="primary" onClick={() => {}}>
                    <AddCircleOutlineIcon fontSize="large" />
                  </IconButton>
                </ModalButton>

                <ModalButton
                  title="Изменение категории"
                  form={(props) => (
                    <AddEditCategoryForm
                      id="editCategoryForm"
                      initialValues={category}
                      {...props}
                    />
                  )}
                  formId="editCategoryForm"
                  onSubmit={updateCategory}
                >
                  <IconButton color="primary">
                    <EditOutlinedIcon fontSize="large" />
                  </IconButton>
                </ModalButton>

                <ConfirmModalButton
                  title="Удалить категорию?"
                  text="Услуги данной категории больше не будут доступны"
                  okText="Удалить"
                  onOk={deleteCategory}
                >
                  <IconButton color="primary">
                    <DeleteOutlineOutlinedIcon fontSize="large" />
                  </IconButton>
                </ConfirmModalButton>
              </>
            )}
          </MyTitle>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <MyIcon
                url={category.image}
                style={{
                  width: "300px",
                  height: "300px",
                  background: Gradients.forIcons,
                }}
              />
            </Grid>
            <Grid item xs={8}>
              <div className={classes.root}>
                <div className={classes.wrapper}>
                  <ServiceList
                    services={services}
                    onAddToCartClick={addToCart}
                    loading={loadingServices}
                    reload={getServices}
                  />
                  {loadingServices && (
                    <CircularProgress
                      size={50}
                      className={classes.serviceProgress}
                    />
                  )}
                </div>
              </div>
            </Grid>
          </Grid>
        </>
      );
    if (isFound === false)
      return (
        <div
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <MyIcon
            url="/svg/error-404.svg"
            style={{ width: "150px", height: "150px", background: "#bdbdbd" }}
          />

          <Typography
            variant="h5"
            color="textSecondary"
            style={{ marginTop: "10px" }}
          >
            Категория не найдена
          </Typography>
        </div>
      );
  };

  return <>{renderComponent(isFound)}</>;
}

export default Category;
