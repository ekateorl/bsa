import React, { useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import MyTitle from "../../UI/MyTitle";
import EditSchedule from "../../components/schedule/edit-schedule/EditSchedule";
import EditTemplates from "../../components/schedule/edit-template/EditTemplates";
import AppointmentsForDay from "../../components/schedule/appointments-schedule/AppointmentsForDay";

const useStyles = makeStyles((theme) => ({
  tabs: {
    marginBottom: theme.spacing(4),
  },
}));

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#745e8a",
    },
  },
})((props) => (
  <Tabs {...props} centered TabIndicatorProps={{ children: <span /> }} />
));

function Schedule() {
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  const page = () => {
    switch (value) {
      case 0:
        return <EditSchedule />;
      case 1:
        return <EditTemplates />;
      default:
        return <AppointmentsForDay />;
    }
  };

  return (
    <>
      <MyTitle>Расписание</MyTitle>
      <div className={classes.tabs}>
        <StyledTabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab disableRipple label="Управление расписанием" />
          <Tab disableRipple label="Шаблоны" />
          <Tab disableRipple label="Записи на дату" />
        </StyledTabs>
      </div>

      {page()}
    </>
  );
}

export default Schedule;
