import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Alert from "@material-ui/lab/Alert";

import { makeStyles } from "@material-ui/core/styles";

import { Formik } from "formik";
import * as Yup from "yup";

import { Authorization, Appointment } from "../api/methods";
import { Routes } from "../common/config";
import { setLocalCart, setUserInfo } from "../common/methods";
import LoadingButton from "../UI/LoadingButton";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    background: "linear-gradient(180deg, #92a9e3 0%, #dba6e8 100%)",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    width: "100%",
    marginTop: theme.spacing(2),
  },
}));

function Login() {
  const classes = useStyles();
  const history = useHistory();
  const state = history.location.state;
  const appointments = state ? state.appointments : null;

  const [error, setError] = useState();

  const makeAppointment = (appointments = []) => {
    Appointment.create({ appointments }, () => {
      history.push(Routes.appointments);
      setLocalCart([]);
    });
  };

  const handleSubmit = (values, setSubmitting) => {
    setSubmitting(true);
    setError("");
    Authorization.login(
      values,
      ({ data }) => {
        setSubmitting(false);
        setUserInfo(data);
        if (appointments) makeAppointment(appointments);
        else history.push(Routes.categories);
      },
      (errors, status) => {
        setSubmitting(false);
        console.log(errors);
        if (status === 401) setError("Неверный логин или пароль");
      }
    );
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Авторизация
        </Typography>
        {error && (
          <div className={classes.alert}>
            <Alert severity="error">{error}</Alert>
          </div>
        )}
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={(values, { setSubmitting }) => {
            handleSubmit(values, setSubmitting);
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Некорректный e-mail")
              .required("Пожалуйста, заполните поле"),
            password: Yup.string().required("Пожалуйста, заполните поле"),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              // dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              // handleReset,
            } = props;
            return (
              <form className={classes.form} onSubmit={handleSubmit} noValidate>
                <TextField
                  error={errors.email && touched.email}
                  helperText={errors.email && touched.email && errors.email}
                  label="E-mail"
                  name="email"
                  required
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />

                <TextField
                  error={errors.password && touched.password}
                  helperText={
                    errors.password && touched.password && errors.password
                  }
                  label="Пароль"
                  name="password"
                  type="password"
                  required
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
                <LoadingButton
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  type="submit"
                  pending={isSubmitting}
                >
                  Войти
                </LoadingButton>
                <Link
                  onClick={() =>
                    history.push({
                      pathname: Routes.register,
                      state: { appointments },
                    })
                  }
                  variant="body2"
                  style={{ color: "#1769aa", cursor: "pointer" }}
                >
                  Зарегистрироваться
                </Link>
              </form>
            );
          }}
        </Formik>
      </div>
    </Container>
  );
}

export default Login;
