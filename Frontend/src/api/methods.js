import axios from "axios";
import { API_URL, C, T, BODY } from "./constants";
import { getUserInfo, removeUserInfo } from "../common/methods";
import { Routes } from "../common/config";

const defaultFail = (errors, status) => console.log(status + ": " + errors);

const commonRequest = (
  requestType,
  methodClass,
  method = "",
  success = () => console.error("Функция success не задана"),
  data = {},
  fail
) => {
  const token = "Bearer " + (getUserInfo() ? getUserInfo().accessToken : "");
  method = method === "" ? method : "/" + method;
  const request = (token) =>
    axios({
      method: requestType,
      baseURL: API_URL,
      url: methodClass + method,
      headers: {
        Authorization: token,
      },
      data,
    })
      .then(success)
      .catch(({ response }) => {
        if (response) {
          const { data, status } = response;

          if (fail) fail(data.errors, status);
          else if (status !== 401) {
            defaultFail(data.errors, status);
          }

          const { location } = document;
          if (status === 401 && location.pathname !== Routes.login) {
            if (getUserInfo()) {
              removeUserInfo();
              window.location.reload();
            }
          }
        }
      });
  request(token);
};

export const fileExists = (url = "", success, fail) => {
  axios({
    method: "GET",
    url: url,
    responseType: "blob",
  })
    .then(success)
    .catch(fail);
};

export const Authorization = {
  login: (body = BODY.login, success, fail) =>
    commonRequest(T.POST, C.account, "login", success, body, fail),
  register: (body = BODY.register, success, fail) =>
    commonRequest(T.POST, C.account, "register", success, body, fail),
};
export const Cart = {
  get: (success, fail) => commonRequest(T.GET, C.cart, "", success, {}, fail),
  getLocal: (body = BODY.cart, success) =>
    commonRequest(T.POST, C.cart, "byId", success, body),
  add: (body = BODY.addToCart, success, fail) =>
    commonRequest(T.POST, C.cart, "", success, body, fail),
  delete: (id, success) => commonRequest(T.DELETE, C.cart, id, success),
};
export const Category = {
  getAll: (success) => commonRequest(T.GET, C.category, "", success),
  get: (id, success, fail) =>
    commonRequest(T.GET, C.category, id, success, {}, fail),
  getCount: (body = BODY.getCount, success) =>
    commonRequest(T.POST, C.category, "count", success, body),
  add: (body, success, fail) =>
    commonRequest(T.POST, C.category, "", success, body, fail),
  update: (id, body, success, fail) =>
    commonRequest(T.PUT, C.category, id, success, body, fail),
  delete: (id, success) => commonRequest(T.DELETE, C.category, id, success),
};

export const Service = {
  getByCategory: (body = BODY.serviceByCategory, success) =>
    commonRequest(T.POST, C.service, "byCategory", success, body),
  getGroups: (body = BODY.cart, success) =>
    commonRequest(T.POST, C.service, "group", success, body),
  create: (body = BODY.service, success, fail) =>
    commonRequest(T.POST, C.service, "", success, body, fail),
  update: (id, body = BODY.service, success, fail) =>
    commonRequest(T.PUT, C.service, id, success, body, fail),
  delete: (id, success) => commonRequest(T.DELETE, C.service, id, success),
};

export const Specialist = {
  getAll: (success) => commonRequest(T.GET, C.specialist, "", success),
  getSpecialistsWIthTime: (body = BODY.specialistsWithTime, success) =>
    commonRequest(T.POST, C.specialist, "forUser", success, body),
  getReviews: (id, success) =>
    commonRequest(T.GET, C.specialist, `${id}/reviews`, success),
  getRatingDynamics: (body = BODY.getRatingDynamics, success) =>
    commonRequest(T.POST, C.specialist, "ratingDynamics", success, body),
  getPerfomance: (body = BODY.getCount, success) =>
    commonRequest(T.POST, C.specialist, "perfomance", success, body),
  create: (body = BODY.specialist, success, fail) =>
    commonRequest(T.POST, C.specialist, "", success, body, fail),
  update: (id, body = BODY.specialist, success, fail) =>
    commonRequest(T.PUT, C.specialist, id, success, body, fail),
  delete: (id, success) => commonRequest(T.DELETE, C.specialist, id, success),
};

export const Appointment = {
  create: (body = BODY.makeAppointment, success) =>
    commonRequest(T.POST, C.appointment, "", success, body),
  getIncomplete: (success) =>
    commonRequest(T.GET, C.appointment, "incomplete", success),
  getComplete: (success) =>
    commonRequest(T.GET, C.appointment, "complete", success),
  getForDate: (body = BODY.getAppForDate, success) =>
    commonRequest(T.POST, C.appointment, "forDate", success, body),
  get: (id, success) => commonRequest(T.GET, C.appointment, id, success),
  cancel: (id, body = BODY.cancelAppointment, success) =>
    commonRequest(T.POST, C.appointment, `cancel/${id}`, success, body),
  rate: (id, body = BODY.rateAppointment, success) =>
    commonRequest(T.POST, C.appointment, id, success, body),
};

export const TimeSlot = {
  getForSpecialist: (body = BODY.getTimeSlots, success) =>
    commonRequest(T.POST, C.timeSlot, "bySpecialist", success, body),
  update: (body = BODY.updateTimeSlots, success, fail) =>
    commonRequest(T.PUT, C.timeSlot, "", success, body, fail),
};

export const Template = {
  getAll: (success) => commonRequest(T.GET, C.scheduleTemplate, "", success),
  get: (id, success, fail) =>
    commonRequest(T.GET, C.scheduleTemplate, id, success, {}, fail),
  create: (body = BODY.createScheduleTemplate, success) =>
    commonRequest(T.POST, C.scheduleTemplate, "", success, body),
  update: (id, body = BODY.updateTemplate, success, fail) =>
    commonRequest(T.PUT, C.scheduleTemplate, id, success, body, fail),
  delete: (id, success) =>
    commonRequest(T.DELETE, C.scheduleTemplate, id, success),
};
