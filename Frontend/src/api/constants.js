export const API_URL = "https://localhost:44333/api/";
export const Images = {
  category: (img) => `https://localhost:44333/images/categories/${img}`,
  specialist: (img) => `https://localhost:44333/images/specialists/${img}`,
};

export const T = {
  POST: "POST",
  GET: "GET",
  PUT: "PUT",
  DELETE: "DELETE",
};

export const C = {
  account: "account",
  cart: "cart",
  category: "category",
  service: "service",
  appointment: "appointment",
  specialist: "specialist",
  timeSlot: "timeSlot",
  scheduleTemplate: "scheduleTemplate",
};

export const BODY = {
  login: {
    email: "",
    password: "",
  },
  register: {
    name: "",
    surname: "",
    fatherName: "",
    email: "",
    phoneNumber: "",
    password: "",
  },
  serviceByCategory: {
    categoryId: 0,
  },
  service: {
    categoryFk: 0,
    name: "",
    price: 0,
    duration: "00:00",
    description: "",
    specialists: [],
  },
  addToCart: {
    serviceId: 0,
  },
  cart: {
    services: [],
  },
  specialistsWithTime: {
    date: "",
  },
  makeAppointment: [],
  rateAppointment: {
    rating: 0,
  },
  getTimeSlots: {
    dates: [],
    specialistId: 0,
  },
  updateTimeSlots: {
    specialistId: 0,
    timeToCreate: [],
    timeToDelete: [],
  },
  createScheduleTemplate: {
    name: "",
    timeSlots: [],
  },
  updateTemplate: {
    name: "",
    timeToCreate: [],
    timeToDelete: [],
  },
  getAppForDate: {
    date: "",
  },
  getCount: {
    start: "",
    end: "",
  },
  getRatingDynamics: {
    start: "",
    end: "",
    specialists: [],
  },
  cancelAppointment: {
    reason: "",
  },
};
