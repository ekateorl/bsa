export const Routes = {
  categories: "/categories",
  category: ({ id }) => `/categories/${id}`,

  specialists: "/specialists",
  schedule: "/schedule",
  appointments: "/appointments",
  login: "/login",
  register: "/register",
  makeAppointment: "/makeAppointment",
  statistics: "/statistics",
};

export const NavbarConfig = [
  {
    path: Routes.categories,
    text: "Услуги",
  },
  {
    path: Routes.specialists,
    text: "Специалисты",
  },
  {
    path: Routes.schedule,
    text: "Расписание",
  },
  {
    path: Routes.statistics,
    text: "Статистика",
  },
  /*
  {
    path: Routes.appointments,
    text: "Записи",
  },*/
];
