export const serviceStatuses = {
  available: 0,
  alreadyAdded: 1,
};

export const Events = {
  removeFromCart: "@@ui/REMOVE_FROM_CART",
  addToCart: "@@ui/ADD_TO_CART",
};

export const Gradients = {
  main: "linear-gradient(-90deg, #92a9e3 0%, #dba6e8 100%)",
  orange: "linear-gradient(180deg, #ffb29d 0%, #dba6e8 100%)",
  blue: "linear-gradient(180deg, #9adfff 0%, #92a9e3 100%)",
  forIcons: "linear-gradient(180deg, #92a9e3 0%, #ca97d3 50%, #ffb29d 100%)",
};

export const userRoles = {
  user: "user",
  admin: "admin",
};

export const ChartGradients = [
  {
    start: "#d4fc79",
    end: "#96e6a1",
  },
  {
    start: "#f6d365",
    end: "#fda085",
  },
  {
    start: "#c2e9fb",
    end: "#a1c4fd",
  },
  {
    start: "#fbc2eb",
    end: "#a18cd1",
  },
];
