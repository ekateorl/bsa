import moment from "moment";
import { NavbarConfig } from "./config";
import { userRoles, ChartGradients } from "./constants";

export const getUserInfo = () => {
  const info = localStorage.getItem("bsaUserInfo");
  return JSON.parse(info);
};

export const setUserInfo = (data) => {
  return localStorage.setItem(
    "bsaUserInfo",
    JSON.stringify({
      accessToken: data.token,
      role: data.role,
      name: data.name,
    })
  );
};

export const removeUserInfo = () => {
  localStorage.removeItem("bsaUserInfo");
};

export const isAdmin = () => {
  const userInfo = getUserInfo();
  return userInfo && userInfo.role === userRoles.admin;
};

export const isUser = () => {
  const userInfo = getUserInfo();
  return userInfo && userInfo.role === userRoles.user;
};

export const getNavbarConfig = () => {
  const info = getUserInfo();
  if (info) {
    const role = info.role;
    if (role === userRoles.admin) return NavbarConfig;
  }
  return NavbarConfig.slice(0, 2);
};

export const formatDuration = (timeString = "00:00") => {
  const time = timeString.split(":");
  const mins = Number(time[1]);
  const hours = Number(time[0]);
  let result = "";
  if (mins > 0) result = `${mins} мин`;
  if (hours > 0) result = `${hours} ч ${result}`;
  return result.trimEnd();
};

const monthsToString = (months) => {
  const remainder = months % 10;
  if (months < 10 || months > 20) {
    if (remainder === 1) return `${months} месяц`;
    if (remainder > 1 && remainder < 5) return `${months} месяца`;
  }
  return `${months} месяцев`;
};
const yearsToString = (years) => {
  const remainder = years % 10;
  if (years < 10 || years > 20) {
    if (remainder === 1) return `${years} год`;
    if (remainder > 1 && remainder < 5) return `${years} года`;
  }
  return `${years} лет`;
};

export const formatMonths = (months = 0) => {
  const years = Math.floor(months / 12);
  const remainder = months % 12;
  if (years === 0) return monthsToString(months);
  if (remainder === 0) return yearsToString(years);
  return `${yearsToString(years)} ${monthsToString(remainder)}`;
};

export const exclude = (arr1, arr2) => {
  return arr1.filter((i) => arr2.findIndex((j) => j.id === i.id) === -1);
};

export const ratingCountToString = (count) => {
  const remainder = count % 10;
  if (count < 10 || count > 20) {
    if (remainder === 1) return `${count} отзыв`;
    if (remainder > 1 && remainder < 5) return `${count} отзыва`;
  }
  return `${count} отзывов`;
};

export const stringToMinutes = (timeString = "00:00") => {
  const time = timeString.split(":");
  const mins = Number(time[1]);
  const hours = Number(time[0]);
  return hours * 60 + mins;
};

export const minutesToString = (minutes) => {
  const mins = minutes % 60;
  const hours = Math.floor(minutes / 60);
  const hoursStr = `${hours}`.padStart(2, "0");
  const minsStr = `${mins}`.padStart(2, "0");
  return `${hoursStr}:${minsStr}`;
};

export const getWeek = (date = moment()) => {
  const startOfWeek = date.clone().startOf("isoWeek");
  const endOfWeek = date.clone().endOf("isoWeek");

  const days = [];
  let day = startOfWeek;

  while (day <= endOfWeek) {
    days.push(day);
    day = day.clone().add(1, "day");
  }
  return days;
};

export const formatDatePeriod = (start = moment(), end = moment()) => {
  let format = "D";
  if (start.month() !== end.month()) {
    format += " MMMM";
    if (start.year() !== end.year()) format += " YYYY";
  }
  const startStr = start.format(format);
  const endStr = end.format("D MMMM YYYY");
  return `${startStr} — ${endStr}`;
};

export const getChartGradients = (ctx, height = 450) => {
  return ChartGradients.map(({ start, end }) => {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, start);
    gradient.addColorStop(1, end);
    return gradient;
  });
};

const calculatePoint = (i, intervalSize, colorRangeInfo) => {
  var { colorStart, colorEnd, useEndAsStart } = colorRangeInfo;
  return useEndAsStart
    ? colorEnd - i * intervalSize
    : colorStart + i * intervalSize;
};

export const interpolateColors = (dataLength, colorScale, colorRangeInfo) => {
  var { colorStart, colorEnd } = colorRangeInfo;
  var colorRange = colorEnd - colorStart;
  var intervalSize = colorRange / dataLength;
  var i, colorPoint;
  var colorArray = [];

  for (i = 0; i < dataLength; i++) {
    colorPoint = calculatePoint(i, intervalSize, colorRangeInfo);
    colorArray.push(colorScale(colorPoint));
  }

  return colorArray;
};

export const getLocalCart = () => {
  const json = localStorage.getItem("bsaLocalCart");
  let cart = JSON.parse(json);
  if (!cart) cart = [];
  return cart;
};

export const setLocalCart = (cart = []) => {
  localStorage.setItem("bsaLocalCart", JSON.stringify(cart));
};
