import React from "react";
import MomentUtils from "@date-io/moment";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import moment from "moment";
import "moment/locale/ru";
import "./App.css";

import Main from "./routes/Main";

const theme = createMuiTheme({
  typography: {
    fontFamily: '"Didact Gothic"',
  },
  palette: {
    primary: {
      main: "#745e8a",
    },
  },
  overrides: {
    MuiTableFooter: {
      "&:last-child td": {
        borderBottom: 0,
      },
    },
  },
});

function App() {
  moment.locale("ru");
  return (
    <MuiPickersUtilsProvider
      libInstance={moment}
      utils={MomentUtils}
      locale="ru"
    >
      <MuiThemeProvider theme={theme}>
        <Main />
      </MuiThemeProvider>
    </MuiPickersUtilsProvider>
  );
}

export default App;
