﻿using System;
using System.Collections.Generic;
using WebApplication.DAL.Entities;

#nullable disable

namespace WebApplication.BLL.DTO
{
    
    public partial class ServiceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Duration { get; set; }
        public int CategoryFk { get; set; }
        public int Status { get; set; }
        public List<SpecialistDTO> Specialists{ get; set; }
    }
}
