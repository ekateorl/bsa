﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class CartTotal
    {
        public string Duration { get; set; }
        public int Price { get; set; }

    }
}
