﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class WorkDayDTO
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Beginning { get; set; }
        public TimeSpan Ending { get; set; }
        public int SpecialistFk { get; set; }
    }
}
