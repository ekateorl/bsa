﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class StatusDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
