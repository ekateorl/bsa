﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class ServiceAppointmentDTO
    {
        public int Id { get; set; }
        public int ServiceFk { get; set; }
        public int AppointmentFk { get; set; }
    }
}
