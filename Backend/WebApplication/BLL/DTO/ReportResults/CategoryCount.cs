﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class CategoryCount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public List<ServiceCount> Services { get; set; }
    }
}
