﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class SpecialistsPerfomance
    {
        public int TotalCount { get; set; }
        public decimal TotalSum { get; set; }
        public List<SpecialistCount> Specialists { get; set; }
    }
}
