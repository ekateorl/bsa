﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class RatingDynamicsResult
    {
        public string Date { get; set; }
        public decimal Rating { get; set; }

    }
}
