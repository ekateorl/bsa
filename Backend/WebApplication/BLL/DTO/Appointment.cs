﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class AppointmentDTO
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public int? TimeSlotFk { get; set; }
        public string Date { get; set; }
        public string Beginning { get; set; }
        public string Duration { get; set; }
        public bool Completed { get; set; }
        public SpecialistDTO Specialist { get; set; }
        public List<string> Services { get; set; }
        public int? Rating { get; set; }
        public string Review { get; set; }
        public bool CanRate { get; set; }
        public ClientDTO Client{ get; set; }
}
}
