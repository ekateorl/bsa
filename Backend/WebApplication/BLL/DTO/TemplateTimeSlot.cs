﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class TemplateTimeSlotDTO
    {
        public int Id { get; set; }
        public string Beginning { get; set; }
        public string Duration { get; set; }
        public int ScheduleTemplateFk { get; set; }
        public int DayOfWeek { get; set; }
    }
}
