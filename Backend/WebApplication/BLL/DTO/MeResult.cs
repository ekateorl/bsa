﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class MeResult
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public List<string> Errors { get; set; }
    }
}
