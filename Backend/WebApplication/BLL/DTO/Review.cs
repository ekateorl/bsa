﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class ReviewDTO
    {
        public int Rating { get; set; }
        public string Review { get; set; }
        public string ReviewTime { get; set; }
        public string ClientName { get; set; }

    }
}
