﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class ApplicationUserDTO
    {
        [Required] public string Name { get; set; }
        [Required] public string Surname { get; set; }
        public string FatherName { get; set; }
        [Required] public string Email { get; set; }
        [Required]  public string PhoneNumber { get; set; }
        [Required]  public string Password { get; set; }
    }
}
