﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class ServiceSpecialistDTO
    {
        public int Id { get; set; }
        public int ServiceFk { get; set; }
        public int SpecialistFk { get; set; }
    }
}
