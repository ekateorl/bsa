﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class AppointmentForDate
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public string Date { get; set; }
        public string Beginning { get; set; }
        public string Duration { get; set; }
        public bool Completed { get; set; }
        public int SpecialistFk { get; set; }
        public List<string> Services { get; set; }
        public int? Rating { get; set; }
        public string Review { get; set; }
        public string ClientName { get; set; }
    }
}
