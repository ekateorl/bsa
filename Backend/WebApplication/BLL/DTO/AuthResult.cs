﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.DTO
{
    public class AuthResult
    {
        public string Token { get; set; }
        public bool Succeeded { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public List<string> Errors { get; set; }
    }
}
