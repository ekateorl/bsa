﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class TimeSlotDTO
    {
        public int Id { get; set; }
        public string Beginning { get; set; }
        public string Duration { get; set; }
        public int WorkDayFk { get; set; }
        public int? AppointmentFk { get; set; }

    }
}
