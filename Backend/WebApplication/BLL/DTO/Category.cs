﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class CategoryDTO
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }
        public string Image { get; set; }
    }
}
