﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.BLL.DTO
{
    public partial class SpecialistDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string Phone { get; set; }
        public string Image { get; set; }
        public string StartDate { get; set; }
        public decimal Rating { get; set; }
        public int RatingCount { get; set; }
        public int QualifyingPeriod { get; set; }
        
        public List<ServiceDTO> Services { get; set; }
    }
}
