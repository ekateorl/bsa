﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IDBDataService
    {
        #region Service
        List<ServiceDTO> GetServices(int categoryId);
        ServiceDTO GetService(int id);
        ServiceDTO CreateService(ServiceDTO service, List<int> specialistIds);
        void UpdateService(ServiceDTO service, List<int> specialistIds);
        bool DeleteService(int id);
        #endregion


        #region Category
        List<CategoryDTO> GetAllCategories();
        CategoryDTO GetCategory(int id);
        void CreateCategory(CategoryDTO category);
        bool UpdateCategory(CategoryDTO category);
        bool DeleteCategory(int id);
        #endregion

        #region Specialist
        List<SpecialistDTO> GetAllSpecialists();

        SpecialistDTO GetSpecialist(int id);
        SpecialistDTO CreateSpecialist(SpecialistDTO specialist, List<int> serviceIds);
        bool UpdateSpecialist(SpecialistDTO specialist, List<int> serviceIds);
        bool DeleteSpecialist(int id);

        #endregion

        
        bool Save();
    }
}
