﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.BLL.Interfaces
{
    public interface ICrudService<TEntity> where TEntity : class
    {
        List<TEntity> GetAll();
        TEntity GetItem(int id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
    }
}
