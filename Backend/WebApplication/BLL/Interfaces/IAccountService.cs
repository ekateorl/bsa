﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.DAL.Entities;

namespace WebApplication.BLL.Interfaces
{
    public interface IAccountService
    {
        AuthResult Register(ApplicationUserDTO user);
        AuthResult SignIn(string Email, string Password, bool RememberMe);
        void SignOut();
        MeResult GetUser(ClaimsPrincipal principal);
        ClientDTO GetUser(int id);
    }
}
