﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IRaitingService
    {
        int RateAppoitnment(int id, string email, int rating, string review);
        void ReacountRating(int specialistId);
        bool CanRate(DateTime date);
        List<ReviewDTO> GetReviews(int specialistId);
    }
}
