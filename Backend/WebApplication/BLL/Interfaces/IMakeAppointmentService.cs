﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IMakeAppointmentService
    {
        List<CartItem> GetCart(string email, out CartTotal cartTotal);
        List<CartItem> GetCartById(List<int> serviceIds, out CartTotal cartTotal);
        bool AddToCart(int serviceId, string email);
        bool RemoveFromCart(int id);
        List<ServiceDTO> GetServicesForUser(int categoryId, string email);
        List<List<int>> GetServiceGroups(List<int> serviceIds);
        public List<SpecialistDTO> GetSpecialistsForServices(List<int> serviceIds);
        List<TimeSlotDTO> GetTimeSlotsForServices(List<int> serviceIds, int specialistId, DateTime date, out string closestDate);
        bool MakeAppointment(string email, List<int> serviceIds, int timeSlotId);
        int CancelAppointment(string email, int id);
        List<AppointmentDTO> GetIncompleteAppointments(string email);
        List<AppointmentDTO> GetCompleteAppointments(string email);
    }
}
