﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IReportService
    {
        List<RatingDynamicsResult> GetRatingDynamics(int specialistId, DateTime start, DateTime end);
        List<CategoryCount> GetCategoryCount(DateTime start, DateTime end);
        SpecialistsPerfomance GetSpecialistsPerfomance(DateTime start, DateTime end);
    }
}
