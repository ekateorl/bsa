﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Interfaces
{
    public interface IScheduleService
    {
        List<TimeSlotDTO> GetTimeSlots(int specialistId, DateTime date);
        List<TimeSlotDTO> CreateTimeSlots(int specialistId, DateTime date, List<TimeSlotDTO> timeSlots);
        bool DeleteTimeSlots(List<int> timeSlotIds);
        bool HasIntersections(List<TimeSlotDTO> list);
        bool HasIntersections(List<TemplateTimeSlotDTO> list);
        List<ScheduleTemplateDTO> GetScheduleTemplates();
        ScheduleTemplateDTO GetScheduleTemplate(int id);
        List<List<TemplateTimeSlotDTO>> GetTemplateTimeSlots(int templateId);
        ScheduleTemplateDTO CreateScheduleTemplate(ScheduleTemplateDTO template);
        bool UpdateScheduleTemplate(ScheduleTemplateDTO template);
        bool CreateTemplateTimeSlots(List<List<TemplateTimeSlotDTO>> timeSlots, int templateId);
        bool DeleteScheduleTemplate(int id);
        bool DeleteTemplateTimeSlots(List<int> timeSlotIds);
        TimeSlotDTO GetTimeSlot(int id);
        TemplateTimeSlotDTO GetTemplateTimeSlot(int id);
        List<AppointmentForDate> GetAppointments(DateTime date);
        AppointmentDTO GetAppointment(int id);
    }
}
