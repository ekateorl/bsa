﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.BLL.DTO;
using AutoMapper;
using System.Security.Claims;

namespace WebApplication.BLL.Services
{
    public class AccountService : IAccountService
    {
        private IDbRepository _crud;
        private IApplicationUserStore _userStore;
        private Mapper userMapper;
        public AccountService(IDbRepository crud)
        {
            _crud = crud;
            _userStore = crud.UserStore;
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, ApplicationUserDTO>());
            userMapper = new Mapper(config);
        }
        public AuthResult Register(ApplicationUserDTO user)
        {
            var appUser = new ApplicationUser
            {
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Name = user.Name,
                Surname = user.Surname,
                FatherName = user.FatherName,
                UserName = user.Email,
            };
            var result =_userStore.Create(appUser, user.Password, "user");
            result.Wait();
            if (result.Result.Succeeded)
            {
                _userStore.SignIn(user.Email, user.Password, false);
                string jwtToken = _userStore.GenerateJwtToken(appUser);
                var roles = _userStore.GetUserRole(appUser);
                string role = null;
                if (roles.Count > 0)
                    role = roles[0];
                return new AuthResult()
                {
                    Token = jwtToken,
                    Succeeded = true,
                    Role = role,
                    Name = user.Name,
                };
            }
            return new AuthResult() { Succeeded = false, Errors = result.Result.Errors.Select(e => e.Description).ToList() };
        }

        public AuthResult SignIn(string Email, string Password, bool RememberMe)
        {
            var result = _userStore.SignIn(Email, Password, RememberMe);
            if (result.Succeeded)
            {
                var user = _userStore.GetUser(Email);
                string jwtToken = _userStore.GenerateJwtToken(user);
                var roles = _userStore.GetUserRole(user);
                string role = null;
                if (roles.Count > 0)
                    role = roles[0];
                return new AuthResult()
                {
                    Token = jwtToken,
                    Succeeded = true,
                    Role = role,
                    Name = user.Name,
                };

            }
            return new AuthResult() { Succeeded = false };
        }

        public void SignOut()
        {
            _userStore.SignOut();
        }

        public MeResult GetUser(ClaimsPrincipal principal)
        {
            var res = _userStore.GetUser(principal.Claims.ToList()[0].Value);
            if (res != null)
            {
                var roles = _userStore.GetUserRole(res);
                string role = null;
                if (roles.Count > 0)
                    role = roles[0];
                return new MeResult
                {
                    UserName = res.UserName,
                    Role = role
                };
            }
            return null;
        }

        public ClientDTO GetUser(int id)
        {
            var res = _userStore.GetUser(id);
            if (res != null)
            {
                var roles = _userStore.GetUserRole(res);
                string role = null;
                if (roles.Count > 0)
                    role = roles[0];
                return new ClientDTO
                {
                    Id = res.Id,
                    Name = res.Name,
                    Email = res.Email,
                    PhoneNumber = res.PhoneNumber
                };
            }
            return null;
        }
    }
}
