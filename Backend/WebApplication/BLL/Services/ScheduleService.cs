﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.BLL.DTO;

namespace WebApplication.BLL.Services
{
    public class ScheduleService : IScheduleService
    {
        private IDbRepository _crud;
        public ScheduleService(IDbRepository crud)
        {
            _crud = crud;
        }

        public List<TimeSlotDTO> GetTimeSlots(int specialistId, DateTime date)
        {
            WorkDay workDay = _crud.WorkDay.GetAll().Where(i => i.Date == date && i.SpecialistFk == specialistId).FirstOrDefault();
            if (workDay == null)
                return null;
            var timeSlots = _crud.TimeSlot.GetAll().Where(i => i.WorkDayFk == workDay.Id).ToList();
            if (timeSlots == null)
                timeSlots = new List<TimeSlot>();
            return timeSlots.Select(i => new TimeSlotDTO
            {
                Id = i.Id,
                Beginning = i.Beginning.ToString(@"hh\:mm"),
                Duration = i.Duration.ToString(@"hh\:mm"),
                WorkDayFk = i.WorkDayFk,
                AppointmentFk = i.AppointmentFk
            }
           ).ToList();
        }

        public bool HasIntersections(List<TimeSlotDTO> list)
        {
            if (list == null || list.Count < 2)
                return false;
            list = list.OrderBy(i => i.Beginning).ToList();
            for (int i = 0; i < list.Count - 1; i++)
            {
                TimeSlot cur = new TimeSlot
                {
                    Beginning = TimeSpan.Parse(list[i].Beginning),
                    Duration = TimeSpan.Parse(list[i].Duration),
                };

                TimeSlot next = new TimeSlot
                {
                    Beginning = TimeSpan.Parse(list[i + 1].Beginning),
                };

                if (cur.Beginning + cur.Duration > next.Beginning)
                    return true;
            }
            return false;
        }

        public bool HasIntersections(List<TemplateTimeSlotDTO> list)
        {
            if (list == null || list.Count < 2)
                return false;
            list = list.OrderBy(i => i.Beginning).ToList();
            for (int i = 0; i < list.Count - 1; i++)
            {
                TimeSlot cur = new TimeSlot
                {
                    Beginning = TimeSpan.Parse(list[i].Beginning),
                    Duration = TimeSpan.Parse(list[i].Duration),
                };

                TimeSlot next = new TimeSlot
                {
                    Beginning = TimeSpan.Parse(list[i + 1].Beginning),
                };

                if (cur.Beginning + cur.Duration > next.Beginning)
                    return true;
            }
            return false;
        }
        bool IntersectsWidthTimeSlots(TimeSlot timeSlot, List<TimeSlot> list)
        {
            if (timeSlot == null)
                return false;
            if (list == null || list.Count == 0)
                return false;
            foreach (TimeSlot t in list)
            {
                TimeSpan beginning1 = t.Beginning;
                TimeSpan end1 = t.Beginning + t.Duration;

                TimeSpan beginning2 = timeSlot.Beginning;
                TimeSpan end2 = timeSlot.Beginning + timeSlot.Duration;

                if (beginning1 < end2 && end1 > beginning2)
                    return true;
            }
            return false;
        }

        bool IntersectsWidthTimeSlots(TemplateTimeSlot timeSlot, List<TemplateTimeSlot> list)
        {
            if (timeSlot == null)
                return false;
            if (list == null || list.Count == 0)
                return false;
            foreach (TemplateTimeSlot t in list)
            {
                TimeSpan beginning1 = t.Beginning;
                TimeSpan end1 = t.Beginning + t.Duration;

                TimeSpan beginning2 = timeSlot.Beginning;
                TimeSpan end2 = timeSlot.Beginning + timeSlot.Duration;

                if (beginning1 < end2 && end1 > beginning2)
                    return true;
            }
            return false;
        }

        public List<TimeSlotDTO> CreateTimeSlots(int specialistId, DateTime date, List<TimeSlotDTO> timeSlots)
        {
            Specialist specialist = _crud.Specialist.GetItem(specialistId);
            if (specialist == null || timeSlots == null || HasIntersections(timeSlots))
                return null;
            WorkDay workDay = _crud.WorkDay.GetAll().Where(i => i.Date == date && i.SpecialistFk == specialistId).FirstOrDefault();
            if (workDay == null)
            {
                if (timeSlots.Count == 0)
                    return timeSlots;
                else
                {
                    workDay = new WorkDay
                    {
                        Date = date,
                        SpecialistFk = specialistId
                    };
                    _crud.WorkDay.Create(workDay);
                    _crud.Save();
                }
            }

            var existingTimeSlots = _crud.TimeSlot.GetAll().Where(i => i.WorkDayFk == workDay.Id).ToList();

            List<TimeSlot> newTimeSlots = new List<TimeSlot>();
            foreach (TimeSlotDTO dto in timeSlots)
            {
                TimeSlot timeSlot = new TimeSlot
                {
                    Beginning = TimeSpan.Parse(dto.Beginning),
                    Duration = TimeSpan.Parse(dto.Duration),
                    WorkDayFk = workDay.Id,
                };
                if (!IntersectsWidthTimeSlots(timeSlot, existingTimeSlots))
                {
                    _crud.TimeSlot.Create(timeSlot);
                    newTimeSlots.Add(timeSlot);
                }
            }
            _crud.Save();

            existingTimeSlots.AddRange(newTimeSlots);
            existingTimeSlots.OrderBy(i => i.Beginning);
            workDay.Beginning = existingTimeSlots.First<TimeSlot>().Beginning;
            var last = existingTimeSlots.Last<TimeSlot>();
            workDay.Ending = last.Beginning + last.Duration;
            _crud.WorkDay.Update(workDay);
            return existingTimeSlots.Select(i => new TimeSlotDTO
            {
                Id = i.Id,
                Beginning = i.Beginning.ToString(@"hh\:mm"),
                Duration = i.Duration.ToString(@"hh\:mm"),
                WorkDayFk = i.WorkDayFk,
                AppointmentFk = i.AppointmentFk

            }
            ).ToList();
        }

        public bool DeleteTimeSlots(List<int> timeSlotIds)
        {
            if (timeSlotIds == null)
                return false;
            foreach (int id in timeSlotIds)
                if (_crud.TimeSlot.GetItem(id) == null)
                    return false;
            foreach (int id in timeSlotIds)
            {
                var timeSlot = _crud.TimeSlot.GetItem(id);
                if (timeSlot.AppointmentFk == null)
                    _crud.TimeSlot.Delete(id);
            }
            _crud.Save();
            return true;
        }

        public List<List<TemplateTimeSlotDTO>> GetTemplateTimeSlots(int templateId)
        {
            var template = _crud.ScheduleTemplate.GetItem(templateId);
            if (template == null)
                return null;

            var timeSlots = _crud.TemplateTimeSlot.GetAll().Where(i => i.ScheduleTemplateFk == templateId).ToList();
            var result = new List<List<TemplateTimeSlotDTO>>();
            for (int day = 0; day < 7; day++)
            {
                var list = timeSlots.Where(i => i.DayOfWeek == day).ToList();
                result.Add(list.Select(i => new TemplateTimeSlotDTO
                {
                    Id = i.Id,
                    Beginning = i.Beginning.ToString(@"hh\:mm"),
                    Duration = i.Duration.ToString(@"hh\:mm"),
                    DayOfWeek = i.DayOfWeek,
                    ScheduleTemplateFk = i.ScheduleTemplateFk

                }).ToList());
            }
            return result;
        }

        public List<ScheduleTemplateDTO> GetScheduleTemplates()
        {
            var templates = _crud.ScheduleTemplate.GetAll().ToList();
            /*var result = new List<ScheduleTemplateDTO>();
            foreach (ScheduleTemplate template in templates)
                result.Add(GetScheduleTemplateDTO(template));*/
            var result = templates.Select(i => new ScheduleTemplateDTO
            {
                Id = i.Id,
                Name = i.Name,
            }).ToList();
            return result;
        }
        public ScheduleTemplateDTO GetScheduleTemplate(int id)
        {
            var template = _crud.ScheduleTemplate.GetItem(id);
            if (template == null)
                return null;
            var result = new ScheduleTemplateDTO
            {
                Id = id,
                Name = template.Name,
            };
            return result;
        }

        public bool CreateTemplateTimeSlots(List<List<TemplateTimeSlotDTO>> timeSlots, int templateId)
        {
            if (_crud.ScheduleTemplate.GetItem(templateId) == null)
                return false;
            if (timeSlots == null)
                return false;
            foreach (List<TemplateTimeSlotDTO> list in timeSlots)
            {
                if (HasIntersections(list))
                    return false;
            }


            int index = 0;
            foreach (List<TemplateTimeSlotDTO> list in timeSlots)
            {
                if (index > 6)
                    break;
                var existingTimeSlots = _crud.TemplateTimeSlot.GetAll().
                    Where(i => i.ScheduleTemplateFk == templateId && i.DayOfWeek == index).
                    ToList();

                foreach (TemplateTimeSlotDTO timeSlotDTO in list)
                {
                    var newTimeSlot = new TemplateTimeSlot
                    {
                        Beginning = TimeSpan.Parse(timeSlotDTO.Beginning),
                        Duration = TimeSpan.Parse(timeSlotDTO.Duration),
                        DayOfWeek = index,
                        ScheduleTemplateFk = templateId,
                    };
                    if (!IntersectsWidthTimeSlots(newTimeSlot, existingTimeSlots))
                        _crud.TemplateTimeSlot.Create(newTimeSlot);
                }
                index++;
            }
            _crud.Save();
            return true;
        }

        public ScheduleTemplateDTO CreateScheduleTemplate(ScheduleTemplateDTO template)
        {
            var newTemplate = new ScheduleTemplate();
            newTemplate.Name = template.Name;
            _crud.ScheduleTemplate.Create(newTemplate);
            _crud.Save();
            template.Id = newTemplate.Id;
            return template;
        }

        public bool UpdateScheduleTemplate(ScheduleTemplateDTO template)
        {
            var updatedTemplate = _crud.ScheduleTemplate.GetItem(template.Id);
            if (updatedTemplate == null)
                return false;
            updatedTemplate.Name = template.Name;
            _crud.ScheduleTemplate.Update(updatedTemplate);
            _crud.Save();
            return true;
        }

        public bool DeleteScheduleTemplate(int id)
        {
            if (_crud.ScheduleTemplate.GetItem(id) == null)
                return false;
            _crud.ScheduleTemplate.Delete(id);
            _crud.Save();
            return true;
        }

        public bool DeleteTemplateTimeSlots(List<int> timeSlotIds)
        {
            if (timeSlotIds == null)
                return false;
            foreach (int id in timeSlotIds)
                if (_crud.TemplateTimeSlot.GetItem(id) == null)
                    return false;
            foreach (int id in timeSlotIds)
                _crud.TemplateTimeSlot.Delete(id);
            _crud.Save();
            return true;
        }

        public TimeSlotDTO GetTimeSlot(int id)
        {
            var t = _crud.TimeSlot.GetItem(id);
            if (t == null) return null;

            var dto = new TimeSlotDTO
            {
                Id = t.Id,
                Beginning = t.Beginning.ToString(@"hh\:mm"),
                Duration = t.Duration.ToString(@"hh\:mm"),
                WorkDayFk = t.WorkDayFk,
                AppointmentFk = t.AppointmentFk
            };
            return dto;
        }

        public TemplateTimeSlotDTO GetTemplateTimeSlot(int id)
        {
            var t = _crud.TemplateTimeSlot.GetItem(id);
            if (t == null) return null;

            var dto = new TemplateTimeSlotDTO
            {
                Id = t.Id,
                Beginning = t.Beginning.ToString(@"hh\:mm"),
                Duration = t.Duration.ToString(@"hh\:mm"),
                DayOfWeek = t.DayOfWeek,
                ScheduleTemplateFk = t.ScheduleTemplateFk
            };
            return dto;
        }

        public List<AppointmentForDate> GetAppointments(DateTime date)
        {
            var appQuery = _crud.Appointment.GetAll().
               Where(i => i.StatusFk != 1).
               Join(_crud.TimeSlot.GetAll(), a => a.TimeSlotFk, t => t.Id,
               (a, t) => new { a, t }).
               Join(_crud.WorkDay.GetAll(), o => o.t.WorkDayFk, w => w.Id,
               (o, w) => new { o.a, o.t, w }).
               Where(i => i.w.Date == date).
               ToList();

            if (appQuery == null)
                return null;

            var result = appQuery.Select(i => new AppointmentForDate
            {
                Id = i.a.Id,
                Date = i.w.Date.ToString("yyyy-MM-dd"),
                Beginning = i.t.Beginning.ToString(@"hh\:mm"),
                Duration = i.a.Duration.ToString(@"hh\:mm"),
                SpecialistFk = i.w.SpecialistFk,
                Rating = i.a.Rating,
                Review = i.a.Review,
            }).ToList();

            for (int i = 0; i < appQuery.Count(); i++)
            {
                var client = _crud.UserStore.GetUser(appQuery[i].a.ClientFk);
                result[i].ClientName = client.Surname + " " +client.Name;
                result[i].Services = new List<string>();
                if (appQuery[i].w.Date > DateTime.Today || (appQuery[i].w.Date == DateTime.Today &&
                        appQuery[i].t.Beginning + TimeSpan.Parse(result[i].Duration) > DateTime.Now.TimeOfDay))
                    result[i].Completed = false;
                else result[i].Completed = true;
                var items = _crud.ServiceAppointment.GetAll().Where(j => j.AppointmentFk == result[i].Id).ToList();
                foreach (ServiceAppointment item in items)
                    result[i].Services.Add(_crud.Service.GetItem(item.ServiceFk).Name);
            }
            return result;
        }

        public AppointmentDTO GetAppointment(int id)
        {
            var app = _crud.Appointment.GetItem(id);
            if (app == null)
                return null;

            var timeSlot = _crud.TimeSlot.GetItem(app.TimeSlotFk.Value);
            var workDay = _crud.WorkDay.GetItem(timeSlot.WorkDayFk);
            var specialist = _crud.Specialist.GetItem(workDay.SpecialistFk);
            var specialistDTO = new SpecialistDTO
            {
                Id = specialist.Id,
                Name = specialist.Name,
                Surname = specialist.Surname,
                FatherName = specialist.FatherName,
                Image = specialist.Image
            };

            var client = _crud.UserStore.GetUser(app.ClientFk);
            var clientDTO = new ClientDTO
            {
                Id = client.Id,
                Name = client.Surname + " " + client.Name + " " + client.FatherName,
                PhoneNumber = client.PhoneNumber,
                Email = client.Email,
            };

            var result = new AppointmentDTO
            {
                Id = app.Id,
                Date = workDay.Date.ToString("yyyy-MM-dd"),
                Beginning = timeSlot.Beginning.ToString(@"hh\:mm"),
                Duration = app.Duration.ToString(@"hh\:mm"),
                Specialist = specialistDTO,
                Rating = app.Rating,
                Review = app.Review,
                Client = clientDTO,
                Price = (int)app.Price
            };

            result.Services = new List<string>();
            if (workDay.Date > DateTime.Today || (workDay.Date == DateTime.Today &&
                    timeSlot.Beginning + app.Duration > DateTime.Now.TimeOfDay))
                result.Completed = false;
            else result.Completed = true;
            var items = _crud.ServiceAppointment.GetAll().Where(j => j.AppointmentFk == result.Id).ToList();
            foreach (ServiceAppointment item in items)
                result.Services.Add(_crud.Service.GetItem(item.ServiceFk).Name);

            return result;
        }
    }
}
