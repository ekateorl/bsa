﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    public class ReportService : IReportService
    {
        private IDbRepository _crud;
        public ReportService(IDbRepository crud)
        {
            _crud = crud;
        }

        public List<RatingDynamicsResult> GetRatingDynamics(int specialistId, DateTime start, DateTime end)
        {
            var specialist = _crud.Specialist.GetItem(specialistId);
            if (specialist == null || start > end) return null;
            var result = new List<RatingDynamicsResult>();

            var appQuery = _crud.Appointment.GetAll().
               Where(i => i.Rating != null && i.ReviewTime.Value.Date < start.Date).
               Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
               (i, j) => new { Id = i.Id, Rating = i.Rating, WorkDayFk = j.WorkDayFk }).
               Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
               (i, j) => new { Id = i.Id, Rating = i.Rating, SpecialistFk = j.SpecialistFk }).
               Where(i => i.SpecialistFk == specialistId).
               ToList();

            decimal ratingSum = appQuery.Sum(i => i.Rating).Value;
            int count = appQuery.Count();

            for (DateTime date = start.Date; date <= end.Date; date = date.AddDays(1))
            {
                var appointments = _crud.Appointment.GetAll().
                    Where(i => i.Rating != null && i.ReviewTime.Value.Date == date.Date).
                    Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
                    (i, j) => new { Id = i.Id, Rating = i.Rating, WorkDayFk = j.WorkDayFk }).
                    Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
                    (i, j) => new { Id = i.Id, Rating = i.Rating, SpecialistFk = j.SpecialistFk }).
                    Where(i => i.SpecialistFk == specialistId).ToList();

                if (appointments != null && appointments.Count > 0)
                {
                    ratingSum += appointments.Sum(i => i.Rating).Value;
                    count += appointments.Count();
                }

                if (date == start.Date || date == end.Date || (appointments != null && appointments.Count > 0))
                    result.Add(new RatingDynamicsResult
                    {
                        Date = date.ToString("yyyy-MM-dd"),
                        Rating = count != 0 ? ratingSum / count : 0,
                    });
            }
            return result;
        }

        public List<CategoryCount> GetCategoryCount(DateTime start, DateTime end)
        {
            List<int> appointmentIds = _crud.Appointment.GetAll().
                Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
               (i, j) => new { Id = i.Id, WorkDayFk = j.WorkDayFk }).
               Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
               (i, j) => new { Id = i.Id, Date = j.Date }).
               Where(i => i.Date >= start && i.Date <= end).
               Select(i => i.Id).ToList();

            if (appointmentIds == null)
                return null;

            var services = _crud.ServiceAppointment.GetAll().Where(i => appointmentIds.Contains(i.AppointmentFk)).
                Join(_crud.Service.GetAll(), i => i.ServiceFk, j => j.Id, (i, j) => j).ToList().
                GroupBy(i => i.Id).
                Select(g => new { Service = g.First(), Count = g.Count() }).
                ToList();

            var result = new List<CategoryCount>();
            var categories = services.GroupBy(i => i.Service.CategoryFk);
            foreach (var g in categories)
            {
                int categoryId = g.First().Service.CategoryFk;
                var category = _crud.Category.GetItem(categoryId);
                result.Add(new CategoryCount
                {
                    Id = categoryId,
                    Name = category.Name,
                    Count = g.Sum(i => i.Count),
                    Services = g.Select(i => new ServiceCount
                    {
                        Id = i.Service.Id,
                        Name = i.Service.Name,
                        Count = i.Count,
                    }).ToList()
                });
            }
            return result;
        }

        SpecialistCount GetSpecialistCount(DateTime start, DateTime end, int specialistId)
        {
            var specialist = _crud.Specialist.GetItem(specialistId);
            if (specialist == null)
                return null;

            var appointments = _crud.Appointment.GetAll().
                Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
               (i, j) => new { Id = i.Id, WorkDayFk = j.WorkDayFk, Price = i.Price }).
               Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
               (i, j) => new { Id = i.Id, Date = j.Date, SpecialistFk = j.SpecialistFk, Price = i.Price }).
               Where(i => i.Date >= start && i.Date <= end && i.SpecialistFk == specialistId)
               .ToList();

            if (appointments == null)
                return null;

            var appointmentIds = appointments.Select(i => i.Id);
            var serviceCount = _crud.ServiceAppointment.GetAll().Where(i => appointmentIds.Contains(i.AppointmentFk)).Count();
            var sum = appointments.Sum(i => i.Price);

            return new SpecialistCount { Id = specialist.Id, Name = specialist.Name, Surname = specialist.Surname, Count = serviceCount, Sum = sum };
        }

        public SpecialistsPerfomance GetSpecialistsPerfomance(DateTime start, DateTime end)
        {
            var specialists = _crud.Specialist.GetAll().Where(i => !i.Deleted).ToList();
            var result = new SpecialistsPerfomance();
            result.Specialists = new List<SpecialistCount>();
            foreach (var specialist in specialists)
                result.Specialists.Add(GetSpecialistCount(start, end, specialist.Id));

            result.TotalCount = result.Specialists.Sum(i => i.Count);
            result.TotalSum = result.Specialists.Sum(i => i.Sum);
            return result;
        }
    }
}
