﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.DAL.Entities;
using WebApplication.DAL.Interfaces;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.BLL.Services
{
    public class DBDataService : IDBDataService
    {
        private IDbRepository _crud;

        private Mapper serviceMapper;
        private Mapper categoryMapper;
        private Mapper specialistMapper;
        public DBDataService(IDbRepository crud)
        {
            _crud = crud;

            MapperConfiguration serviceConfig = new MapperConfiguration(cfg => cfg.CreateMap<Service, ServiceDTO>());
            serviceMapper = new Mapper(serviceConfig);

            MapperConfiguration categoryConfig = new MapperConfiguration(cfg => cfg.CreateMap<Category, CategoryDTO>());
            categoryMapper = new Mapper(categoryConfig);

            MapperConfiguration specialistConfig = new MapperConfiguration(cfg => cfg.CreateMap<Specialist, SpecialistDTO>());
            specialistMapper = new Mapper(specialistConfig);
        }

        #region Service

        public ServiceDTO CreateService(ServiceDTO service, List<int> specialistIds)
        {
            Service newService = new Service
            {
                Name = service.Name,
                Price = service.Price,
                Duration = TimeSpan.Parse(service.Duration),
                Description = service.Description,
                CategoryFk = service.CategoryFk,
            };
            _crud.Service.Create(newService);
            Save();
            if (specialistIds != null)
                foreach (int id in specialistIds)
                {
                    if (_crud.Specialist.GetItem(id) != null)
                        _crud.ServiceSpecialist.Create(new ServiceSpecialist
                        {
                            ServiceFk = newService.Id,
                            SpecialistFk = id,
                        });
                }
            Save();

            service.Id = newService.Id;
            var serviceSpecialists = _crud.ServiceSpecialist.GetAll().Where(s => s.ServiceFk == service.Id).ToList();
            service.Specialists = new List<SpecialistDTO>();
            foreach (ServiceSpecialist ss in serviceSpecialists)
            {
                var specialist = _crud.Specialist.GetItem(ss.SpecialistFk);
                service.Specialists.Add(new SpecialistDTO
                {
                    Id = specialist.Id,
                    Surname = specialist.Surname,
                    Name = specialist.Name,
                    FatherName = specialist.FatherName,
                    Image = specialist.Image
                });
            }
            return service;
        }
        public List<ServiceDTO> GetServices(int categoryId)
        {
            var list = _crud.Service.GetAll().Where(i => i.CategoryFk == categoryId && i.Deleted == false).ToList();

            var result = list.Select(i => new ServiceDTO
            {
                Id = i.Id,
                Name = i.Name,
                Price = (int)i.Price,
                Duration = i.Duration.ToString(@"hh\:mm"),
                Description = i.Description,
                CategoryFk = i.CategoryFk,
                Status = 3,
            }).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                result[i].Specialists = new List<SpecialistDTO>();
                var serviceSpecialists = _crud.ServiceSpecialist.GetAll().Where(s => s.ServiceFk == result[i].Id).ToList();
                foreach (ServiceSpecialist ss in serviceSpecialists)
                {
                    var specialist = _crud.Specialist.GetItem(ss.SpecialistFk);
                    result[i].Specialists.Add(new SpecialistDTO
                    {
                        Id = specialist.Id,
                        Surname = specialist.Surname,
                        Name = specialist.Name,
                        FatherName = specialist.FatherName,
                        Image = specialist.Image
                    });
                }
            }
            return result;
        }
        public ServiceDTO GetService(int id)
        {
            Service service = _crud.Service.GetItem(id);
            return serviceMapper.Map<ServiceDTO>(service);
        }
        public void UpdateService(ServiceDTO service, List<int> specialistIds)
        {
            Service updatedService = _crud.Service.GetItem(service.Id);
            if (updatedService != null)
            {
                updatedService.Name = service.Name;
                updatedService.Price = service.Price;
                updatedService.Duration = TimeSpan.Parse(service.Duration);
                updatedService.Description = service.Description;
                updatedService.CategoryFk = service.CategoryFk;

                _crud.Service.Update(updatedService);
                Save();
                var serviceSpecialistList = _crud.ServiceSpecialist.GetAll().Where(i => i.ServiceFk == service.Id).ToList();
                var currentSpecialists = serviceSpecialistList.Select(i => i.SpecialistFk).ToList();
                List<int> specialistsToDelete;
                if (specialistIds != null)
                {
                    specialistsToDelete = currentSpecialists.Except(specialistIds).ToList();
                    var specialistsToCreate = specialistIds.Except(currentSpecialists).ToList();
                    foreach (int id in specialistsToCreate)
                    {
                        if (_crud.Specialist.GetItem(id) != null)
                            _crud.ServiceSpecialist.Create(new ServiceSpecialist
                            {
                                ServiceFk = service.Id,
                                SpecialistFk = id,
                            });
                    }
                }
                else
                    specialistsToDelete = currentSpecialists;
                foreach (int id in specialistsToDelete)
                {
                    var serviceSpecialist = serviceSpecialistList.Where(i => i.SpecialistFk == id).FirstOrDefault();
                    if (serviceSpecialist != null)
                        _crud.ServiceSpecialist.Delete(serviceSpecialist.Id);
                }
                Save();
            }
        }
        public bool DeleteService(int id)
        {
            var service = _crud.Service.GetItem(id);
            if (service != null)
            {
                service.Deleted = true;
                _crud.Service.Update(service);
                var serviceSpecialistList = _crud.ServiceSpecialist.GetAll().Where(i => i.ServiceFk == id);
                foreach (ServiceSpecialist s in serviceSpecialistList)
                    _crud.ServiceSpecialist.Delete(s.Id);
                Save();
                return true;
            }
            return false;
        }
        #endregion

        #region Category
        public List<CategoryDTO> GetAllCategories()
        {
            var list = _crud.Category.GetAll().Where(i => i.Deleted == false).ToList();
            return categoryMapper.Map<List<CategoryDTO>>(list);
        }
        public CategoryDTO GetCategory(int id)
        {
            Category category = _crud.Category.GetItem(id);
            return categoryMapper.Map<CategoryDTO>(category);
        }

        public void CreateCategory(CategoryDTO category)
        {
            Category newCategory = new Category
            {
                Name = category.Name,
                Image = category.Image,
            };
            _crud.Category.Create(newCategory);
            Save();
            category.Id = newCategory.Id;
        }
        public bool UpdateCategory(CategoryDTO category)
        {
            var newCategory = _crud.Category.GetItem(category.Id);
            if (newCategory != null)
            {
                newCategory.Id = category.Id;
                newCategory.Name = category.Name;
                newCategory.Image = category.Image;

                _crud.Category.Update(newCategory);
                Save();
                return true;
            }
            else return false;
        }
        public bool DeleteCategory(int id)
        {
            var category = _crud.Category.GetItem(id);
            if (category != null)
            {
                category.Deleted = true;
                var services = _crud.Service.GetAll().Where(i => i.CategoryFk == category.Id).ToList();
                foreach (Service service in services)
                {
                    service.Deleted = true;
                    _crud.Service.Update(service);
                }
                _crud.Category.Update(category);
                Save();
                return true;
            }
            return false;
        }
        #endregion

        #region Specialist

        int differenceInMonths(DateTime date1, DateTime date2)
        {
            return ((date1.Year - date2.Year) * 12) + date1.Month - date2.Month;
        }
        public List<SpecialistDTO> GetAllSpecialists()
        {
            var list = _crud.Specialist.GetAll().Where(i => i.Deleted == false).ToList();
            var result = new List<SpecialistDTO>();
            foreach (Specialist s in list)
            {
                SpecialistDTO dto = new SpecialistDTO();
                
                dto.Id = s.Id;
                dto.Name = s.Name;
                dto.Surname = s.Surname;
                dto.FatherName = s.FatherName;
                dto.Image = s.Image;
                dto.Phone = s.Phone;
                dto.StartDate = s.StartDate.ToString("yyyy-MM-dd");
                dto.Rating = s.Rating;
                dto.RatingCount = s.RatingCount;
                dto.QualifyingPeriod = differenceInMonths(DateTime.Today, s.StartDate);

                result.Add(dto);
            }
            for (int i = 0; i < result.Count; i++)
            {
                var serviceSpecialists = _crud.ServiceSpecialist.GetAll().Where(s => s.SpecialistFk == result[i].Id).ToList();
                result[i].Services = new List<ServiceDTO>();
                foreach (ServiceSpecialist ss in serviceSpecialists)
                {
                    var service = _crud.Service.GetItem(ss.ServiceFk);
                    result[i].Services.Add(new ServiceDTO
                    {
                        Id = service.Id,
                        Name = service.Name,
                    });
                }
            }
            return result;
        }
        public SpecialistDTO GetSpecialist(int id)
        {
            var s = _crud.Specialist.GetItem(id);
            if (s == null) return null;

            SpecialistDTO dto = new SpecialistDTO();
            dto.Id = s.Id;
            dto.Name = s.Name;
            dto.Surname = s.Surname;
            dto.FatherName = s.FatherName;
            dto.Image = s.Image;
            dto.Phone = s.Phone;
            dto.StartDate = s.StartDate.ToString("yyyy-MM-dd");
            dto.Rating = s.Rating;
            dto.RatingCount = s.RatingCount;
            dto.QualifyingPeriod = differenceInMonths(DateTime.Today, s.StartDate);
            return dto;
        }

        public SpecialistDTO CreateSpecialist(SpecialistDTO specialist, List<int> serviceIds)
        {
            Specialist newSpecialist = new Specialist
            {
                Name = specialist.Name,
                Surname = specialist.Surname,
                FatherName = specialist.FatherName,
                Image = specialist.Image,
                Phone = specialist.Phone,
            };
            newSpecialist.StartDate = DateTime.Parse(specialist.StartDate);
            _crud.Specialist.Create(newSpecialist);
            Save();
            if (serviceIds != null)
                foreach (int id in serviceIds)
                {
                    if (_crud.Service.GetItem(id) != null)
                        _crud.ServiceSpecialist.Create(new ServiceSpecialist
                        {
                            ServiceFk = id,
                            SpecialistFk = newSpecialist.Id,
                        });
                }
            Save();

            specialist.Id = newSpecialist.Id;
            var serviceSpecialists = _crud.ServiceSpecialist.GetAll().Where(s => s.SpecialistFk == specialist.Id).ToList();
            specialist.Services = new List<ServiceDTO>();
            foreach (ServiceSpecialist ss in serviceSpecialists)
            {
                var service = _crud.Service.GetItem(ss.ServiceFk);
                specialist.Services.Add(new ServiceDTO
                {
                    Id = service.Id,
                    Name = service.Name,
                });
            }
            return specialist;
        }
        public bool UpdateSpecialist(SpecialistDTO specialist, List<int> serviceIds)
        {
            Specialist updatedSpecialist = _crud.Specialist.GetItem(specialist.Id);
            if (updatedSpecialist != null)
            {
                updatedSpecialist.Id = specialist.Id;
                updatedSpecialist.Name = specialist.Name;
                updatedSpecialist.Surname = specialist.Surname;
                updatedSpecialist.FatherName = specialist.FatherName;
                updatedSpecialist.Image = specialist.Image;
                updatedSpecialist.Phone = specialist.Phone;
                updatedSpecialist.StartDate = DateTime.Parse(specialist.StartDate);

                _crud.Specialist.Update(updatedSpecialist);
                Save();
                var serviceSpecialistList = _crud.ServiceSpecialist.GetAll().Where(i => i.SpecialistFk == specialist.Id).ToList();
                var currentServices = serviceSpecialistList.Select(i => i.ServiceFk).ToList();
                List<int> servicesToDelete;
                if (serviceIds != null)
                {
                    servicesToDelete = currentServices.Except(serviceIds).ToList();
                    var servicesToCreate = serviceIds.Except(currentServices).ToList();
                    foreach (int id in servicesToCreate)
                    {
                        if (_crud.Service.GetItem(id) != null)
                            _crud.ServiceSpecialist.Create(new ServiceSpecialist
                            {
                                ServiceFk = id,
                                SpecialistFk = specialist.Id,
                            });
                    }
                }
                else
                    servicesToDelete = currentServices;
                foreach (int id in servicesToDelete)
                {
                    var serviceSpecialist = serviceSpecialistList.Where(i => i.ServiceFk == id).FirstOrDefault();
                    if (serviceSpecialist != null)
                        _crud.ServiceSpecialist.Delete(serviceSpecialist.Id);
                }
                Save();
                return true;
            }
            return false;
        }
        public bool DeleteSpecialist(int id)
        {
            var specialist = _crud.Specialist.GetItem(id);
            if (specialist != null)
            {
                specialist.Deleted = true;
                _crud.Specialist.Update(specialist);
                var serviceSpecialistList = _crud.ServiceSpecialist.GetAll().Where(i => i.SpecialistFk == id);
                foreach (ServiceSpecialist s in serviceSpecialistList)
                    _crud.ServiceSpecialist.Delete(s.Id);
                Save();
                return true;
            }
            return false;
        }
        #endregion

        public bool Save()
        {
            if (_crud.Save() > 0) return true;
            return false;
        }
    }
}
