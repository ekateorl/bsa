﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.DAL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.BLL.Services
{
    public class MakeAppointmentService : IMakeAppointmentService
    {
        private IDbRepository _crud;
        public MakeAppointmentService(IDbRepository crud)
        {
            _crud = crud;
        }

        Appointment GetCartAppointment(int userId)
        {
            return _crud.Appointment.GetAll().Where(i => i.ClientFk == userId && i.StatusFk == 1).FirstOrDefault();
        }
        Appointment GetCartApp(int userId)
        {
            Appointment app = GetCartAppointment(userId);
            if (app == null)
            {
                app = new Appointment
                {
                    Price = 0,
                    Duration = new TimeSpan(0),
                    StatusFk = 1,
                    ClientFk = userId
                };
                _crud.Appointment.Create(app);
                _crud.Save();
            }
            return app;
        }
        List<ServiceAppointment> GetCart(int userId)
        {
            Appointment app = GetCartApp(userId);
            var items = _crud.ServiceAppointment.GetAll().Where(i => i.AppointmentFk == app.Id).ToList();
            foreach (ServiceAppointment item in items)
                item.ServiceFkNavigation = _crud.Service.GetItem(item.ServiceFk);
            return items;
        }
        public List<CartItem> GetCart(string email, out CartTotal cartTotal)
        {
            var user = _crud.UserStore.GetUser(email);
            cartTotal = new CartTotal();
            if (user != null)
            {
                var cart = GetCart(user.Id);
                TimeSpan totalDuration = new TimeSpan(0);
                int totalPrice = 0;
                foreach (ServiceAppointment s in cart)
                {
                    totalDuration += s.ServiceFkNavigation.Duration;
                    totalPrice += (int)s.ServiceFkNavigation.Price;
                }
                cartTotal.Duration = totalDuration.ToString(@"hh\:mm");
                cartTotal.Price = totalPrice;
                return cart.Select(i => new CartItem
                {
                    ItemId = i.Id,
                    Id = i.ServiceFkNavigation.Id,
                    Name = i.ServiceFkNavigation.Name,
                    Price = (int)i.ServiceFkNavigation.Price,
                    Duration = i.ServiceFkNavigation.Duration.ToString(@"hh\:mm"),
                    CategoryFk = i.ServiceFkNavigation.CategoryFk,
                }).ToList();
            }
            return null;
        }
        public List<CartItem> GetCartById(List<int> serviceIds, out CartTotal cartTotal)
        {
            cartTotal = new CartTotal();

            List<Service> cart = new List<Service>();
            foreach (int id in serviceIds)
                cart.Add(_crud.Service.GetItem(id));
            TimeSpan totalDuration = new TimeSpan(0);
                int totalPrice = 0;
                foreach (Service s in cart)
                {
                    totalDuration += s.Duration;
                    totalPrice += (int)s.Price;
                }
                cartTotal.Duration = totalDuration.ToString(@"hh\:mm");
                cartTotal.Price = totalPrice;
                return cart.Select(i => new CartItem
                {
                    Id = i.Id,
                    Name = i.Name,
                    Price = (int)i.Price,
                    Duration = i.Duration.ToString(@"hh\:mm"),
                    CategoryFk = i.CategoryFk,
                }).ToList();
       
        }
        List<int> GetSpecialistsByService(int serviceId)
        {
            return _crud.ServiceSpecialist.GetAll().Where(i => i.ServiceFk == serviceId).Select(i => i.SpecialistFk).ToList();
        }
        List<int> GetCommonSpecialists(List<Service> services)
        {
            List<int> result = new List<int>();
            if (services.Count > 0)
            {
                result = GetSpecialistsByService(services[0].Id);
                for (int i = 1; i < services.Count; i++)
                {
                    var specialists = GetSpecialistsByService(services[i].Id);
                    result = result.Intersect(specialists).ToList();
                }
            }
            return result;
        }
        List<int> GetCommonSpecialists(List<int> serviceIds)
        {
            List<int> result = new List<int>();
            if (serviceIds.Count > 0)
            {
                result = GetSpecialistsByService(serviceIds[0]);
                for (int i = 1; i < serviceIds.Count; i++)
                {
                    var specialists = GetSpecialistsByService(serviceIds[i]);
                    result = result.Intersect(specialists).ToList();
                }
            }
            return result;
        }

        public List<SpecialistDTO> GetSpecialistsForServices(List<int> serviceIds)
        {
            if (serviceIds == null)
                return null;
            var idList = GetCommonSpecialists(serviceIds);
            List<Specialist> result = new List<Specialist>();
            foreach (int id in idList)
            {
                Specialist person = _crud.Specialist.GetItem(id); //исключение???
                if (person != null)
                    result.Add(person);
            }
            return result.Select(i => new SpecialistDTO
            {
                Id = i.Id,
                Surname = i.Surname,
                Name = i.Name,
                FatherName = i.FatherName,
                Phone = i.Phone,
                Image = i.Image,
                Rating = i.Rating,
                RatingCount = i.RatingCount,
            }).ToList();
        }
        public List<List<int>> GetServiceGroups(List<int> serviceIds)
        {
            var result = new List<List<int>>();
            if (serviceIds == null)
                return result;
            foreach (int id in serviceIds)
            {
                bool added = false;
                for (int group = 0; group < result.Count; group++)
                {
                    var newGroup = result[group].ToList();
                    newGroup.Add(id);
                    var commonSpecialists = GetCommonSpecialists(newGroup);
                    if (commonSpecialists.Count > 0)
                    {
                        result[group].Add(id);
                        added = true;
                        break;
                    }
                }
                if (!added)
                {
                    result.Add(new List<int>());
                    result[result.Count - 1].Add(id);
                }
            }
            return result;
        }
        public List<ServiceDTO> GetServicesForUser(int categoryId, string email)
        {
            List<Service> services = new List<Service>();

            services = _crud.Service.GetAll().Where(i => i.CategoryFk == categoryId && i.Deleted == false).ToList();

            var user = _crud.UserStore.GetUser(email);
            if (user == null)
                return services.Select(i => new ServiceDTO
                {
                    Id = i.Id,
                    Name = i.Name,
                    Price = (int)i.Price,
                    Duration = i.Duration.ToString(@"hh\:mm"),
                    CategoryFk = i.CategoryFk,
                    Status = 0,
                    Description = i.Description,
                }).ToList();
            var cart = GetCart(user.Id);
            var cartServices = cart.Select(i => i.ServiceFkNavigation).ToList();
            var commonSpecialists = GetCommonSpecialists(cartServices);
            List<ServiceDTO> result = new List<ServiceDTO>();
            foreach (Service service in services)
            {
                int status = 0;
                if (cartServices.Contains(service, new ServiceEqualityComparer()))
                    status = 1;

                result.Add(new ServiceDTO
                {
                    Id = service.Id,
                    Name = service.Name,
                    Price = (int)service.Price,
                    Duration = service.Duration.ToString(@"hh\:mm"),
                    CategoryFk = service.CategoryFk,
                    Status = status,
                    Description = service.Description,
                });
            }
            return result;
        }
        public bool AddToCart(int serviceId, string email)
        {
            var user = _crud.UserStore.GetUser(email);
            if (user != null)
            {
                Service service = _crud.Service.GetItem(serviceId); //кинуть исключение NotFound
                var cart = GetCart(user.Id);
                var services = cart.Select(i => i.ServiceFkNavigation).ToList();

                Appointment app = GetCartApp(user.Id);

                _crud.ServiceAppointment.Create(new ServiceAppointment { AppointmentFk = app.Id, ServiceFk = serviceId });
                _crud.Save();
                return true;

            }
            return false;
        }
        public bool RemoveFromCart(int id)
        {
            var cartItem = _crud.ServiceAppointment.GetItem(id);
            if (cartItem == null)
                return false;
            _crud.ServiceAppointment.Delete(id);
            _crud.Save();
            return true;
        }
        public DateTime? FindClosestDateForSpecialist(int specialistId, TimeSpan duration, DateTime startDate)
        {
            DateTime? result = null;
            var workDays = _crud.WorkDay.GetAll().Where(i => i.Date >= startDate && i.SpecialistFk == specialistId).OrderBy(i => i.Date).ToList();
            if (workDays != null)
                foreach (WorkDay workDay in workDays)
                {
                    List<TimeSlot> timeSLots = _crud.TimeSlot.GetAll().
                        Where(i => i.WorkDayFk == workDay.Id && i.AppointmentFk == null).
                        OrderBy(i => i.Beginning).ToList();
                    timeSLots = SelectTime(timeSLots, duration);
                    if (timeSLots.Count > 0)
                    {
                        result = workDay.Date;
                        break;
                    }
                }
            return result;
        }
        public List<TimeSlotDTO> GetTimeSlotsForServices(List<int> serviceIds, int specialistId, DateTime date, out string closestDate)
        {
            closestDate = null;
            TimeSpan duration = new TimeSpan();

            foreach (int id in serviceIds)
            {
                var service = _crud.Service.GetItem(id);
                duration += service.Duration;
            }

            WorkDay workDay = _crud.WorkDay.GetAll().Where(i => i.Date.Date == date.Date && i.SpecialistFk == specialistId).FirstOrDefault();
            if (workDay != null)
            {
                List<TimeSlot> result = null;
                if (workDay.Date < DateTime.Today)
                    return null;
                if (workDay.Date == DateTime.Today)
                    result = _crud.TimeSlot.GetAll().
                    Where(i => i.WorkDayFk == workDay.Id && i.AppointmentFk == null && i.Beginning >= DateTime.Now.TimeOfDay).
                    OrderBy(i => i.Beginning).ToList();
                if (workDay.Date > DateTime.Today)
                    result = _crud.TimeSlot.GetAll().
                        Where(i => i.WorkDayFk == workDay.Id && i.AppointmentFk == null).
                        OrderBy(i => i.Beginning).ToList();
                
                result = SelectTime(result, duration);
                if (result.Count > 0)
                    return result.Select(i => new TimeSlotDTO
                    {
                        Id = i.Id,
                        Beginning = i.Beginning.ToString(@"hh\:mm"),
                        Duration = i.Duration.ToString(@"hh\:mm"),
                        WorkDayFk = i.WorkDayFk,
                    }).ToList();
            }
            var nextDate = FindClosestDateForSpecialist(specialistId, duration, DateTime.Today.AddDays(1));
            if (nextDate == null)
                closestDate = null;
            else closestDate = nextDate.Value.ToString("yyyy-MM-dd");
            if (workDay != null)
                return new List<TimeSlotDTO>();
            return null;
        }
        bool CheckTimeSlot(List<TimeSlot> timeSlots, TimeSpan duration, int i)
        {
            TimeSpan time = timeSlots[i].Duration;
            for (int j = i + 1; j < timeSlots.Count() && time <= duration; j++)
            {
                if (timeSlots[j].Beginning == timeSlots[j - 1].Beginning + timeSlots[j - 1].Duration)
                    time += timeSlots[j].Duration;
                else break;
            }
            return time >= duration;
        }
        List<TimeSlot> SelectTime(List<TimeSlot> timeSlots, TimeSpan duration)
        {
            List<TimeSlot> result = new List<TimeSlot>();
            for (int i = 0; i < timeSlots.Count(); i++)
                if (CheckTimeSlot(timeSlots, duration, i))
                    result.Add(timeSlots.ElementAt(i));
            return result;
        }
        void ClearCart(int userId, List<int> serviceIds)
        {
            var cart = GetCart(userId);
            foreach (var item in cart)
                if (serviceIds.Contains(item.ServiceFk))
                    _crud.ServiceAppointment.Delete(item.Id);
            _crud.Save();
        }
        public bool MakeAppointment(string email, List<int> serviceIds, int timeSlotId)
        {
            var user = _crud.UserStore.GetUser(email);
            if (user == null || serviceIds == null || serviceIds.Count == 0)
                return false;
            TimeSlot timeSlot = _crud.TimeSlot.GetItem(timeSlotId);
            if (timeSlot == null)
                return false;
            int specialistId = _crud.WorkDay.GetItem(timeSlot.WorkDayFk).SpecialistFk;

            var specialists = GetCommonSpecialists(serviceIds).ToList();
            if (!specialists.Contains(specialistId))
                return false;

            decimal totalPrice = 0;
            TimeSpan duration = new TimeSpan();
            foreach (int id in serviceIds)
            {
                var service = _crud.Service.GetItem(id);
                totalPrice += service.Price;
                duration += service.Duration;
            }

            List<TimeSlot> timeSlots = _crud.TimeSlot.GetAll().
                Where(i => i.WorkDayFk == timeSlot.WorkDayFk && i.Beginning >= timeSlot.Beginning && i.AppointmentFk == null).
                OrderBy(i => i.Beginning).
                ToList();

            if (CheckTimeSlot(timeSlots, duration, 0))
            {
                var app = new Appointment();
                app.TimeSlotFk = timeSlotId;
                app.Duration = duration;
                app.Price = totalPrice;
                app.ClientFk = user.Id;
                app.StatusFk = 2;
                _crud.Appointment.Create(app);
                _crud.Save();
                foreach (int id in serviceIds)
                    _crud.ServiceAppointment.Create(new ServiceAppointment
                    {
                        AppointmentFk = app.Id,
                        ServiceFk = id,
                    });

                TimeSpan time = new TimeSpan();
                for (int i = 0; time < duration; i++)
                {
                    time += timeSlots[i].Duration;
                    timeSlots[i].AppointmentFk = app.Id;
                    _crud.TimeSlot.Update(timeSlots[0]);
                }
                _crud.Save();
                ClearCart(user.Id, serviceIds);
                return true;
            }
            return false;
        }
        public int CancelAppointment(string email, int id)
        {
            var user = _crud.UserStore.GetUser(email);
            if (user == null)
                return 3;
            var app = _crud.Appointment.GetItem(id);
            if (app == null)
                return 1;

            var roles = _crud.UserStore.GetUserRole(user);
            bool isAdmin = roles.Contains("admin");
            if (app.ClientFk != user.Id && !isAdmin)
                return 3;
            if (app.StatusFk != 2) //выполнена или оформляется
                return 2;
            _crud.Appointment.Delete(id);
            var timeSlots = _crud.TimeSlot.GetAll().Where(i => i.AppointmentFk == id).ToList();
            foreach (TimeSlot timeSlot in timeSlots)
            {
                timeSlot.AppointmentFk = null;
                _crud.TimeSlot.Update(timeSlot);
            }
            _crud.Save();
            return 0;
        }
        public List<AppointmentDTO> GetAppointments(string email)
        {
            var user = _crud.UserStore.GetUser(email);
            if (user == null)
                return null;
            var appointmnets = _crud.Appointment.GetAll().Where(i => i.ClientFk == user.Id && i.StatusFk > 1).Select(i => new AppointmentDTO
            {
                Id = i.Id,
                Duration = i.Duration.ToString(@"hh\:mm"),
                Price = (int)i.Price,
                TimeSlotFk = i.TimeSlotFk,
                Rating = i.Rating,
                Review = i.Review,
            }).ToList();
            foreach (AppointmentDTO app in appointmnets)
            {
                if (app.TimeSlotFk != null)
                {
                    TimeSlot timeslot = _crud.TimeSlot.GetItem(app.TimeSlotFk.Value);
                    app.Beginning = timeslot.Beginning.ToString(@"hh\:mm");
                    var workDay = _crud.WorkDay.GetItem(timeslot.WorkDayFk);
                    app.Date = workDay.Date.ToString("yyyy-MM-dd");
                    var specialist = _crud.Specialist.GetItem(workDay.SpecialistFk);
                    app.Specialist = new SpecialistDTO
                    {
                        Id = specialist.Id,
                        Surname = specialist.Surname,
                        Name = specialist.Name,
                        FatherName = specialist.FatherName,
                        Image = specialist.Image
                    };

                    if (workDay.Date > DateTime.Today || (workDay.Date == DateTime.Today &&
                        timeslot.Beginning + TimeSpan.Parse(app.Duration) > DateTime.Now.TimeOfDay))
                        app.Completed = false;
                    else app.Completed = true;
                }
                app.Services = new List<string>();
                var items = _crud.ServiceAppointment.GetAll().Where(i => i.AppointmentFk == app.Id).ToList();
                foreach (ServiceAppointment item in items)
                    app.Services.Add(_crud.Service.GetItem(item.ServiceFk).Name);
            }
            return appointmnets;
        }
        public List<AppointmentDTO> GetIncompleteAppointments(string email)
        {
            return GetAppointments(email).Where(i => !i.Completed).ToList();
        }
        public List<AppointmentDTO> GetCompleteAppointments(string email)
        {
            return GetAppointments(email).Where(i => i.Completed).ToList();
        }
        class ServiceEqualityComparer : IEqualityComparer<Service>
        {
            public bool Equals(Service s1, Service s2)
            {
                if (s1 != null && s2 != null)
                    return s1.Id == s2.Id;
                else return false;
            }

            public int GetHashCode(Service service)
            {
                return service.GetHashCode();
            }
        }
    }
}
