﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;
using WebApplication.DAL.Interfaces;

namespace WebApplication.BLL.Services
{
    public class RaitingService: IRaitingService
    {
        private IDbRepository _crud;
        public RaitingService(IDbRepository crud)
        {
            _crud = crud;
        }
        public int RateAppoitnment(int id, string email, int rating, string review)
        {
            var user = _crud.UserStore.GetUser(email);
            if (user == null)
                return 1;
            var app = _crud.Appointment.GetItem(id);
            if (app == null || app.ClientFk != user.Id)
                return 2;
            if (rating < 0 || rating > 5)
                return 4;
            if (app.StatusFk == 1)
                return 3;
           
            var timeSlot = _crud.TimeSlot.GetItem(app.TimeSlotFk.Value);
            var workDay = _crud.WorkDay.GetItem(timeSlot.WorkDayFk);

            if (workDay.Date > DateTime.Today ||
                (workDay.Date == DateTime.Today &&
                timeSlot.Beginning + app.Duration > DateTime.Now.TimeOfDay))
                return 3;

            if (!CanRate(workDay.Date))
                return 5;
            app.Rating = rating;
            app.Review = review;
            app.ReviewTime = DateTime.Now;
            _crud.Appointment.Update(app);
            _crud.Save();
            ReacountRating(workDay.SpecialistFk);
            return 0;
        }

        public void ReacountRating(int specialistId)
        {
            var specialist = _crud.Specialist.GetItem(specialistId);
            var appQuery = _crud.Appointment.GetAll().
                Where(i => i.Rating != null).
                Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
                (i, j) => new { Id = i.Id, Rating = i.Rating, WorkDayFk = j.WorkDayFk }).
                Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
                (i, j) => new { Id = i.Id, Rating = i.Rating, SpecialistFk = j.SpecialistFk }).
                Where(i => i.SpecialistFk == specialistId).
                ToList();

            int ratingSum = appQuery.Sum(i => i.Rating).Value;
            int count = appQuery.Count();
            if (count != 0)
                specialist.Rating = (decimal)ratingSum / (decimal)count;
            else specialist.Rating = 0;
            specialist.RatingCount = count;
            _crud.Specialist.Update(specialist);
            _crud.Save();
        }

        public List<ReviewDTO> GetReviews(int specialistId)
        {
            var specialist = _crud.Specialist.GetItem(specialistId);
            if (specialist == null)
                return null;
            var appQuery = _crud.Appointment.GetAll().
                Where(i => i.Rating != null).
                Join(_crud.TimeSlot.GetAll(), i => i.TimeSlotFk, j => j.Id,
                (i, j) => new
                {
                    Id = i.Id,
                    Rating = i.Rating,
                    Review = i.Review,
                    ReviewTime = i.ReviewTime,
                    ClientFk = i.ClientFk,
                    WorkDayFk = j.WorkDayFk
                }).
                Join(_crud.WorkDay.GetAll(), i => i.WorkDayFk, j => j.Id,
                (i, j) => new
                {
                    Id = i.Id,
                    Rating = i.Rating,
                    Review = i.Review,
                    ReviewTime = i.ReviewTime,
                    ClientFk = i.ClientFk,
                    SpecialistFk = j.SpecialistFk
                }).
                Where(i => i.SpecialistFk == specialistId).
                ToList();
            if (appQuery == null)
                return null;

            var result = new List<ReviewDTO>();
            for (int i = 0; i < appQuery.Count; i++)
            {
                result.Add(new ReviewDTO());
                var client = _crud.UserStore.GetUser(appQuery[i].ClientFk);
                result[i].ClientName = client.Surname + " " + client.Name;
                result[i].Rating = appQuery[i].Rating.Value;
                result[i].Review = appQuery[i].Review;
                result[i].ReviewTime = appQuery[i].ReviewTime.Value.ToString("s");
            }
            return result;
        }

        public bool CanRate(DateTime date)
        {
            return date.AddDays(14) >= DateTime.Today;
        }
    }
}
