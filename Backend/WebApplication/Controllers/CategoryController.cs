﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ILogger<ServiceController> _logger;
        private readonly IDBDataService _crud;
        private readonly IReportService _reportService;

        private readonly string imagesFolder = "./wwwroot/images/categories/";

        public CategoryController(IDBDataService crud, IReportService reportService, ILogger<ServiceController> logger)
        {
            _crud = crud;
            _reportService = reportService;
            _logger = logger;
        }

        private string getImage (CategoryDTO category)
        {
            string img = $"category-{category.Id}.svg";
            if (System.IO.File.Exists($"{imagesFolder}{img}"))
                return img;
            return "";
        }

        [HttpGet]
        [Route("api/category")]
        public IEnumerable<CategoryDTO> Get()
        {
            var result = _crud.GetAllCategories();
            foreach (CategoryDTO category in result)
            category.Image = getImage(category);
            return result;
        }

        [HttpGet("api/category/{id}")]
        public async Task<IActionResult> GetItem([FromRoute] int id)
        {
            var u = _crud.GetCategory(id);
            if (u == null)
                return NotFound();
            u.Image = getImage(u);
            return Ok(u);
        }

        [HttpPost("api/category")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromForm] CategoryDTO category, [FromForm] IFormFile file)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    _crud.CreateCategory(category);
                    return Ok(category);
                }
                if (file.Length > 0)
                {
                    var ext = Path.GetExtension(file.FileName).ToLowerInvariant();

                    if (string.IsNullOrEmpty(ext) || ext != ".svg")
                        return BadRequest(new { errors = new string[] { "Only .svg files are allowed" } });

                    _crud.CreateCategory(category);
                    using (var stream = new FileStream($"./wwwroot/images/categories/category-{category.Id}.svg", FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        return Ok(category);
                    }
                }
                return BadRequest(new { errors = new string[] { "Empty file" } });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpPut("api/category/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update([FromForm] CategoryDTO category, [FromForm] IFormFile file, [FromForm] bool imageDelete, [FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                string imagePath = $"./wwwroot/images/categories/category-{id}.svg";
                if (file == null)
                {
                    if (imageDelete && System.IO.File.Exists(imagePath))
                        System.IO.File.Delete(imagePath);
                    category.Id = id;
                    if (_crud.UpdateCategory(category))
                        return NoContent();
                    else return NotFound();
                }
                if (file.Length > 0)
                {
                    var ext = Path.GetExtension(file.FileName).ToLowerInvariant();

                    if (string.IsNullOrEmpty(ext) || ext != ".svg")
                    {
                        return BadRequest(new { errors = new string[] { "Only .svg files are allowed" } });
                    }
                    category.Id = id;
                    if (!_crud.UpdateCategory(category))
                        return NotFound();
                    using (var stream = new FileStream(imagePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        return NoContent();
                    }
                }
                return BadRequest(new { errors = new string[] { "Empty file" } });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpDelete("api/category/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (_crud.DeleteCategory(id))
                    return NoContent();
                return NotFound();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class PeriodModel
        {
            [Required] public string Start { get; set; }
            [Required] public string End { get; set; }

        };

        [HttpPost]
        [Route("api/category/count")]
        public async Task<IActionResult> GetCategoryCount([FromBody] PeriodModel model)
        {
          /*  string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();*/
            if (ModelState.IsValid)
            {
                DateTime start, end;
                if (!DateTime.TryParse(model.Start, out start))
                    return BadRequest(new { errors = new string[] { "Start date has wrong format" } });
                if (!DateTime.TryParse(model.End, out end))
                    return BadRequest(new { errors = new string[] { "End date has wrong format" } });
                if (start > end)
                    return BadRequest(new { errors = new string[] { "Start can't be later than end" } });
          
                return Ok(_reportService.GetCategoryCount(start, end));
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }
    }
}
