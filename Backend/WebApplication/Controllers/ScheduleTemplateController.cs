﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class ScheduleTemplateController : Controller
    {
        private readonly ILogger<ServiceController> _logger;
        private readonly IScheduleService _scheduleService;
        private readonly IDBDataService _crud;

        public ScheduleTemplateController(IScheduleService scheduleService, IDBDataService crud, ILogger<ServiceController> logger)
        {
            _scheduleService = scheduleService;
            _crud = crud;
            _logger = logger;
        }

        [HttpGet]
        [Route("api/scheduleTemplate")]
        public IEnumerable<ScheduleTemplateDTO> Get()
        {
            return _scheduleService.GetScheduleTemplates();
        }

        class GetScheduleTemplateResult
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public  List<List<TemplateTimeSlotDTO>> TimeSlots { get; set; }
        }

        [HttpGet("api/scheduleTemplate/{id}")]
        public async Task<IActionResult> GetItem([FromRoute] int id)
        {
            var template = _scheduleService.GetScheduleTemplate(id);
            if (template == null)
                return NotFound();
            var timeSlots = _scheduleService.GetTemplateTimeSlots(id);
            var result = new GetScheduleTemplateResult
            {
                Id = template.Id,
                Name = template.Name,
                TimeSlots = timeSlots,
            };
            return Ok(result);
        }

        public class TimeSlotModel
        {
            [Required] public string Beginning { get; set; }
            [Required] public string Duration { get; set; }
        }

        public class CreateScheduleTemplateModel
        {
            [Required]
            public string Name { get; set; }
            public List<List<TimeSlotModel>> TimeSlots { get; set; }
        }

        [HttpPost("api/scheduleTemplate")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] CreateScheduleTemplateModel model)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (model.TimeSlots != null)
                {
                    foreach (List<TimeSlotModel> item in model.TimeSlots)
                    {
                        foreach (TimeSlotModel timeSlot in item)
                        {
                            TimeSpan time;
                            if (!TimeSpan.TryParse(timeSlot.Beginning, out time))
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} has wrong format" } });
                            if (time.Hours > 24)
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} is not valid" } });

                            if (!TimeSpan.TryParse(timeSlot.Duration, out time))
                                return BadRequest(new { errors = new string[] { $"Duration {timeSlot.Duration} has wrong format" } });
                        }

                        var timeSlots = item.Select(i => new TemplateTimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();

                        if (_scheduleService.HasIntersections(timeSlots))
                            return BadRequest(new { errors = new string[] { $"TimeSlots should not intersect" } });
                    }
                }

                ScheduleTemplateDTO template = new ScheduleTemplateDTO();
                template.Name = model.Name;
                _scheduleService.CreateScheduleTemplate(template);
                if (model.TimeSlots != null)
                {
                    var timeToCreate = new List<List<TemplateTimeSlotDTO>>();
                    foreach (List<TimeSlotModel> item in model.TimeSlots)
                    {
                        var timeSlots = item.Select(i => new TemplateTimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();
                        timeToCreate.Add(timeSlots);
                    }
                    _scheduleService.CreateTemplateTimeSlots(timeToCreate, template.Id);
                }
                return Ok(new { Id = template.Id });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }
        public class UpdateScheduleTemplateModel
        {
            [Required]
            public string Name { get; set; }
            public List<List<TimeSlotModel>> TimeToCreate { get; set; }
            public List<int> TimeToDelete { get; set; }
        }

        [HttpPut("api/scheduleTemplate/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update([FromBody] UpdateScheduleTemplateModel model, [FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                var template = _scheduleService.GetScheduleTemplate(id);
                if (template == null)
                    return NotFound();
                template.Name = model.Name;
                if (model.TimeToCreate != null)
                {
                    foreach (List<TimeSlotModel> item in model.TimeToCreate)
                    {
                        foreach (TimeSlotModel timeSlot in item)
                        {
                            TimeSpan time;
                            if (!TimeSpan.TryParse(timeSlot.Beginning, out time))
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} has wrong format" } });
                            if (time.Hours > 24)
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} is not valid" } });

                            if (!TimeSpan.TryParse(timeSlot.Duration, out time))
                                return BadRequest(new { errors = new string[] { $"Duration {timeSlot.Duration} has wrong format" } });
                        }

                        var timeSlots = item.Select(i => new TemplateTimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();

                        if (_scheduleService.HasIntersections(timeSlots))
                            return BadRequest(new { errors = new string[] { $"TimeSlots should not intersect" } });

                    }
                }
                if (model.TimeToDelete != null)
                {
                    foreach (int timeSlotId in model.TimeToDelete)
                    {
                        if (_scheduleService.GetTemplateTimeSlot(timeSlotId) == null)
                            return NotFound(new { errors = new string[] { $"TimeSlot with id={timeSlotId} is not found" } });
                    }
                }
                _scheduleService.UpdateScheduleTemplate(template);
                _scheduleService.DeleteTemplateTimeSlots(model.TimeToDelete);
                if (model.TimeToCreate != null)
                {
                    var timeToCreate = new List<List<TemplateTimeSlotDTO>>();
                    foreach (List<TimeSlotModel> item in model.TimeToCreate)
                    {
                        var timeSlots = item.Select(i => new TemplateTimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();
                        timeToCreate.Add(timeSlots);
                    }
                    _scheduleService.CreateTemplateTimeSlots(timeToCreate, id);
                }
                return NoContent();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpDelete("api/scheduleTemplate/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (_scheduleService.DeleteScheduleTemplate(id))
                    return NoContent();
                return NotFound();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

    }
}
