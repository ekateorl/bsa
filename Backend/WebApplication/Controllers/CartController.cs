﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class CartController : Controller
    {
        private IMakeAppointmentService _makeAppService;
        public CartController(IMakeAppointmentService makeAppService)
        {
            _makeAppService = makeAppService;
        }

        [HttpGet]
        [Route("api/cart")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetCart()
        {
            CartTotal cartTotal;
            var cart = _makeAppService.GetCart(HttpContext.User.Claims.ToList()[0].Value, out cartTotal);

            return Ok(new { items= cart, total = cartTotal});
        }

        public class GetCartModel
        {
            [Required]
            public List<int> Services { get; set; }

        }

        [HttpPost]
        [Route("api/cart/byId")]
        public async Task<IActionResult> GetCartById([FromBody] GetCartModel model)
        {
            if (ModelState.IsValid)
            {
                CartTotal cartTotal;
                var cart = _makeAppService.GetCartById(model.Services, out cartTotal);
                return Ok(new { items = cart, total = cartTotal });
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class AddToCartModel
        {
            [Required]
            public int ServiceId { get; set; }
        }
        [HttpPost]
        [Route("api/cart")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AddToCart([FromBody] AddToCartModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _makeAppService.AddToCart(model.ServiceId, HttpContext.User.Claims.ToList()[0].Value);
                if (result)
                    return NoContent();
                return BadRequest(new { errors = new string[] { "Unable to add this service to the cart" } });
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpDelete]
        [Route("api/cart/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteFromCart(int id)
        {
            var result = _makeAppService.RemoveFromCart(id);
            if (result)
                return NoContent();
            return NotFound();
        }

    }
}
