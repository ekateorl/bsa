﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class TimeSlotController : Controller
    {
        private readonly ILogger<ServiceController> _logger;
        private readonly IScheduleService _scheduleService;
        private readonly IDBDataService _crud;

        public TimeSlotController(IScheduleService scheduleService, IDBDataService crud, ILogger<ServiceController> logger)
        {
            _scheduleService = scheduleService;
            _crud = crud;
            _logger = logger;
        }

       public class GetTimeSlotsModel
        {
            [Required] public string[] Dates { get; set; }
            [Required] public int? SpecialistId { get; set; }
        }

        class GetTimeSlotsResult
        {
            public string Date { get; set; }
            public List<TimeSlotDTO> TimeSlots { get; set; }
        }

        [HttpPost]
        [Route("api/timeSlot/bySpecialist")]
        public async Task<IActionResult> Get([FromBody] GetTimeSlotsModel model)
        {
            if (ModelState.IsValid)
            {
                var specialist = _crud.GetSpecialist(model.SpecialistId.Value);
                if (specialist == null)
                    return NotFound();
                var result = new List<GetTimeSlotsResult>();
                foreach (string dateStr in model.Dates)
                {
                    DateTime date;
                    if (!DateTime.TryParse(dateStr, out date))
                        return BadRequest(new { errors = new string[] { $"Date {dateStr} has wrong format" } });
                    var timeSlots = _scheduleService.GetTimeSlots(model.SpecialistId.Value, date);
                    var resultItem = new GetTimeSlotsResult();
                    resultItem.Date = dateStr;
                    if (timeSlots == null)
                        resultItem.TimeSlots = null;
                    else
                        resultItem.TimeSlots = timeSlots.OrderBy(i => i.Beginning).ToList();
                    result.Add(resultItem);
                }
                return Ok(result);
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class TimeSlotModel
        {
            [Required] public string Beginning { get; set; }
            [Required] public string Duration { get; set; }
        }

        public class DateModel
        {
            [Required] public string Date { get; set; }
            [Required] public TimeSlotModel[] TimeSlots { get; set; }
        }

        public class UpdateTimeSlotsModel
        {
            public List<DateModel> TimeToCreate { get; set; }
            public List<int> TimeToDelete { get; set; }
            [Required] public int? SpecialistId { get; set; }
        }
        [HttpPut("api/timeSlot")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update([FromBody] UpdateTimeSlotsModel model)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                var specialist = _crud.GetSpecialist(model.SpecialistId.Value);
                if (specialist == null)
                    return NotFound();
                if (model.TimeToCreate != null)
                    foreach (DateModel item in model.TimeToCreate)
                    {
                        DateTime date;
                        if (!DateTime.TryParse(item.Date, out date))
                            return BadRequest(new { errors = new string[] { $"Date {item.Date} has wrong format" } });

                        foreach (TimeSlotModel timeSlot in item.TimeSlots)
                        {
                            TimeSpan time;
                            if (!TimeSpan.TryParse(timeSlot.Beginning, out time))
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} on {item.Date} has wrong format" } });
                            if (time.Hours > 24)
                                return BadRequest(new { errors = new string[] { $"Beginning {timeSlot.Beginning} is not valid" } });

                            if (!TimeSpan.TryParse(timeSlot.Duration, out time))
                                return BadRequest(new { errors = new string[] { $"Duration {timeSlot.Duration} on {item.Date} has wrong format" } });
                        }

                        var timeSlots = item.TimeSlots.Select(i => new TimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();

                        if (_scheduleService.HasIntersections(timeSlots))
                            return BadRequest(new { errors = new string[] { $"TimeSlots on {item.Date} should not intersect" } });

                    }
                if (model.TimeToDelete != null)
                {
                    foreach (int id in model.TimeToDelete)
                    {
                        if (_scheduleService.GetTimeSlot(id) == null)
                            return NotFound(new { errors = new string[] { $"TimeSlot with id={id} is not found" } });
                    }
                    _scheduleService.DeleteTimeSlots(model.TimeToDelete);
                }
                if (model.TimeToCreate != null)
                    foreach (DateModel item in model.TimeToCreate)
                    {
                        DateTime date = DateTime.Parse(item.Date);
                        var timeSlots = item.TimeSlots.Select(i => new TimeSlotDTO
                        {
                            Beginning = i.Beginning,
                            Duration = i.Duration,
                        }).ToList();

                        timeSlots = _scheduleService.CreateTimeSlots(model.SpecialistId.Value, date, timeSlots);

                    }
                return NoContent();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class DeleteTimeSlotsModel
        {
            [Required] public List<int> TimeSlotIds { get; set; }
        }

        [HttpDelete("api/timeSlot")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromBody] DeleteTimeSlotsModel model)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                foreach (int id in model.TimeSlotIds)
                {
                    if (_scheduleService.GetTimeSlot(id) == null)
                        return NotFound(new { errors = new string[] { $"TimeSlot with id={id} is not found" } });
                }
                _scheduleService.DeleteTimeSlots(model.TimeSlotIds);
                return NoContent();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }
    }
}
