﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class AppointmentController : Controller
    {
        private IMakeAppointmentService _makeAppService;
        private IRaitingService _raitingService;
        private IScheduleService _scheduleService;
        private IEmailService _emailService;


        public AppointmentController(IMakeAppointmentService makeAppService, IRaitingService raitingService,
            IScheduleService scheduleService, IEmailService emailService)
        {
            _makeAppService = makeAppService;
            _raitingService = raitingService;
            _scheduleService = scheduleService;
            _emailService = emailService;
        }

        [HttpGet]
        [Route("api/appointment/incomplete")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IEnumerable<AppointmentDTO> GetIncomplete()
        {
            string email = HttpContext.User.Claims.ToList()[0].Value;
            return _makeAppService.GetIncompleteAppointments(email).OrderBy(i=>i.Date);
        }

        [HttpGet]
        [Route("api/appointment/complete")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IEnumerable<AppointmentDTO> GetComplete()
        {
            string email = HttpContext.User.Claims.ToList()[0].Value;
            var appoitments = _makeAppService.GetCompleteAppointments(email);
            foreach (AppointmentDTO app in appoitments)
                app.CanRate = _raitingService.CanRate(DateTime.Parse(app.Date));
            return appoitments.OrderByDescending(i=>i.Date);
        }

        [HttpGet]
        [Route("api/appointment/{id}")]
        public async Task<IActionResult> GetItem([FromRoute] int id)
        {
            var result = _scheduleService.GetAppointment(id);
            if (result == null)
                return NotFound();
            return Ok(result);
        }

        public class GetAppointmentModel
        {
            [Required]
            public string Date { get; set; }
        }

        [HttpPost]
        [Route("api/appointment/forDate")]
        public async Task<IActionResult> GetForDate([FromBody] GetAppointmentModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime date;
                if (!DateTime.TryParse(model.Date, out date))
                    return BadRequest(new { errors = new string[] { $"Date {model.Date} has wrong format" } });
                return Ok(_scheduleService.GetAppointments(date));
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

     

        public class AppointmentModel
        {
            [Required] public int TimeSlotId { get; set; }
            [Required] public List<int> Services { get; set; }
        }
        public class MakeAppointmentsModel
        {
            [Required] public List<AppointmentModel> Appointments { get; set; }

        }
        [HttpPost]
        [Route("api/appointment")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] MakeAppointmentsModel model)
        {
            if (ModelState.IsValid)
            {
                string email = HttpContext.User.Claims.ToList()[0].Value;
                foreach (var appointment in model.Appointments)
                    _makeAppService.MakeAppointment(email, appointment.Services, appointment.TimeSlotId);
                return NoContent();
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class RateAppointmentModel
        {
            [Required]
            public int? Rating { get; set; }
            public string Review { get; set; }
        }

        [HttpPost]
        [Route("api/appointment/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Rate([FromBody] RateAppointmentModel model, [FromRoute] int id)
        {
            if (ModelState.IsValid)
            {
                string email = HttpContext.User.Claims.ToList()[0].Value;
                int result = _raitingService.RateAppoitnment(id, email, model.Rating.Value, model.Review);
                switch (result)
                {
                    case 1: return Unauthorized();
                    case 2: return NotFound();
                    case 3: return BadRequest(new { errors = new string[] { "Unable to rate incompleted appointment" } });
                    case 4: return BadRequest(new { errors = new string[] { "Rating must more than or equal to 0 and less than or equal to 5" } });
                    case 5: return BadRequest(new { errors = new string[] { "Unable to rate appointment completed more than 2 weeks ago" } });

                    default: return NoContent();
                }
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class CancelAppointmentModel
        {
          
            public string Reason { get; set; }
        }

        [HttpPost]
        [Route("api/appointment/cancel/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] int id, [FromBody] CancelAppointmentModel model)
        {
            if (ModelState.IsValid)
            {
                string email = HttpContext.User.Claims.ToList()[0].Value;
                string role = HttpContext.User.Claims.ToList()[1].Value;

                var appointment = _scheduleService.GetAppointment(id);
                int result = _makeAppService.CancelAppointment(email, id);
                switch (result)
                {
                    case 1: return NotFound();
                    case 2: return BadRequest(new { errors = new string[] { "Unable to cancel completed appointment" } });
                    case 3: return Unauthorized();
                    default:
                        {
                            //if (role == "admin")
                            //    SendCancelNotification(appointment, model.Reason);
                            return NoContent();
                        }
                }
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        async void SendCancelNotification(AppointmentDTO appointment, string reason)
        {
            var client = appointment.Client;
            var dateString = DateTime.Parse(appointment.Date).ToString("D");
            var specialistName = appointment.Specialist.Surname + " " + appointment.Specialist.Name;
            string message = $"Здравствуйте, {client.Name}! К сожалению, ваша запись на {dateString} в {appointment.Beginning} " +
                $"(специалист - {specialistName}) была отменена. Причина отмены: {reason}. Приносим свои извинения!";
            _emailService.SendEmailAsync(client.Email, "Отмена записи", message);
        }

    }
}
