﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class AccountController : Controller
    {
        private IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        [Route("api/account/register")]
        public async Task<IActionResult> Register([FromBody] ApplicationUserDTO model)
        {
            if (ModelState.IsValid)
            {
                var result = _accountService.Register(model);
                if (result.Succeeded)
                    return Ok(result);
                else return Unauthorized(new { errors = result.Errors });
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class LoginModel
        {
            [Required]
            public string Email { get; set; }

            [Required]
            public string Password { get; set; }
            public bool RememberMe { get; set; }
        }

        [HttpPost]
        [Route("api/account/login")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _accountService.SignIn(model.Email, model.Password, model.RememberMe);
                if (result.Succeeded)
                    return Ok(result);
                else return Unauthorized(new { errors=new string[] { "Authorization failed" } });
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });

        }


        [HttpPost]
        [Route("api/account/logoff")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
             _accountService.SignOut();
            return Ok();
        }

        [HttpGet]
        [Route("api/account/me")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Me()
        {
            var res = _accountService.GetUser(HttpContext.User);
            if (res == null)
                return Unauthorized();
            return Ok(res);
        }
    }
}
