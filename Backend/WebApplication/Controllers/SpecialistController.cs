﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.DTO;
using WebApplication.BLL.Interfaces;

namespace WebApplication.Controllers
{
    public class SpecialistController: Controller
    {
        private IMakeAppointmentService _makeAppService;
        private IRaitingService _raitingService;
        private IReportService _reportService;

        private readonly IDBDataService _crud;

        public SpecialistController(IDBDataService crud, IMakeAppointmentService makeAppService, IRaitingService raitingService, IReportService reportService)
        {
            _makeAppService = makeAppService;
            _raitingService = raitingService;
            _reportService = reportService;
            _crud = crud;
        }

        [HttpGet]
        [Route("api/specialist")]
        public IEnumerable<SpecialistDTO> Get()
        {
            var result = _crud.GetAllSpecialists();
            return result;
        }
        public class GetSpecialistsModel
        {
            [Required] public string Date { get; set; }
            [Required] public List<int> Services { get; set; }

        }
        public class SpecialistForUserResult
        {
            public SpecialistDTO Specialist { get; set; }
            public List<TimeSlotDTO> TimeSlots { get; set; }
            public string ClosestDate { get; set; }
            public int Status { get; set; }
        }

        [HttpPost]
        [Route("api/specialist/forUser")]
        public async Task<IActionResult> GetSpecialistsForUser([FromBody] GetSpecialistsModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime date;
                if (DateTime.TryParse(model.Date, out date))
                {
                    var specialists = _makeAppService.GetSpecialistsForServices(model.Services);
                    var result = new List<SpecialistForUserResult>();
                    foreach (SpecialistDTO s in specialists)
                    {
                        var resultItem = new SpecialistForUserResult();
                        resultItem.Specialist = s;
                        string closestDate;
                        resultItem.TimeSlots = _makeAppService.GetTimeSlotsForServices(model.Services, s.Id, date, out closestDate);
                        if (resultItem.TimeSlots == null)
                            resultItem.Status = 2;
                        else if (resultItem.TimeSlots.Count == 0)
                            resultItem.Status = 1;
                        else
                            resultItem.Status = 0;
                        resultItem.ClosestDate = closestDate;
                        result.Add(resultItem);
                    }
                    return Ok(result);
                }
                return BadRequest(new { errors = new string[] { "Date has wrong format" } });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpGet]
        [Route("api/specialist/{id}/reviews")]
        public async Task<IActionResult> GetReviews([FromRoute] int id)
        {
            if (_crud.GetSpecialist(id) == null)
                return NotFound();
            return Ok(_raitingService.GetReviews(id));
        }

        public class RatingDynamicsModel
        {
            [Required] public string Start { get; set; }
            [Required] public string End { get; set; }
            [Required] public List<int> Specialists { get; set; }
        };

        public class SpecialistRatingDynamicsResult
        {
            public int Id { get; set; }
            public List<RatingDynamicsResult> RatingDynamics { get; set; }
        }

        [HttpPost]
        [Route("api/specialist/ratingDynamics")]
        public async Task<IActionResult> GetRatingDynamics([FromBody] RatingDynamicsModel model)
        {
           /* string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();*/
            if (ModelState.IsValid)
            {
                DateTime start, end;
                if (!DateTime.TryParse(model.Start, out start))
                    return BadRequest(new { errors = new string[] { "Start date has wrong format" } });
                if (!DateTime.TryParse(model.End, out end))
                    return BadRequest(new { errors = new string[] { "End date has wrong format" } });
                if (start > end)
                    return BadRequest(new { errors = new string[] { "Start can't be later than end" } });
                foreach (int id in model.Specialists)
                if (_crud.GetSpecialist(id) == null)
                    return NotFound();
                var result = new List<SpecialistRatingDynamicsResult>();
                foreach (int id in model.Specialists)
                {
                    var ratingDynamics = _reportService.GetRatingDynamics(id, start, end);
                    result.Add(new SpecialistRatingDynamicsResult
                    {
                        Id = id,
                        RatingDynamics = ratingDynamics,
                    });
                }
                return Ok(result);
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class PerfomanceModel
        {
            [Required] public string Start { get; set; }
            [Required] public string End { get; set; }
        };

        [HttpPost]
        [Route("api/specialist/perfomance")]
        public async Task<IActionResult> GetPerfomance([FromBody] PerfomanceModel model)
        {
            /* string role = HttpContext.User.Claims.ToList()[1].Value;
             if (role != "admin")
                 return Unauthorized();*/
            if (ModelState.IsValid)
            {
                DateTime start, end;
                if (!DateTime.TryParse(model.Start, out start))
                    return BadRequest(new { errors = new string[] { "Start date has wrong format" } });
                if (!DateTime.TryParse(model.End, out end))
                    return BadRequest(new { errors = new string[] { "End date has wrong format" } });
                if (start > end)
                    return BadRequest(new { errors = new string[] { "Start can't be later than end" } });
               
                return Ok(_reportService.GetSpecialistsPerfomance(start, end));
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class CreateSpecialistModel
        {
            [Required] public string Name { get; set; }
            [Required] public string Surname { get; set; }
            public string FatherName { get; set; }
            [Required] public string Phone { get; set; }
            public string Image { get; set; }
            [Required] public string StartDate { get; set; }
            public List<int> Services { get; set; }

        };

        [HttpPost("api/specialist")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromForm] CreateSpecialistModel specialist, IFormFile file)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                DateTime date;
                if (!DateTime.TryParse(specialist.StartDate, out date))
                    return BadRequest(new { errors = new string[] { "Start date has wrong format" } });
                var newSpecialist = new SpecialistDTO
                {
                    Name = specialist.Name,
                    Surname = specialist.Surname,
                    FatherName = specialist.FatherName,
                    //Image = specialist.Image,
                    Phone = specialist.Phone,
                    StartDate = specialist.StartDate,
                };
                if (file == null)
                {
                    _crud.CreateSpecialist(newSpecialist, specialist.Services);
                    return Ok(newSpecialist);
                }
                if (file.Length > 0)
                {
                    var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                    if (string.IsNullOrEmpty(ext) || (ext != ".jpg" && ext != ".jpeg" && ext != ".png" && ext != ".gif"))
                        return BadRequest(new { errors = new string[] { "Only images are allowed" } });
                    _crud.CreateSpecialist(newSpecialist, null);
                    string fileName = $"specialist-{newSpecialist.Id}{ext}";
                    using (var stream = new FileStream($"./wwwroot/images/specialists/{fileName}", FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        newSpecialist.Image = fileName;
                        _crud.UpdateSpecialist(newSpecialist, specialist.Services);
                        return Ok(newSpecialist);
                    }
                }
                return BadRequest(new { errors = new string[] { "Empty file" } });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpPut("api/specialist/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update([FromForm] CreateSpecialistModel specialist, [FromForm] IFormFile file, [FromForm] bool imageDelete, [FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
                var curSpecialist = _crud.GetSpecialist(id);
            if(curSpecialist == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                DateTime date;
                if (!DateTime.TryParse(specialist.StartDate, out date))
                    return BadRequest(new { errors = new string[] { "Start date has wrong format" } });
                var newSpecialist = new SpecialistDTO
                {
                    Id = id,
                    Name = specialist.Name,
                    Surname = specialist.Surname,
                    FatherName = specialist.FatherName,
                   // Image = specialist.Image,
                    Phone = specialist.Phone,
                    StartDate = specialist.StartDate,
                };
                string imagePath = $"./wwwroot/images/specialists/";
                string curImage = $"{imagePath}{curSpecialist.Image}";
                if (file == null)
                {
                    if (imageDelete)
                    {
                        if (System.IO.File.Exists(curImage))
                            System.IO.File.Delete(curImage);
                        newSpecialist.Image = null;
                    }
                    else
                        newSpecialist.Image = curSpecialist.Image;
                    _crud.UpdateSpecialist(newSpecialist, specialist.Services);
                    return NoContent();
                }
                if (file.Length > 0)
                {
                    var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                    if (string.IsNullOrEmpty(ext) || (ext != ".jpg" && ext != ".jpeg" && ext != ".png" && ext != ".gif"))
                        return BadRequest(new { errors = new string[] { "Only images are allowed" } });
                    _crud.UpdateSpecialist(newSpecialist, null);
                    string fileName = $"specialist-{id}{ext}";
                    newSpecialist.Id = id;
                    newSpecialist.Image = fileName;
                    _crud.UpdateSpecialist(newSpecialist, specialist.Services);
                    using (var stream = new FileStream($"{imagePath}{fileName}", FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        return NoContent();
                    }
                }
                return BadRequest(new { errors = new string[] { "Empty file" } });
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }


        [HttpDelete("api/specialist/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (_crud.DeleteSpecialist(id))
                    return NoContent();
                return NotFound();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }
    }
}
