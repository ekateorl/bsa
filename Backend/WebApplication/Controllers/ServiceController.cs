﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.BLL.Interfaces;
using WebApplication.BLL.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Controllers
{
    [ApiController]
    public class ServiceController : Controller
    {

        private readonly ILogger<ServiceController> _logger;
        private readonly IDBDataService _crud;
        private readonly IAccountService _accountService;
        private IMakeAppointmentService _makeAppService;

        public ServiceController(IDBDataService crud, ILogger<ServiceController> logger, IAccountService accountService, IMakeAppointmentService makeAppService)
        {
            _crud = crud;
            _logger = logger;
            _accountService = accountService;
            _makeAppService = makeAppService;
        }


        public class ServiceModel
        {
            public int? categoryId { get; set; }
        };

        [HttpPost]
        [Route("api/service/byCategory")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [AllowAnonymous]

        public async Task<IActionResult> Get([FromBody] ServiceModel model)
        {
            if (ModelState.IsValid)
            {
                string email = "";
                var claims = HttpContext.User.Claims.ToList();
                if (claims.Count > 0 && claims[1].Value == "admin")
                    return Ok(_crud.GetServices((int)model.categoryId));
                if (claims.Count > 0 && claims[1].Value == "user")
                    email = claims[0].Value;
                return Ok(_makeAppService.GetServicesForUser((int)model.categoryId, email));
            }
            else
                return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class CreateServiceModel
        {
            [Required] public string Name { get; set; }
            public string Description { get; set; }
            [Required] public int Price { get; set; }
            [Required] public string Duration { get; set; }
            [Required] public int CategoryFk { get; set; }
            public List<int> Specialists { get; set; }
        };

        [HttpPost("api/service")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([FromBody] CreateServiceModel service)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                TimeSpan duration;
                if (!TimeSpan.TryParse(service.Duration, out duration))
                    return BadRequest(new { errors = new string[] { "Duration has wrong format" } });
                var newService = new ServiceDTO
                {
                    Name = service.Name,
                    Price = service.Price,
                    Duration = service.Duration,
                    Description = service.Description,
                    CategoryFk = service.CategoryFk,
                };
                _crud.CreateService(newService, service.Specialists);
                return Ok(newService);
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpPut("api/service/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update([FromBody] CreateServiceModel service, [FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (_crud.GetService(id) == null)
                return NotFound();
            if (ModelState.IsValid)
            {
                TimeSpan duration;
                if (!TimeSpan.TryParse(service.Duration, out duration))
                    return BadRequest(new { errors = new string[] { "Duration has wrong format" } });
                var newService = new ServiceDTO
                {
                    Id = id,
                    Name = service.Name,
                    Price = service.Price,
                    Duration = service.Duration,
                    Description = service.Description,
                    CategoryFk = service.CategoryFk,
                };
                _crud.UpdateService(newService, service.Specialists);
                return NoContent();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        [HttpDelete("api/service/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            string role = HttpContext.User.Claims.ToList()[1].Value;
            if (role != "admin")
                return Unauthorized();
            if (ModelState.IsValid)
            {
                if (_crud.DeleteService(id))
                    return NoContent();
                return NotFound();
            }
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }

        public class GroupServiceModel
        {
            [Required] public List<int> Services { get; set; }
        };

        [HttpPost("api/service/group")]
        public async Task<IActionResult> GetGroups([FromBody] GroupServiceModel model)
        {
            if (ModelState.IsValid)
                return Ok(_makeAppService.GetServiceGroups(model.Services));
            return BadRequest(new { errors = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage)) });
        }
    }
}

