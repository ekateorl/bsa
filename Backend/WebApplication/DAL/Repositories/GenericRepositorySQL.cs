﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication.DAL;
using WebApplication.DAL.Interfaces;

namespace WebApplication.DAL.Repositories
{
    public class GenericRepositorySQL<TEntity>: IRepository<TEntity> where TEntity : class
    {
        private BeautySalonContext _db;
        private DbSet<TEntity> _dbSet;
        public GenericRepositorySQL(BeautySalonContext dbcontext)
        {
            if (dbcontext == null)
            {
                throw new ApplicationException("DbContext cannot be null.");
            }
            _db = dbcontext;
            _dbSet = _db.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable<TEntity>();
        }
        public TEntity GetItem(int id)
        {
            return _dbSet.Find(id);
        }
        public IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.Where(expression).AsQueryable<TEntity>();
        }
        public void Create(TEntity entity)
        {
            _dbSet.Add(entity);
        }
        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }
        public void Delete(int id)
        {
            TEntity entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }
    }
}
