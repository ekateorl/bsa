﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.DAL.Identity;
using WebApplication.DAL.Interfaces;
using WebApplication.DAL.Entities;
using WebApplication.configuration;
using Microsoft.Extensions.Options;

namespace WebApplication.DAL.Repositories
{
    public class DbRepositorySQL : IDbRepository
    {
        private BeautySalonContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole<int>> _roleManager;
        private readonly JwtConfig _jwtConfig;

        private GenericRepositorySQL<Appointment> appointmentRepository;
        private GenericRepositorySQL<Category> categoryRepository;
        private GenericRepositorySQL<Service> serviceRepository;
        private GenericRepositorySQL<ServiceAppointment> serviceAppointmentRepository;
        private GenericRepositorySQL<ServiceSpecialist> serviceSpecialistRepository;
        private GenericRepositorySQL<Status> statusRepository;
        private GenericRepositorySQL<TimeSlot> timeSlotRepository;
        private GenericRepositorySQL<Specialist> specialistRepository;
        private GenericRepositorySQL<WorkDay> workDayRepository;
        private GenericRepositorySQL<ScheduleTemplate> scheduleTemplateRepository;
        private GenericRepositorySQL<TemplateTimeSlot> templateTimeSlotRepository;

        private ApplicationUserStore userStore;

        public DbRepositorySQL(BeautySalonContext db, UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole<int>> roleManager, IOptionsMonitor<JwtConfig> optionsMonitor)
        {
            if (db == null)
                throw new ArgumentNullException("DbContext cannot be null.");
            this.db = db;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtConfig = optionsMonitor.CurrentValue;
        }
        public IRepository<Appointment> Appointment
        {
            get
            {
                if (appointmentRepository == null)
                    appointmentRepository = new GenericRepositorySQL<Appointment>(db);
                return appointmentRepository;
            }
        }

        public IRepository<Category> Category
        {
            get
            {
                if (categoryRepository == null)
                    categoryRepository = new GenericRepositorySQL<Category>(db);
                return categoryRepository;
            }
        }

        public IRepository<Service> Service
        {
            get
            {
                if (serviceRepository == null)
                    serviceRepository = new GenericRepositorySQL<Service>(db);
                return serviceRepository;
            }
        }

        public IRepository<ServiceAppointment> ServiceAppointment
        {
            get
            {
                if (serviceAppointmentRepository == null)
                    serviceAppointmentRepository = new GenericRepositorySQL<ServiceAppointment>(db);
                return serviceAppointmentRepository;
            }
        }

        public IRepository<ServiceSpecialist> ServiceSpecialist
        {
            get
            {
                if (serviceSpecialistRepository == null)
                    serviceSpecialistRepository = new GenericRepositorySQL<ServiceSpecialist>(db);
                return serviceSpecialistRepository;
            }
        }

        public IRepository<Status> Status
        {
            get
            {
                if (statusRepository == null)
                    statusRepository = new GenericRepositorySQL<Status>(db);
                return statusRepository;
            }
        }

        public IRepository<TimeSlot> TimeSlot
        {
            get
            {
                if (timeSlotRepository == null)
                    timeSlotRepository = new GenericRepositorySQL<TimeSlot>(db);
                return timeSlotRepository;
            }
        }


        public IRepository<Specialist> Specialist
        {
            get
            {
                if (specialistRepository == null)
                    specialistRepository = new GenericRepositorySQL<Specialist>(db);
                return specialistRepository;
            }
        }

        public IRepository<WorkDay> WorkDay
        {
            get
            {
                if (workDayRepository == null)
                    workDayRepository = new GenericRepositorySQL<WorkDay>(db);
                return workDayRepository;
            }
        }

        public IRepository<ScheduleTemplate> ScheduleTemplate
        {
            get
            {
                if (scheduleTemplateRepository == null)
                    scheduleTemplateRepository = new GenericRepositorySQL<ScheduleTemplate>(db);
                return scheduleTemplateRepository;
            }
        }

        public IRepository<TemplateTimeSlot> TemplateTimeSlot
        {
            get
            {
                if (templateTimeSlotRepository == null)
                    templateTimeSlotRepository = new GenericRepositorySQL<TemplateTimeSlot>(db);
                return templateTimeSlotRepository;
            }
        }

        public IApplicationUserStore UserStore
        {
            get
            {
                if (userStore == null)
                    userStore = new ApplicationUserStore(db, _userManager, _roleManager, _jwtConfig);
                return userStore;
            }
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity: class
        {
            string name = typeof(TEntity).Name;
            var property = this.GetType().GetProperty(name);
            if (property.PropertyType != typeof(IRepository<TEntity>))
                throw new Exception("No such repository");
            return (GenericRepositorySQL<TEntity>)property.GetValue(this);
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch (Exception e)
            {
               // logger.LogError(e.Message);
            }
            return -1;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
