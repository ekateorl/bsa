﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.DAL.Entities
{
    public class ApplicationRole: IdentityRole<int>
    {
        public ApplicationRole() : base()
        {
        }
        public ApplicationRole(string role): base(role)
        {
        }
    }
}
