﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplication.DAL.Entities;

#nullable disable

namespace WebApplication.DAL
{
    public partial class BeautySalonContext : IdentityDbContext<IdentityUser<int>, IdentityRole<int>, int>
    {
        public BeautySalonContext()
        {
        }

        public BeautySalonContext(DbContextOptions<BeautySalonContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceAppointment> ServiceAppointments { get; set; }
        public virtual DbSet<ServiceSpecialist> ServiceSpecialists { get; set; }
        public virtual DbSet<Specialist> Specialists { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<TimeSlot> TimeSlots { get; set; }
        public virtual DbSet<WorkDay> WorkDays { get; set; }
        public virtual DbSet<ScheduleTemplate> ScheduleTemplates { get; set; }
        public virtual DbSet<TemplateTimeSlot> TemplateTimeSlots { get; set; }



        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=BeautySalon;Trusted_Connection=True;ConnectRetryCount=0");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.ToTable("Appointment");

                entity.HasIndex(e => e.ClientFk, "IX_Appointment_ClientFk");

                entity.HasIndex(e => e.TimeSlotFk, "IX_Appointment_TimeSlotFk");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.Review).IsUnicode(false);

                entity.Property(e => e.ReviewTime).HasColumnType("datetime");

                entity.HasOne(d => d.ClientFkNavigation)
                   .WithMany(p => p.Appointments)
                   .HasForeignKey(d => d.StatusFk)
                   .OnDelete(DeleteBehavior.ClientSetNull)
                   .HasConstraintName("FK_Appointment_ApplicationUser");

                entity.HasOne(d => d.StatusFkNavigation)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.StatusFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Appointment_Status");

                entity.HasOne(d => d.TimeSlotFkNavigation)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.TimeSlotFk)
                    .HasConstraintName("FK_Appointment_TimeSlot");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.Image)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("Service");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.HasOne(d => d.CategoryFkNavigation)
                    .WithMany(p => p.Services)
                    .HasForeignKey(d => d.CategoryFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Service_Category");
            });

            modelBuilder.Entity<ServiceAppointment>(entity =>
            {
                entity.ToTable("ServiceAppointment");

                entity.HasOne(d => d.AppointmentFkNavigation)
                    .WithMany(p => p.ServiceAppointments)
                    .HasForeignKey(d => d.AppointmentFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_ServiceAppointment_Appointment");

                entity.HasOne(d => d.ServiceFkNavigation)
                    .WithMany(p => p.ServiceAppointments)
                    .HasForeignKey(d => d.ServiceFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_ServiceAppointment_Service");
            });

            modelBuilder.Entity<ServiceSpecialist>(entity =>
            {
                entity.ToTable("ServiceSpecialist");

                entity.HasOne(d => d.ServiceFkNavigation)
                    .WithMany(p => p.ServiceSpecialists)
                    .HasForeignKey(d => d.ServiceFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_ServiceSpecialist_Service");

                entity.HasOne(d => d.SpecialistFkNavigation)
                    .WithMany(p => p.ServiceSpecialists)
                    .HasForeignKey(d => d.SpecialistFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_ServiceSpecialist_User");
            });

            modelBuilder.Entity<Specialist>(entity =>
            {
                entity.ToTable("Specialist");

                entity.Property(e => e.Image)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("Status");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TimeSlot>(entity =>
            {
                entity.ToTable("TimeSlot");

                entity.HasOne(d => d.WorkDayFkNavigation)
                    .WithMany(p => p.TimeSlots)
                    .HasForeignKey(d => d.WorkDayFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TimeSlot_WorkDay");
            });

            modelBuilder.Entity<WorkDay>(entity =>
            {
                entity.ToTable("WorkDay");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.HasOne(d => d.SpecialistFkNavigation)
                    .WithMany(p => p.WorkDays)
                    .HasForeignKey(d => d.SpecialistFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_WorkDay_User");
            });

            modelBuilder.Entity<ScheduleTemplate>(entity =>
            {
                entity.ToTable("ScheduleTemplate");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TemplateTimeSlot>(entity =>
            {
                entity.ToTable("TemplateTimeSlot");

                entity.HasOne(d => d.ScheduleTemplateFkNavigation)
                    .WithMany(p => p.TemplateTimeSlots)
                    .HasForeignKey(d => d.ScheduleTemplateFk)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TemplateTimeSlot_ScheduleTemplate");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
