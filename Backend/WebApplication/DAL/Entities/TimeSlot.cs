﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class TimeSlot
    {
        public TimeSlot()
        {
            Appointments = new HashSet<Appointment>();
        }

        public int Id { get; set; }
        public TimeSpan Beginning { get; set; }
        public TimeSpan Duration { get; set; }
        public int WorkDayFk { get; set; }
        public int? AppointmentFk { get; set; }

        public virtual WorkDay WorkDayFkNavigation { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
