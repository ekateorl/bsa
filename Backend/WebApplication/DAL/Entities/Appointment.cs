﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class Appointment
    {
        public Appointment()
        {
            ServiceAppointments = new HashSet<ServiceAppointment>();
        }

        public int Id { get; set; }
        public decimal Price { get; set; }
        public int ClientFk { get; set; }
        public int? TimeSlotFk { get; set; }
        public TimeSpan Duration { get; set; }
        public int StatusFk { get; set; }
        public int? Rating { get; set; }
        public string Review { get; set; }
        public DateTime? ReviewTime { get; set; }

        public virtual ApplicationUser ClientFkNavigation { get; set; }
        public virtual Status StatusFkNavigation { get; set; }
        public virtual TimeSlot TimeSlotFkNavigation { get; set; }
        public virtual ICollection<ServiceAppointment> ServiceAppointments { get; set; }
    }
}
