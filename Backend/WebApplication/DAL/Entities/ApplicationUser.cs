﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class ApplicationUser: IdentityUser<int>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }

    }
}
