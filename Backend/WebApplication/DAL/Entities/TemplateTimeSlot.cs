﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.DAL.Entities
{
    public class TemplateTimeSlot
    {
        public int Id { get; set; }
        public TimeSpan Beginning { get; set; }
        public TimeSpan Duration { get; set; }
        public int ScheduleTemplateFk { get; set; }
        public int DayOfWeek { get; set; }

        public virtual ScheduleTemplate ScheduleTemplateFkNavigation { get; set; }
    }
}
