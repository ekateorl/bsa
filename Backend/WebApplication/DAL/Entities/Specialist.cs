﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class Specialist
    {
        public Specialist()
        {
            ServiceSpecialists = new HashSet<ServiceSpecialist>();
            WorkDays = new HashSet<WorkDay>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string Phone { get; set; }
        public string Image { get; set; }
        public bool Deleted { get; set; }
        public DateTime StartDate { get; set; }
        public  decimal Rating { get; set; }
        public int RatingCount { get; set; }
        public virtual ICollection<ServiceSpecialist> ServiceSpecialists { get; set; }
        public virtual ICollection<WorkDay> WorkDays { get; set; }
    }
}
