﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class ServiceAppointment
    {
        public int Id { get; set; }
        public int ServiceFk { get; set; }
        public int AppointmentFk { get; set; }

        public virtual Appointment AppointmentFkNavigation { get; set; }
        public virtual Service ServiceFkNavigation { get; set; }
    }
}
