﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.DAL.Entities
{
    public class ScheduleTemplate
    {
        public ScheduleTemplate()
        {
            TemplateTimeSlots = new HashSet<TemplateTimeSlot>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TemplateTimeSlot> TemplateTimeSlots { get; set; }
    }
}
