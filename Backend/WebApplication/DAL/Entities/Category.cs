﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class Category
    {
        public Category()
        {
            Services = new HashSet<Service>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Service> Services { get; set; }
    }
}
