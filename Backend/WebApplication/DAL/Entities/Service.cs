﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class Service
    {
        public Service()
        {
            ServiceAppointments = new HashSet<ServiceAppointment>();
            ServiceSpecialists = new HashSet<ServiceSpecialist>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public TimeSpan Duration { get; set; }
        public int CategoryFk { get; set; }
        public bool Deleted { get; set; }

        public virtual Category CategoryFkNavigation { get; set; }
        public virtual ICollection<ServiceAppointment> ServiceAppointments { get; set; }
        public virtual ICollection<ServiceSpecialist> ServiceSpecialists { get; set; }
    }
}
