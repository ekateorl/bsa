﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class ServiceSpecialist
    {
        public int Id { get; set; }
        public int ServiceFk { get; set; }
        public int SpecialistFk { get; set; }

        public virtual Service ServiceFkNavigation { get; set; }
        public virtual Specialist SpecialistFkNavigation { get; set; }
    }
}
