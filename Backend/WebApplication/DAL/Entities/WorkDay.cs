﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.DAL.Entities
{
    public partial class WorkDay
    {
        public WorkDay()
        {
            TimeSlots = new HashSet<TimeSlot>();
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Beginning { get; set; }
        public TimeSpan Ending { get; set; }
        public int SpecialistFk { get; set; }

        public virtual Specialist SpecialistFkNavigation { get; set; }
        public virtual ICollection<TimeSlot> TimeSlots { get; set; }
    }
}
