﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.DAL.Entities;

namespace WebApplication.DAL.Interfaces
{
    public interface IDbRepository : IDisposable
    {
        IRepository<Appointment> Appointment { get; }
        IRepository<Category> Category { get; }
        IRepository<Service> Service { get; }
        IRepository<ServiceAppointment> ServiceAppointment { get; }
        IRepository<ServiceSpecialist> ServiceSpecialist { get; }
        IRepository<Status> Status { get; }
        IRepository<TimeSlot> TimeSlot { get; }
        IRepository<Specialist> Specialist { get; }
        IRepository<WorkDay> WorkDay { get; }
        IRepository<ScheduleTemplate> ScheduleTemplate { get; }
        IRepository<TemplateTimeSlot> TemplateTimeSlot { get; }
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        IApplicationUserStore UserStore { get; }
        int Save();
    }
}
