﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApplication.DAL.Entities;

namespace WebApplication.DAL.Interfaces
{
    public interface IApplicationUserStore
    {
        ApplicationUser GetUser(int id);
        ApplicationUser GetUser(string email);
        ApplicationUser GetUser(ClaimsPrincipal principal);
        List<string> GetUserRole(ApplicationUser user);
        Task<IdentityResult> Create(ApplicationUser user, string password, string role);
        SignInResult SignIn(string Email, string Password, bool RememberMe);
        string GenerateJwtToken(ApplicationUser user);
        void SignOut();
    }
}
