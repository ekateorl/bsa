﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebApplication.DAL.Interfaces;
using WebApplication.DAL.Entities;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WebApplication.configuration;

namespace WebApplication.DAL.Identity
{
    public class ApplicationUserStore : IApplicationUserStore
    {
        private BeautySalonContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole<int>> _roleManager;
        private readonly JwtConfig _jwtConfig;

        public ApplicationUserStore(BeautySalonContext db, UserManager<ApplicationUser> userManager, 
             RoleManager<IdentityRole<int>> roleManager, JwtConfig jwtConfig)
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtConfig = jwtConfig;
        }

        public ApplicationUser GetUser(int id)
        {
            return (ApplicationUser)_db.Users.Find(id);
        }
        public ApplicationUser GetUser(string email)
        {
            return (ApplicationUser)_db.Users.Where(u => u.Email == email).ToList().FirstOrDefault();
        }

        public ApplicationUser GetUser(ClaimsPrincipal principal)
        {
            var res = _userManager.GetUserAsync(principal);
            res.Wait();
            return res.Result;
        }

        public List<string> GetUserRole(ApplicationUser user)
        {
            var res = _userManager.GetRolesAsync(user);
            res.Wait();
            return res.Result.ToList();
        }
        public async Task<IdentityResult> Create(ApplicationUser user, string password, string role)
        {
            IdentityResult res = null;
            if (role == null)
                role = "user";
            if (await _roleManager.FindByNameAsync(role) == null)
            {
                res = await _roleManager.CreateAsync(new IdentityRole<int>(role));
            }
            if (res == null || res.Succeeded)
            {
                res = await _userManager.CreateAsync(user, password);
                if (res.Succeeded)
                    await _userManager.AddToRoleAsync(user, role);
            }
            return res;
        }

        public SignInResult SignIn(string Email, string Password, bool RememberMe)
        {
            var user = GetUser(Email);
            if (user == null)
                return SignInResult.Failed;
           var result = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, Password);
            if (result == PasswordVerificationResult.Success)
            return SignInResult.Success;
            return SignInResult.Failed;
        }

        public string GenerateJwtToken(ApplicationUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);

            var claims = new List<Claim> { new Claim(JwtRegisteredClaimNames.Email, user.Email), new Claim("Role", GetUserRole(user)[0]) };
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
               
                Expires = null,

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }

        public void SignOut()
        {
        }

    }
}
