﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class CategoryCascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Service_Category",
                table: "Service");

            migrationBuilder.AddForeignKey(
                name: "FK_Service_Category",
                table: "Service",
                column: "CategoryFk",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Service_Category",
                table: "Service");

            migrationBuilder.AddForeignKey(
                name: "FK_Service_Category",
                table: "Service",
                column: "CategoryFk",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
