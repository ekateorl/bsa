﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class Cascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceAppointment_Appointment",
                table: "ServiceAppointment");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceAppointment_Service",
                table: "ServiceAppointment");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceSpecialist_Service",
                table: "ServiceSpecialist");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceSpecialist_User",
                table: "ServiceSpecialist");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlot_WorkDay",
                table: "TimeSlot");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkDay_User",
                table: "WorkDay");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceAppointment_Appointment",
                table: "ServiceAppointment",
                column: "AppointmentFk",
                principalTable: "Appointment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceAppointment_Service",
                table: "ServiceAppointment",
                column: "ServiceFk",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceSpecialist_Service",
                table: "ServiceSpecialist",
                column: "ServiceFk",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceSpecialist_User",
                table: "ServiceSpecialist",
                column: "SpecialistFk",
                principalTable: "Specialist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlot_WorkDay",
                table: "TimeSlot",
                column: "WorkDayFk",
                principalTable: "WorkDay",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkDay_User",
                table: "WorkDay",
                column: "SpecialistFk",
                principalTable: "Specialist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceAppointment_Appointment",
                table: "ServiceAppointment");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceAppointment_Service",
                table: "ServiceAppointment");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceSpecialist_Service",
                table: "ServiceSpecialist");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceSpecialist_User",
                table: "ServiceSpecialist");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlot_WorkDay",
                table: "TimeSlot");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkDay_User",
                table: "WorkDay");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceAppointment_Appointment",
                table: "ServiceAppointment",
                column: "AppointmentFk",
                principalTable: "Appointment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceAppointment_Service",
                table: "ServiceAppointment",
                column: "ServiceFk",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceSpecialist_Service",
                table: "ServiceSpecialist",
                column: "ServiceFk",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceSpecialist_User",
                table: "ServiceSpecialist",
                column: "SpecialistFk",
                principalTable: "Specialist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlot_WorkDay",
                table: "TimeSlot",
                column: "WorkDayFk",
                principalTable: "WorkDay",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkDay_User",
                table: "WorkDay",
                column: "SpecialistFk",
                principalTable: "Specialist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
