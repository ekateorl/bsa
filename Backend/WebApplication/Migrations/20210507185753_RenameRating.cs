﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class RenameRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RaitingCount",
                table: "Specialist",
                newName: "RatingCount");

            migrationBuilder.RenameColumn(
                name: "Raiting",
                table: "Specialist",
                newName: "Rating");

            migrationBuilder.RenameColumn(
                name: "Raiting",
                table: "Appointment",
                newName: "Rating");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RatingCount",
                table: "Specialist",
                newName: "RaitingCount");

            migrationBuilder.RenameColumn(
                name: "Rating",
                table: "Specialist",
                newName: "Raiting");

            migrationBuilder.RenameColumn(
                name: "Rating",
                table: "Appointment",
                newName: "Raiting");
        }
    }
}
